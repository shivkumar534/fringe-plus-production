({
    doInit: function(component, event, helper) { 
        var action = component.get("c.getCustomSettingDetails");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();           
                //alert(JSON.stringify(result));
                component.set("v.ObjectName", result);
            }
        });
        $A.enqueueAction(action);
    },
	submit : function(component, event, helper) {
        //alert(component.get("v.docId"));
		var action = component.get("c.bulkapijob");  
        var objName = component.get("v.selectedObject");
        //alert(objName);
        action.setParams({
            recordId : component.get("v.docId"),
            operation :component.get("v.operation"),
            ObjectName:objName
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                $A.get('e.force:refreshView').fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "The record has been created successfully."
                });
                toastEvent.fire(); 
                //alert(JSON.stringify(response.getReturnValue()));                
            } else if(state === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert('Upload Proper File format');
                        alert(errors[0].message);
                    }
                }
            }
        });       
        $A.enqueueAction(action);	
	},
    handleUploadFinished: function (cmp, event) {
        //Get the list of uploaded files
        var uploadedFiles = event.getParam("files");
		var documentId = uploadedFiles[0].documentId;  
        cmp.set("v.docId",documentId);
        //Show success message – with no of files uploaded
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "type" : "success",
            "message": uploadedFiles[0].Name+" files has been uploaded successfully!"
        });
        toastEvent.fire();      
    } 
})