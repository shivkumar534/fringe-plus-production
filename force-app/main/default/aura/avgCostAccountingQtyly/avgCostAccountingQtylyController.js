({
	handleClick : function(component, event, helper) {
	var action = component.get("c.getAvgQtrDetails");
        action.setParams({
            fId : component.get("v.fringeID"),
            qtr:component.get("v.quarter")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();           
                //alert(JSON.stringify(result));
                component.set("v.recordTypeName", result);
            }
        });
        $A.enqueueAction(action);
    },	
    onquarterChange : function(component, event, helper) {
        var sel = component.find("quarterId").get("v.value");
        if(sel =="Q1"){
            component.set("v.quarter1", true);
            component.set("v.quarter2", false);
            component.set("v.quarter3", false);
            component.set("v.quarter4", false);
        }else if(sel =="Q2"){
            component.set("v.quarter2", true);
            component.set("v.quarter1", false);
            component.set("v.quarter3", false);
            component.set("v.quarter4", false);
        }else if(sel =="Q3"){
            component.set("v.quarter3", true);
            component.set("v.quarter1", false);
            component.set("v.quarter2", false);
            component.set("v.quarter4", false);
        }else if(sel =="Q4"){
            component.set("v.quarter4", true);
            component.set("v.quarter1", false);
            component.set("v.quarter2", false);
            component.set("v.quarter3", false);
        }
    },
	
})