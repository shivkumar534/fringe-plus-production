({
	doInit : function(component, event, helper) {
		helper.getChildRecors(component, event);
        helper.getLabelEmployee(component, event);
	},
     selectStep1 : function(component,event,helper){
        component.set("v.selectedStep", "masterRcrd");
    },
    selectStep2 : function(component,event,helper){
        component.set("v.selectedStep", "reassign");
    },
    submitAddBenefits: function(component,event,helper){
        helper.createIFB(component, event);
    },
    hraChangeVale:function(component,event,helper){
        var fetchObj = component.get("v.ChildRecordList");
        var ifbObj = component.get("v.submitRecord");
        var sel = component.find("HRAPick").get("v.value");
        if(sel=="Transfer"){
            component.set("v.submitRecord.HRA__c", fetchObj.HRA__c);
            component.set("v.submitRecord.Month_and_Year__c", fetchObj.Employee__r.Month_and_Year__c);
            component.set("v.submitEmp.Coverage_Tier__c", fetchObj.Employee__r.Coverage_Tier__c);
            component.set("v.submitEmp.Health_Insurance_Plan__c", fetchObj.Employee__r.Health_Insurance_Plan__c);
            component.set("v.submitEmp.Dental_Insurance_Plan__c", fetchObj.Employee__r.Dental_Insurance_Plan__c);
            component.set("v.submitEmp.Life_Insurance_Plan__c", fetchObj.Employee__r.Life_Insurance_Plan__c);
            component.set("v.submitEmp.Vision_Insurance_Plan__c", fetchObj.Employee__r.Vision_Insurance_Plan__c);
        }else{
           component.set("v.submitRecord.HRA__c", null);
           component.set("v.submitRecord.Month_and_Year__c", fetchObj.Employee__r.Month_and_Year__);
        }
    },
    onRetirementChange:function(component,event,helper){
        var fetchObj = component.get("v.ChildRecordList");
        var ifbObj = component.get("v.submitRecord");
        var sel = component.find("RetirementPick").get("v.value");
        if(sel=="Transfer"){
           component.set("v.submitRecord.Retirement__c", fetchObj.Retirement__c);
        }else{
           component.set("v.submitRecord.Retirement__c", null);
        }
    },
    onPaidLeaveChange:function(component,event,helper){
        var fetchObj = component.get("v.ChildRecordList");
        var ifbObj = component.get("v.submitRecord");
        var sel = component.find("PaidPick").get("v.value");
        if(sel=="Transfer"){
           component.set("v.submitRecord.Paid_Leave_Account__c", fetchObj.Paid_Leave_Account__c);
        }else{
           component.set("v.submitRecord.Paid_Leave_Account__c", null);
        }
    },
    onRollBalChange:function(component,event,helper){
        var fetchObj = component.get("v.ChildRecordList");
        var ifbObj = component.get("v.submitRecord");
        var sel = component.find("RollingPick").get("v.value");
        if(sel=="Transfer"){
           component.set("v.submitRecord.Rolling_Individual_Premium_Reserve__c", fetchObj.Rolling_Individual_Premium_Reserve__c);
        }else{
           component.set("v.submitRecord.Rolling_Individual_Premium_Reserve__c", null);
        }
    },
    handleFinish:function(component,event,helper){
        $A.get("e.force:closeQuickAction").fire(); 
    }
})