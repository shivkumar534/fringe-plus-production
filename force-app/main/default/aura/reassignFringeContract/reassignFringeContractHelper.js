({
	getChildRecors: function(component, event) {
        // call apex method for fetch child records list.        
        var action = component.get('c.getindividualBenefits');
        action.setParams({
            "recId": component.get("v.recordId")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === 'SUCCESS') {
                //set response value in ChildRecordList attribute on component.
                component.set('v.ChildRecordList', actionResult.getReturnValue());
                //alert(JSON.stringify(component.get('v.ChildRecordList')));
            }
        });
        $A.enqueueAction(action);
    },
    createIFB : function(component, event) {
        //var fetchObj = component.get("v.ChildRecordList");
        var ifbObj = component.get("v.submitRecord");
        var empObj = component.get("v.submitEmp");
        //empObj.Id = component.get("v.recordId"); 
        ifbObj.Employee__c = component.get("v.recordId");
        var action = component.get('c.createBenefits'); 
        action.setParams({
            "objBenefits": ifbObj,
            "empObj":empObj
        }); 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {    
                $A.get("e.force:closeQuickAction").fire();
                 var toastEvent = $A.get("e.force:showToast");
          			toastEvent.setParams({
        				"title": "Success!",
        				"message": "The Child record's has been added successfully."
    				});
                    toastEvent.fire();              
               //$A.get('e.force:refreshView').fire();                               
               }
        });
        $A.enqueueAction(action); 
    },
    getLabelEmployee: function(component, event) {
        var action = component.get('c.getLabelEmployee');
       /* action.setParams({
            "recId": component.get("v.recordId")
        });*/
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === 'SUCCESS') {
                //set response value in ChildRecordList attribute on component.
                component.set('v.planLabel', actionResult.getReturnValue());
                //alert(JSON.stringify(component.get('v.planLabel')));
            }
        });
        $A.enqueueAction(action);
    }
})