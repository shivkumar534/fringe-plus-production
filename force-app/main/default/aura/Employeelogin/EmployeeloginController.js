({
    doInit : function(component, event, helper) {
        component.set("v.Login",true);
        helper.contractName(component);
    },
    getContractName : function(component, event, helper) {
        var AccountName=component.get("v.AccountName");
        var action=component.get("c.getContract");
        action.setParams({
            "accId":AccountName
        });
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state==="SUCCESS"){
                component.set("v.contractoptions", response.getReturnValue());
            }else{
                console.log(response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    login : function(component, event, helper) {
        helper.Authenticate(component);
    },
    
    getEmployeedetails : function(component, event, helper) { 
    	var Empid=component.find("Empid").get("v.value");
        alert('employee id :'+Empid);
        if(Empid=="SUCCESS"){
            System.debug('its from getEmployeeDetails in employeelogincontroller');
            //window.open("https://fringedev-fp.cs96.force.com/EmployeePortal/s/employee","_self");
            component.set("v.Login",false);
            component.set("v.Details",true);            
        }
        else{
            component.set("v.Login",true);
            component.set("v.Details",false); 
        }
    }
//}
 })