({
    contractName : function(component, event, helper) {
        var name=component.get("v.objName");
        var fld=component.get("v.fldName");
        var action=component.get("c.getData");
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state==="SUCCESS"){
                component.set("v.options", response.getReturnValue());
            }else{
                console.log(response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    Authenticate : function(component, event, helper) {
        //alert('after clicking on login');
        var name=component.get("v.AccountName");
        var Empid=component.find("Empid").get("v.value");
        var action=component.get("c.getEmployeeData");
        action.setParams({
            "AccountName":name,
            "EmployeeId":Empid,          
        });
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state==="SUCCESS"){
                //alert(response.getReturnValue());
                debugger;
                if(response.getReturnValue()){
                    var resp = response.getReturnValue(); 
                    //alert('tet employee id === '+resp.Id);
                    component.set("v.EmpId",resp.Id);
                    component.set("v.EmpName",resp.name);
                    component.set("v.EmpUniqueId",resp.UniqueId);
                    component.set("v.DOB__c",resp.DOB__c);//DOB__c
                    //alert('DOb :'+resp.DOB__c);
                    component.set("v.Gender__c",resp.Gender__c);//Gender__c
                    //alert('Gender :'+resp.Gender__c);
                    component.set("v.MonthlyFC",resp.MonthlyFC);
                    component.set("v.FringeRate",resp.FringeRate);
                    /*component.set("v.Curr_Reg_Hrs_NYC__c",resp.Curr_Reg_Hrs_NYC__c);
                    alert('hours :'+resp.Curr_Reg_Hrs_NYC__c)
                    component.set("v.Benefits_Effective_Date__c",resp.Benefits_Effective_Date__c);
                    alert('benefit date :'+resp.Benefits_Effective_Date__c)
                    component.set("v.Coveragetier",resp.Coveragetier);
                    component.set("v.CoverageEffectivefromDate",resp.CoverageEffectivefromDate);
                    component.set("v.CoverageEffectivetoDate",resp.CoverageEffectivetoDate);
                    component.set("v.Health_Insurance_Plan__c",resp.Health_Insurance_Plan__c);
                    component.set("v.Dental_Insurance_Plan__c",resp.Dental_Insurance_Plan__c);
                    component.set("v.Vision_Insurance_Plan__c",resp.Vision_Insurance_Plan__c);
                    component.set("v.Plan_1__c",resp.Plan_1__c);
                    component.set("v.Plan_2__c",resp.Plan_2__c);
                    component.set("v.Plan_3__c",resp.Plan_3__c);
                    component.set("v.Plan_4__c",resp.Plan_4__c);
                    component.set("v.Plan_5__c",resp.Plan_5__c);
                    
					component.set("v.Dental_Insurance_Plan_Value",resp.Dental_Insurance_Plan_Value);
                    component.set("v.Health_Insurance_Plan_Value",resp.Health_Insurance_Plan_Value);
                    component.set("v.Vision_Insurance_Plan_Value",resp.Vision_Insurance_Plan_Value);
                    component.set("v.Monthly_Cost_Of_Benefits__c",resp.Monthly_Cost_Of_Benefits__c);
                    alert('monthly cost of benefits :'+resp.Monthly_Cost_Of_Benefits__c); */
                    //alert('after commenting for employee details in employee-login-helper');
                    component.set("v.loginsuccess",true);
                    component.set("v.Login",false);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "LoggedIn successfully."
                    });
                    toastEvent.fire();
                    //Login
                   /* $A.get('e.force:refreshView').fire();
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "LoggedIn successfully."
                    });
                    toastEvent.fire();*/
                    //window.open("https://fringedev-fp.cs96.force.com/EmployeePortal/s/employee","_self");
                    /*var evt = $A.get("e.force:navigateToComponent");
                    console.log('evt '+ evt);
                    evt.setParams({
                        componentDef  : "c:Employee" ,
                        componentAttributes : {
                            Empid : resp
                            
                        }
                    });
                    evt.fire();*/
                }
            }
            else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "OOps!",
                    "message": "Invalid Credientials."
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    }
    
})