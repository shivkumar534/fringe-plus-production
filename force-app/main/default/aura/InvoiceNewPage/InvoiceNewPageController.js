({
	myAction : function(component, event, helper) {
		
	},
     openmodal:function(component,event,helper) 
    {
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
        helper.getRecordType(component,event,helper);
    },
    getLocationDetails:function(component,event,helper){
      var action = component.get("c.getLocationValue");
        action.setParams({
            recId : component.get("v.invoice.Fringe_Contract__c")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                //alert(JSON.stringify(result.Location__c));
                var location =[];
                var str_array = result.Location__c.split(';');                
                for(var i = 0; i < str_array.length; i++) {
                    location.push(str_array[i]);
                }                
                //alert(JSON.stringify(location));
                component.set("v.jobLoc",location);
            }
        });
        $A.enqueueAction(action);  
    },
    Savedata :function(component,event,helper){
    helper.saveDataInvoice(component,helper);
	},
    closeModal : function (component, event, helper) {
        var action = component.get("c.getListViews");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var listviews = response.getReturnValue();
                var navEvent = $A.get("e.force:navigateToList");
                navEvent.setParams({
                    "listViewId": listviews.Id,
                    "listViewName": null,
                    "scope": "Fringe_Remittance_Invoice__c"
                });
                navEvent.fire();
            }
        });
        $A.enqueueAction(action);
	}
    
})