({
	getRecordType : function(component) {
		var action = component.get("c.getRecordTypeValue");        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();              
                component.set("v.recordTypeName", result);                
            }
        });
        $A.enqueueAction(action);  
	},
    saveDataInvoice :function(component) {
        var objInvoice = component.get("v.invoice");
        var dualBoxSStates = component.get("v.selected");
        //alert(dualBoxSStates);
        objInvoice.Job_Site_Locations__c =dualBoxSStates.join(" ; ");
        var action = component.get("c.createInvoice");
        action.setParams({
            objInvoice : objInvoice
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
               // var result = response.getReturnValue();
                $A.get('e.force:refreshView').fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "The record has been created successfully."
                });
                toastEvent.fire();
                var navLink = component.find("navLink");
                var pageRef ={
                    type: 'standard__recordPage',
                    attributes:{
                        actionName : 'view',
                        objectApiName :'Fringe_Remittance_Invoice__c',
                        recordId : response.getReturnValue()
                    },
                };
                navLink.navigate(pageRef,true);                
            }
        });
        $A.enqueueAction(action);
    },    
})