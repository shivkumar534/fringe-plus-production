({
	myAction : function(component, event, helper) {
		
	},
     openmodal:function(component,event,helper) 
    {
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
        //helper.getRecordType(component,event,helper);
    },
      selectStep1 : function(component,event,helper){
        component.set("v.selectedStep", "masterRcrd");
    },
    selectStep2 : function(component,event,helper){
        component.set("v.selectedStep", "reassign");
    },
})