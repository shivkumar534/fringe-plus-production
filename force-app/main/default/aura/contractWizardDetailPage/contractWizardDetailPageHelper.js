({
    getRecordType: function(component, event) {
        var action = component.get("c.getRecordType");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();           
                //alert(JSON.stringify(result));
                component.set("v.recordTypeName", result);
            }
        });
        $A.enqueueAction(action);
    },
    fetchCustomSettingsBenefits: function(component, event) {
        var action = component.get("c.getCustomSettingDetails");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();           
                //alert(JSON.stringify(result));fetchCustomSettingsBenefits
                component.set("v.benefitsTypeName", result);
            }
        });
        $A.enqueueAction(action);
    },
    dailbox: function(component, event, helper) {
        var action = component.get("c.getPiklistValues");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var result = response.getReturnValue();
                var plValues = [];
                for (var i = 0; i < result.length; i++) {
                    plValues.push({
                        label: result[i],
                        value: result[i]
                    });
                }
                //alert(JSON.stringify(plValues));
                component.set("v.GenreList", plValues);
            }
        });
        $A.enqueueAction(action);
    },    
    selectBenefits: function(component, event) {
        var action = component.get("c.getBenefitsValue");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {                
                var result = response.getReturnValue();
                var fieldMap = [];
                for(var key in result){
                    fieldMap.push({key: key, value: result[key]});
                } 
                //alert(JSON.stringify(fieldMap));
                component.set("v.fieldBenefits", fieldMap);
            }
        });
        $A.enqueueAction(action);
    },
    contractType: function(component, event) {
        var action = component.get("c.getcontractType");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var fieldMap = [];
                for(var key in result){
                    fieldMap.push({key: key, value: result[key]});
                }
                //alert(JSON.stringify(fieldMap));
                component.set("v.fieldContract", fieldMap);
            }
        });
        $A.enqueueAction(action);
    },
    debitRule: function(component, event) {
        var action = component.get("c.getDebitFieldValue");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var fieldMap = [];
                for(var key in result){
                    fieldMap.push({key: key, value: result[key]});
                }
                // alert(JSON.stringify(fieldMap));
                component.set("v.fieldDebit", fieldMap);
            }
        });
        $A.enqueueAction(action);
    },
    getStatePicklistValues: function(component, event) {
        var action = component.get("c.getStateFieldValue");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                
                var fieldMap = [];
                for(var key in result){
                    fieldMap.push(result[key]);
                }
                // alert(JSON.stringify(fieldMap));
                component.set("v.fieldMap", fieldMap);
            }
        });
        $A.enqueueAction(action);
    },
    getPicklistFAccRule: function(component, event) {
       
        var action = component.get("c.getFringeAccRule");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var fieldMap = [];
                for(var key in result){
                    fieldMap.push({key: key, value: result[key]});
                }
                component.set("v.fieldMapAccRule", fieldMap);
            }
        });
        $A.enqueueAction(action);
    },
    getPicklistFAllocMthd: function(component, event) {        
        var action = component.get("c.getFringeMultiLocation");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var result = response.getReturnValue();
                var plValues = [];
                for (var i = 0; i < result.length; i++) {
                    plValues.push({
                        label: result[i],
                        value: result[i]
                    });
                }
                //alert(JSON.stringify(plValues));
                component.set("v.fieldMapAllocMthd", plValues);
            }
        });
        $A.enqueueAction(action);
    },
    getPicklistMthly: function(component, event) {
        var action = component.get("c.getFringeMonthly");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var fieldMap = [];
                for(var key in result){
                    fieldMap.push({key: key, value: result[key]});
                }
                component.set("v.fieldMapMonthly", fieldMap);
            }
        });
        $A.enqueueAction(action);
    },
    fetchFringeRates : function(component, event, ratesID) {
        var action = component.get("c.fetchFringeRateDetails");
        action.setParams({
            ratesID : ratesID
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                //alert(JSON.stringify(response.getReturnValue()));
                component.set("v.fringeRates",response.getReturnValue());
                //component.set("v.allPicklist",picklist);
            } else if(state === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert(errors[0].message);
                    }
                }
            }
        });       
        $A.enqueueAction(action);
        
    },
    fetchAllocation : function(component, event, AllocationId) {
        var action = component.get("c.fetchAllocationDetails");
        action.setParams({
            AllocationId : AllocationId
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                //alert(JSON.stringify(response.getReturnValue()));
                var picklist = response.getReturnValue().FringeBenefits__c.split(';');
                component.set("v.fringeAllocation",response.getReturnValue());
                component.set("v.allPicklist",picklist);
            } else if(state === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert(errors[0].message);
                    }
                }
            }
        });       
        $A.enqueueAction(action);
        
    },
    saveObjContractWizard : function(component, event) {
        
        //var rt = component.find("govContract").get("v.value");
        var selectedOption = component.get("v.selectedLocation");
        var fcObjt = component.get("v.fcObjt"); 
     	var fcObjtBenefitsList = component.get("v.fcObjtBenefitsList");
        //alert(JSON.stringify(fcObjt));
        //alert(JSON.stringify(fcObjtBenefitsList));
        //fcObjt.RecordTypeId =rt; 
        var testlook = component.get("v.selectedLookUpRecords");
        var options = '';
        var newOptions =[] ;
        for(var i = 0; i < testlook.length; i++){ 
            if(testlook[i].Name != undefined){
                options =options + testlook[i].Name+";"; 
            }                        
        }
		
        var costOfBenefit = component.get("v.coBenefitsValue");
        var avgRuleMQValue = component.get("v.avgRuleMQValue");
        //var creditRuleValue = component.get("v.creditRuleValue");
        var fringeAccountValue = component.get("v.fringeAccountValue");
        var benefitFringeValue = component.get("v.benefitFringeValue");
        var fringeCapValue = component.get("v.fringeCapValue");
        var AvgAccValue = component.get("v.AvgAccValue");
        var reportTypeValue = component.get("v.reportTypeValue");
        var preset = component.get("v.preSetValue");
        //var individualtwo = component.get("v.individualtwo");
        //var individualOnePer = component.get("v.individualOnePer");
        //var individualTwoPer = component.get("v.individualTwoPer");

        if(options != null) {
          fcObjt.Location__c = options;   
        }  
       
        fcObjt.SELECT_COST_OF_BENEFITS__c = costOfBenefit;
        fcObjt.Avg_Accounting_Rules__c = avgRuleMQValue;
        //fcObjt.Credit_Rule__c = creditRuleValue;
        fcObjt.HOURLY_COST_OF_BENEFITS__c = fringeAccountValue;
        fcObjt.Fringe_Benefit_Account__c = benefitFringeValue;
        fcObjt.Cap_Eligibity__c = fringeCapValue;
        fcObjt.Average_Cost_Accounting_Methods__c = AvgAccValue;
        fcObjt.Reporting_Type__c = reportTypeValue;
        fcObjt.Preset_Type__c = preset;
        //fcObjt.Individual_Fringe_Benefit_Account_Two__c = individualtwo;
        //fcObjt.Individual_Fringe_Benefit_One_Percent__c = individualOnePer;
        //fcObjt.Individual_Fringe_Benefit_Two_Percent__c = individualTwoPer;
        //var dualBoxSStates = component.get("v.selectedGenreList");
        //fcObjt.Job_Site_Locations__c =dualBoxSStates.join(" ; ");
        //alert('>>>>>>'+test+'<>'+test1+'=='+rt);,creditRuleValue,fringeAccountValue,benefitFringeValue,fringeCapValue,hourBankValue        
        var action = component.get("c.createContractWizardSetup");
        action.setParams({
            objContract : fcObjt,
            fcObjtBenefitsList:fcObjtBenefitsList
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                //alert(response.getReturnValue());
                $A.get('e.force:refreshView').fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "The record has been created successfully."
                });
                toastEvent.fire();                
                var navLink = component.find("navLink");
                var pageRef ={
                    type: 'standard__recordPage',
                    attributes:{
                        actionName : 'view',
                        objectApiName :'Fringe_Contract__c',
                        recordId : response.getReturnValue()
                    },
                };
                navLink.navigate(pageRef,true);
                                              
            } else if(state === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert(errors[0].message);
                    }
                }
            }
        });       
        $A.enqueueAction(action);
    }, 
    //add and remove row
    addAccountRecord: function(component, event) {
        var fcObjtList = component.get("v.fcObjtBenefitsList");
        fcObjtList.push({
            'sobjectType': 'Individual_Fringe_Benefits_Percentages__c',
            'Individual_Fringe_Benefit_Account__c': '',
            'Percentage__c': '',
            'Credit_Debit_Rules__c':'',
            'Monthly_Fringe_Obligations__c':''           
        });
        component.set("v.fcObjtBenefitsList", fcObjtList);
        
    },     
    validateAccountList: function(component, event) {
        var isValid = true;
        var fcObjtBenefitsList = component.get("v.fcObjtBenefitsList");
        for (var i = 0; i < fcObjtBenefitsList.length; i++) {
            if (fcObjtBenefitsList[i].Name == '') {
                isValid = false;
                alert('Account Name cannot be blank on row number ' + (i + 1));
            }
        }
        return isValid;
    }, 
})