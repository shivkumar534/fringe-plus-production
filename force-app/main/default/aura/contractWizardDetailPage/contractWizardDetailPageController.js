({
    doInit: function(component, event, helper) {        
        //helper.getStatePicklistValues(component, event);
        //helper.fetchPickListVal(component, 'SELECT_COST_OF_BENEFITS__c', 'accIndustry');
        helper.getPicklistFAccRule(component, event);
        helper.getPicklistFAllocMthd(component, event);
        helper.getPicklistMthly(component, event);
        //helper.getRecordType(component);
        //helper.fetchCustomSettingsBenefits(component, event);
        helper.dailbox(component, event);
        //helper.selectBenefits(component, event);
        helper.contractType(component, event);
        //helper.debitRule(component, event);
        
    },
    paidleaveEmp : function(component, event, helper) {
        var sel = component.find("paidLeaveAccMeth").get("v.value");
        //alert(sel);
        if(sel=="SAME AMOUNT FOR ALL EMPLOYEES"){
            component.set("v.paidLeaveSameAmt",true);
            component.set("v.paidLeaveAmtBased",false);
        }else if(sel=="AMOUNT BASED ON HOURLY PAY RATE"){
            component.set("v.paidLeaveSameAmt",false);
            component.set("v.paidLeaveAmtBased",true);
        }else{
            component.set("v.paidLeaveSameAmt",false);
            component.set("v.paidLeaveAmtBased",false);
        }
    },
    getAllocationRules : function(component, event, helper) { 
    	var fcObjt = component.get("v.fcObjt");
        var allocation = fcObjt.Fringe_Allocation_Rules__c;
        //alert(allocation);Fringe_Rate_Based_on_Job_Classification__c
        helper.fetchAllocation(component, event,allocation);
    },
    davisbeconFringeRate : function(component, event, helper) { 
        var fcObjt = component.get("v.fcObjt");
    	var sel = component.find("jobClass").get("v.value");
        if(sel=="Fringe Rate Based on Job Classification"){
            //component.set("v.fcObjt.Fringe_Rate_Based_on_Job_Classification__c",true);
            component.set("v.davisClass",true); 
            component.set("v.davisRate",false);
        }else if(sel=="Same Fringe Rates for All Employees"){
            component.set("v.davisRate",true);            
            component.set("v.davisClass",false);
        }else{
            component.set("v.davisRate",false);
            component.set("v.davisClass",false);
            //component.set("v.davis",false);
        }
        //alert(contractType);wage
        //helper.fetchAllocation(component, event,allocation);a0A1g000004VyJYEA0,a0A1g000004VyJdEAK,a0A1g000004VyJTEA0,a0A1g000004VyJOEA0
    },
    getFringeRatesdetail : function (component, event, helper) {
        var fcObjt = component.get("v.fcObjt");
        var ratesID = fcObjt.Fringe_Rate_lukUp__c;
        component.set("v.coBID",ratesID);
        //alert(allocation);
        helper.fetchFringeRates(component, event,ratesID);
    },
    handleGenreChange: function (component, event, helper) {
        var selectedValues = event.getParam("value");
        component.set("v.selectedGenreList", selectedValues);
    },
    handleFinish : function (component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
    OpenModalPopup:function (component, event, helper) {
        component.set("v.openFringeBenefits", true);
    }, 
    closeModalPopup: function(component, event, helper) {
        component.set("v.openFringeBenefits",false);
    },    
    getSelectedGenre : function(component, event, helper){
        //Get selected Genre List on button click 
        var selectedValues = component.get("v.selectedGenreList");
        console.log('Selectd Genre-' + selectedValues);
    },
    onControllerFieldChange: function(component, event, helper) {
        var sel = component.find("accRules").get("v.value");
        //alert(sel); 
        if(sel=="Individual Fringe Accounting Rule"){
            component.set("v.bDisabledDependentFld" , false); 
            component.set("v.avgAccRule" , false);
            component.set("v.paidLeaveAcc" , false);
        }else if(sel=="Average Cost Fringe Accounting Rule"){
            component.set("v.bDisabledDependentFld" , true);
            component.set("v.avgAccRule" , true);
            component.set("v.paidLeaveAcc" , false);
        }else if(sel=="Paid Leave Accounting"){
            component.set("v.bDisabledDependentFld" , false);
            component.set("v.avgAccRule" , false);
            component.set("v.paidLeaveAcc" , true);
        }else{
            component.set("v.bDisabledDependentFld" , false);
            component.set("v.avgAccRule" , false);
            component.set("v.paidLeaveAcc" , false);
        }
    },
    onpresetTypeRule : function(component, event, helper){
        var sel = component.find("presetTypeRule").get("v.value");
        if(sel === '--None--'){
            
        }else{
            component.set("v.preSetValue", sel);
        }
    },
    paidLeaveRadio: function(component, event, helper) {
        var sel = component.find("paidLeaveID").get("v.value");
        //alert(sel);
        if(sel=="Hourly"){
            component.set("v.paidLeaveHrs" , true);
            component.set("v.paidLeaveDay" , false);
        }else if(sel=="Monthly"){
            component.set("v.paidLeaveDay" , true);
            component.set("v.paidLeaveHrs" , false);
            component.set("v.fcObjt.Hours_Per_Day__c" ,8);
            //component.set("v.fcObjt.Paid_Leave_Days_per_Calendar_Year__c" ,5);
        }else{
            component.set("v.paidLeaveDay" , false);
            component.set("v.paidLeaveHrs" , false);
        }
    },
    onAllocationChange : function(component, event, helper) {
        var sel = component.find("allocRules").get("v.value");
        //alert(sel);
        if(sel=="Individual Fringe Benefit Accounts"){
            component.set("v.MethodA",true);
            component.set("v.MethodB",false);
            component.set("v.MethodC",false);
            component.set("v.MethodC2",false);
            component.set("v.MethodD",false);
            component.set("v.MethodE",false);
        }else if(sel=="Individual Fringe Benefit  & Individual Premium Reserve Accounts"){
            component.set("v.MethodA",false);
            component.set("v.MethodB",true);
            component.set("v.MethodC",false);
            component.set("v.MethodC2",false);
            component.set("v.MethodD",false);
            component.set("v.MethodE",false);
        }else if(sel=="Hour Bank Accounting"){
            component.set("v.MethodA",false);
            component.set("v.MethodB",false);
            component.set("v.MethodC",true);
            component.set("v.MethodC2",false);
            component.set("v.MethodD",false);
            component.set("v.MethodE",false);
        }else if(sel=="Hourly Cost of Benefit Plans without Hour Bank Accounts"){
            component.set("v.MethodA",false);
            component.set("v.MethodB",false);
            component.set("v.MethodC",false);
            component.set("v.MethodC2",false);
            component.set("v.MethodD",true);
            component.set("v.MethodE",false);
        }else if(sel=="Class Level Coverage"){
            component.set("v.MethodA",false);
            component.set("v.MethodB",false);
            component.set("v.MethodC",false);
            component.set("v.MethodC2",false);
            component.set("v.MethodD",false);
            component.set("v.MethodE",true);
        }else if(sel=="Hour Bank Accounting with Individual Fringe Benefit Accounts for Underspent Fringe"){
            component.set("v.MethodC2",true);
            component.set("v.MethodA",false);
            component.set("v.MethodB",false);
            component.set("v.MethodC",false);
            component.set("v.MethodD",false);
            component.set("v.MethodE",false);
        }else{            
            component.set("v.MethodA",false);
            component.set("v.MethodB",false);
            component.set("v.MethodC",false);
            component.set("v.MethodC2",false);
            component.set("v.MethodD",false);
            component.set("v.MethodE",false);
        }
    },
    onchangeObligation:function(component, event, helper) {
        var sel = component.find("methodB").get("v.value");
        if(sel=="Underspent"){
            component.set("v.obligationUnderSpent",true);
            component.set("v.obligationOverSpent",false);
        }else if(sel=="Overspent"){
            component.set("v.obligationOverSpent",true);
            component.set("v.obligationUnderSpent",false);
        }else{
            component.set("v.obligationOverSpent",false);
            component.set("v.obligationUnderSpent",false);
        }
    },
    toggleMonthly :function(component, event, helper) {
        var sel = component.find("monthlyRule").get("v.value");
        if(sel=="Paid Hours Accounting"){
            component.set("v.paidHrs",true);
            component.set("v.productiveHrs",false);
        }
        else if(sel=="Productive Hours Accounting"){
            component.set("v.productiveHrs",true);
            component.set("v.paidHrs",false);
        }
            else{
                component.set("v.paidHrs",false);
                component.set("v.productiveHrs",false);
            }        
    },
    contractRules:function(component, event, helper) {
        var sel = component.find("ContractType").get("v.value");
        //alert(sel);wageParity	       
        if(sel=="SCA - Service Contract Act" || sel=="Ability One Program" ||sel=="Responsible Wage Ordinance" || sel=="Living Wage and Responsible Wage Ordinances"){
            component.set("v.truthy",true);
            component.set("v.davis",false);
            component.set("v.wageParity",false);
        }else if( sel=="Davis Bacon" ||sel=="State Prevailing Wage Law" || sel=="Little Davis-Bacon Acts"){
            component.set("v.davis",true);
            component.set("v.truthy",false);
            component.set("v.wageParity",false);
        }else if(sel=="New York Wage Parity"){
            component.set("v.truthy",false);
            component.set("v.davis",false);
            component.set("v.wageParity",true);
        }else{
            component.set("v.truthy",false);
            component.set("v.davis",false);
            component.set("v.wageParity",false);
        }
    },
    weeklyhourRules:function(component, event, helper) {
        var sel = component.find("ContractorReports").get("v.value");
        if(sel=="Hours"){
            component.set("v.contractorReports",true);
        }else if(sel === "Pay Rate and Hours"){
            component.set("v.payRateAndHours", true);
            component.set("v.contractorReports",false);
        }else{
            component.set("v.payRateAndHours", false);
            component.set("v.contractorReports",false);   
        }     
    },
    weeklyWageParityFringeRate:function(component, event, helper) {
        var sel = component.find("WageParityFringeRate").get("v.value");
        if(sel=="Fringe Rate"){
            component.set("v.WageParityFringeRateType",true);
            component.set("v.contractorReports",true);//
            component.set("v.truthy",true);
            component.set("v.wageParity",false);
        }else if(sel === "WageParity PriceBook"){
            component.set("v.contractorReports",true);
            component.set("v.wageParity",true);
            component.set("v.truthy",false);
            component.set("v.WageParityFringeRateType",false);
        }     
    },
    tpaAdminCheckBox:function(component, event, helper) {
        var sel = component.find("TPAadmin").get("v.checked");
        if(sel=="false"){
            component.set("v.tpaAdmin",false);
        }
    },
    tpaPercentCost:function(component, event, helper) {
        var fcObjt = component.get("v.fcObjt");
        var sel = component.find("TPAAdminFee").get("v.value");
        //alert(sel);
        if(sel=="Percentage Fringe Contribution"){
            component.set("v.tpaAdminPercent",true);
            component.set("v.tpaAdminCost",false);
            component.set("v.fcObjt.TPA_Cost__c",0);
        }else if(sel=="Flat Dollar Amount(PEPM)"){
            component.set("v.tpaAdminCost",true);
            component.set("v.tpaAdminPercent",false);
            component.set("v.fcObjt.TPA_Percentage__c",0);
        }else{
            component.set("v.tpaAdminPercent",false);
            component.set("v.tpaAdminCost",false)
        }
    },
    // Allocation   
    monthlyCapChange:function(component, event, helper) {
        var sel = component.find("monthlyCap").get("v.checked");        
        var hide = component.find("hourlyDiv");
        if(sel==true){
            $A.util.addClass(hide,"slds-hide");
        }else{
            $A.util.removeClass(hide,"slds-hide");
        }            
    },
    hourlyCapChange:function(component, event, helper) {
        var sel = component.find("hourlyCap").get("v.checked");        
        var hide = component.find("monthlyDiv");
        if(sel==true){
            $A.util.addClass(hide,"slds-hide");
        }else{
            $A.util.removeClass(hide,"slds-hide");
        }            
    },
    //method A 
    onchangeMonthlyBenefits:function(component, event, helper) {
        var hide = component.find("HourlyBenefits");        
        $A.util.toggleClass(hide,"slds-hide");           
    },
    onchangeHourlyBenefits:function(component, event, helper) {
        var hide = component.find("monthlyBenefits");        
        $A.util.toggleClass(hide,"slds-hide");                     
    },
    //Method B month/hourly
    onchangeHourlyBenefitsB:function(component, event, helper) {
        var hide = component.find("monthlyBenefitsB");        
        $A.util.toggleClass(hide,"slds-hide");           
    },
    onchangeMonthlyBenefitsB:function(component, event, helper) {
        var hide = component.find("HourlyBenefitsB");        
        $A.util.toggleClass(hide,"slds-hide");                     
    },
    //MethodB Credit
    onchangeCreditRule1B:function(component, event, helper) {
        var hide = component.find("CreditRule2B");        
        $A.util.toggleClass(hide,"slds-hide");           
    },
    onchangeCreditRule2B:function(component, event, helper) {
        var hide = component.find("CreditRule1B");        
        $A.util.toggleClass(hide,"slds-hide");                     
    },
    //MethodB Debit
    onchangedebitRule1B:function(component, event, helper) {
        var hide = component.find("debitRule2B");        
        $A.util.toggleClass(hide,"slds-hide");           
    },
    onchangedebitRule2B:function(component, event, helper) {
        var hide = component.find("debitRule1B");        
        $A.util.toggleClass(hide,"slds-hide");                     
    },
    submitAddContact:function(component, event, helper) {
        var fcObjt = component.get("v.fcObjt");
        var isValid = false;
        var isValidPercent = true;
        let percetSum = 0;
        var results = [];
        //alert(fcObjt.Contract_Name__c);
        if(fcObjt.Contract_Name__c == null || fcObjt.Contract_Name__c == undefined || fcObjt.Contract_Name__c == ''){           
            var isValid = true;
        }
        if(fcObjt.Govt_Contractor__c == null || fcObjt.Govt_Contractor__c == undefined || fcObjt.Govt_Contractor__c == ''){           
            var isValid = true;
        }
        if(fcObjt.Contract_Type__c == null || fcObjt.Contract_Type__c == undefined || fcObjt.Contract_Type__c == ''){           
            var isValid = true;
        }
        if(fcObjt.Fr__c == null || fcObjt.Fr__c == undefined || fcObjt.Fr__c == ''){           
            var isValid = true;
        }        
        if(isValid == true){
            alert('Oops ! You cant proceed with empty required fields'); 
            return;            
        }
        var fcObjtBenefitsList = component.get("v.fcObjtBenefitsList");
        for (var i = 0; i < fcObjtBenefitsList.length; i++) {
            if(fcObjtBenefitsList[i].Percentage__c != null ){
                percetSum += parseInt(fcObjtBenefitsList[i].Percentage__c);
                isValidPercent = false;
            }            
        }
        if(isValidPercent == false){
            if(percetSum != 100){
                alert('Sum of all % should be 100 !!!'+'Total Value'+percetSum); 
                return
            }
        }
        helper.saveObjContractWizard(component, event);
    },
    //Add and remove row
    addRow: function(component, event, helper) {
        //var freqRow = component.get("v.freightChargesAir").find(row => row.Id === event.target.id);
        
        helper.addAccountRecord(component, event);
    },     
    removeRow: function(component, event, helper) {
        var fcObjtList = component.get("v.fcObjtBenefitsList");
        var selectedItem = event.currentTarget;
        var index = selectedItem.dataset.record;
        fcObjtList.splice(index, 1);
        component.set("v.fcObjtBenefitsList", fcObjtList);
    }, 
    save: function(component,event) {
        let percetSumUnderSpent = 0;
        let percetSumOverSpent = 0;
        let percetSumDebit1 = 0;
        let percetSumDebit2 = 0;
        let percentSumCredit1= 0;
        let percentSumCredit2= 0;
        let percetSum = 0;
        var isValid = true;
        var isValidUnder = false;
        var isValidOver = false; 
        var isValidDebit1 = false;
        var isValidDebit2 = false;
        var isValidCredit1 = false;
        var isValidCredit2 = false;
        var results = [];
        var fcObjtBenefitsList = component.get("v.fcObjtBenefitsList");
        for (var i = 0; i < fcObjtBenefitsList.length; i++) {
            if(fcObjtBenefitsList[i].Monthly_Fringe_Obligations__c =='UnderSpent'){
                percetSumUnderSpent += parseInt(fcObjtBenefitsList[i].Percentage__c);
                isValidUnder = true;             
            }
            for (var i = 0; i < fcObjtBenefitsList.length - 1; i++) {                
                if (fcObjtBenefitsList[i + 1].Individual_Fringe_Benefit_Account__c == fcObjtBenefitsList[i].Individual_Fringe_Benefit_Account__c) {
                    results.push(fcObjtBenefitsList[i].Individual_Fringe_Benefit_Account__c);
                    alert("Duplicate Value:  " + results);
                }
            }            
            if(fcObjtBenefitsList[i].Monthly_Fringe_Obligations__c =='Overspent'){
                percetSumOverSpent += parseInt(fcObjtBenefitsList[i].Percentage__c); 
                isValidOver = true;                
            } if(fcObjtBenefitsList[i].Credit_Debit_Rules__c == 'Debit 1'){
                percetSumDebit1 += parseInt(fcObjtBenefitsList[i].Percentage__c);
                isValidDebit1 = true;
            }
            if(fcObjtBenefitsList[i].Credit_Debit_Rules__c == 'Debit 2'){
                percetSumDebit2 += parseInt(fcObjtBenefitsList[i].Percentage__c);
                isValidDebit2 = true;
            }
            if(fcObjtBenefitsList[i].Credit_Debit_Rules__c == 'Credit 1'){
                percentSumCredit1 += parseInt(fcObjtBenefitsList[i].Percentage__c);
                isValidCredit1 = true;
            }
            if(fcObjtBenefitsList[i].Credit_Debit_Rules__c == 'Credit 2'){
                percentSumCredit2 += parseInt(fcObjtBenefitsList[i].Percentage__c);
                isValidCredit2 = true;
            }
            if(fcObjtBenefitsList[i].Percentage__c != null && fcObjtBenefitsList[i].Credit_Debit_Rules__c != 'Credit 1' && fcObjtBenefitsList[i].Credit_Debit_Rules__c != 'Credit 2' && fcObjtBenefitsList[i].Monthly_Fringe_Obligations__c !='UnderSpent' && fcObjtBenefitsList[i].Monthly_Fringe_Obligations__c !='Overspent' && fcObjtBenefitsList[i].Credit_Debit_Rules__c != 'Debit 1' && fcObjtBenefitsList[i].Credit_Debit_Rules__c != 'Debit 2'){
                percetSum += parseInt(fcObjtBenefitsList[i].Percentage__c);
                isValid = false;
            }            
        }
        if(isValidDebit1 == true && isValidDebit2 == true){
            alert('Both Debit Rules cannot be applicable at a time !!!');  
        }
        if(isValidCredit1 == true && isValidCredit2 == true){
            alert('Both Credit Rules cannot be applicable at a time !!!');  
        }
        if(isValidDebit1 == true){
            if(percetSumDebit1 != 100){
                alert('Sum of all % should be 100 for Debit Rules !!!');   
            }          
        }
        if(isValidDebit2 == true){
            if(percetSumDebit2 != 100){
                alert('Sum of all % should be 100 for Debit Rules !!!');   
            }          
        }
        if(isValidCredit1 == true ){
            if(percentSumCredit1 != 100){
                alert('Sum of all % should be 100 for Credit Rules !!!'); 
            }         
        }
        if(isValidCredit2 == true ){
            if(percentSumCredit2 != 100){
                alert('Sum of all % should be 100 for Credit Rules !!!'); 
            }         
        }
        if(isValidUnder == true){
            if(percetSumUnderSpent != 100){
                alert('Sum of all % should be 100 for Underspent !!!'+'Total Value'+percetSumUnderSpent);   
            }                    
        }
        if(isValidOver == true){            
            if(percetSumOverSpent != 100){
                alert('Sum of all % should be 100 for Overspent!!!'+'Total Value'+percetSumOverSpent);   
            } 
        }
        if(isValid == false){
            if(percetSum != 100){
                alert('Sum of all % should be 100 !!!'+'Total Value'+percetSum);   
            }}
    },
    debitCreditRulesValid: function(component, event, helper) {
        var isValidDebit1 = false;
        var isValidDebit2 = false;
        var isValidCredit1 = false;
        var isValidCredit2 = false;
        var fcObjtBenefitsList = component.get("v.fcObjtBenefitsList");
        for (var i = 0; i < fcObjtBenefitsList.length; i++) {            
            if(fcObjtBenefitsList[i].Credit_Debit_Rules__c == 'Debit 1'){
                isValidDebit1 = true;
            }
            if(fcObjtBenefitsList[i].Credit_Debit_Rules__c == 'Debit 2'){
                isValidDebit2 = true;
            }
            if(fcObjtBenefitsList[i].Credit_Debit_Rules__c == 'Credit 1'){
                isValidCredit1 = true;
            }
            if(fcObjtBenefitsList[i].Credit_Debit_Rules__c == 'Credit 2'){
                isValidCredit2 = true;
            }
        }
        if(isValidDebit1 == true && isValidDebit2 == true){
            alert('Both Debit Rules cannot be applicable at a time!!!');  
        }
        if(isValidCredit1 == true && isValidCredit2 == true){
            alert('Both Credit Rules cannot be applicable at a time!!!');  
        }
        //alert(target);        
    },
    percent: function(component, event, helper) {
        var target= event.getSource().get("v.name");
        var test1value= event.getSource().get("v.value");
        if(target=='test1'){
            component.set("v.individualOnePer", test1value); 
        }
        else if(target=='test2'){
            component.set("v.individualTwoPer", test1value); 
        }
        //alert(target);percent        
    },
    
})