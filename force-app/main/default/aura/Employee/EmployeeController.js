({
    doInit: function(component, event, helper) {
        
    },
    openNav : function(component, event, helper) {
        
        document.getElementById("mySidebar").style.width = "250px";
        document.getElementById("main").style.marginLeft = "250px";
        console.log("opennav");
    },
    
    closeNav : function(component, event, helper) {
        document.getElementById("mySidebar").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
        console.log("closenav");
    },
    
    getEmpyears : function(component, event, helper) {
        component.set("v.BenefitElection",true);
        component.set("v.HRAacc",false);
        component.set("v.Retirementacc",false);
        component.set("v.PremiumReserve",false);
        component.set("v.PaidLeaveacc",false);
        component.set("v.Hourbankacc",false);
        var action=component.get("c.getEmpYears");
        console.log('getempyears function crossed');
        var EmpId=component.get("v.EmpId");
        var empmonth=component.get("v.Empmonth");
        //alert('-----------------'+EmpId);
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state==="SUCCESS"){
                component.set("v.Empyears", response.getReturnValue());
                console.log('component set for years');
            }else{
                console.log(response.getError());
                console.log('error');
            }
        });
        $A.enqueueAction(action);
    },
    
    Onclick : function(component, event, helper){
        component.set("v.BenefitTable",true);
        //alert('after clicking submit button');
        var EmpId=component.get("v.EmpId") != null ?component.get("v.EmpId") :component.get("v.EmployeeId");
        //alert(EmpId);
        var empmonth=component.get("v.Empmonth");
        //alert(empmonth);
        var empyear=component.get("v.Empyear");
		//alert(empyear);        
        var monthyear= empmonth +' '+ empyear; 
        //alert(monthyear);
        component.set("v.BenefitMonth",monthyear);
        //console.log('month year  :'+monthyear);
        if(monthyear){
            var action = component.get("c.getEmployeeDatabyMonthYear");
            action.setParams({
                "EmployeeId":EmpId,
                "Monthyear": monthyear
            });
            action.setCallback(this,function(response){
                var state=response.getState();
                //alert('state----'+state);
                if(component.isValid() && state==="SUCCESS"){
                    //debugger;
                   
                        var resp = response.getReturnValue(); 
                     //alert('resp.Health_Insurance_Plan__c----'+resp.Health_Insurance_Plan__c);
                    component.set("v.EmpId",resp.Id);
                    component.set("v.EmpName",resp.name);
                    component.set("v.HRALinks",resp.HRA_Links);
                    component.set("v.RetirementLinks",resp.Retirement_Links);
                    component.set("v.PaidLeaveLinks",resp.PaidLeave_Links);
                    component.set("v.IPRALinks",resp.IPRA_Links);
                    component.set("v.EmpUniqueId",resp.UniqueId);
                    component.set("v.DOB__c",resp.DOB__c);//DOB__c
                    component.set("v.Gender__c",resp.Gender__c);//Gender__c
                    component.set("v.FirstName",resp.FirstName);
                    component.set("v.MiddleName",resp.MiddleName);
                    component.set("v.LastName",resp.LastName);
                    component.set("v.FringeContribution",resp.FringeContribution != null ? resp.FringeContribution :'0.00' );
                    component.set("v.PaidPeriod",resp.PaidPeriod);
                    component.set("v.ObligationResult",resp.ObligationResult);
                    component.set("v.IFBFringeRate",resp.IFBFringeRate);
                    component.set("v.Curr_Reg_Hrs_NYC__c",resp.Curr_Reg_Hrs_NYC__c);
                    component.set("v.Benefits_Effective_Date__c",resp.Benefits_Effective_Date__c);
                    component.set("v.Coveragetier",resp.Coveragetier);
                    component.set("v.CoverageEffectivefromDate",resp.CoverageEffectivefromDate);
                    component.set("v.CoverageEffectivetoDate",resp.CoverageEffectivetoDate);
                    component.set("v.Health_Insurance_Plan__c",resp.Health_Insurance_Plan__c);
                    component.set("v.Dental_Insurance_Plan__c",resp.Dental_Insurance_Plan__c);
                    component.set("v.Vision_Insurance_Plan__c",resp.Vision_Insurance_Plan__c);
                    component.set("v.Life_Insurance_Plan__c",resp.Life_Insurance_Plan__c);
                    component.set("v.Short_Term_Disability_Insurance__c",resp.Short_Term_Disability_Insurance__c);
                    component.set("v.Long_Term_Disability_Insurance__c",resp.Long_Term_Disability_Insurance__c);
                    component.set("v.Plan_1__c",resp.Plan_1__c);
                    component.set("v.Plan_2__c",resp.Plan_2__c);
                    component.set("v.Plan_3__c",resp.Plan_3__c);
                    component.set("v.Plan_4__c",resp.Plan_4__c);
                    component.set("v.Plan_5__c",resp.Plan_5__c);
                    //HRA,PaidLeave,Retirement,HourBankAccount,PremiumAccount
                    component.set("v.HRA",resp.HRA != null ? resp.HRA : '0.00');
                    component.set("v.PaidLeave",resp.PaidLeave != null ? resp.PaidLeave :'0.00' );
                    component.set("v.Retirement",resp.Retirement != null ? resp.Retirement : '0.00');
                    component.set("v.HourBankAccount",resp.HourBankAccount != null ? resp.HourBankAccount : '0.00');
                    component.set("v.PremiumAccount",resp.PremiumAccount != null ? resp.PremiumAccount : '0.00');
                    component.set("v.WithdrawIPRA",resp.WithdrawIPRA != null ? resp.WithdrawIPRA : '0.00' );
                    component.set("v.MonthlyIPRA",resp.MonthlyIPRA != null ? resp.MonthlyIPRA : '0.00');
                    component.set("v.Dental_Insurance_Plan_Value",resp.Dental_Insurance_Plan_Value != null ? resp.Dental_Insurance_Plan_Value : '0.00' );
                    component.set("v.Health_Insurance_Plan_Value",resp.Health_Insurance_Plan_Value != null ? resp.Health_Insurance_Plan_Value : '0.00');
                    component.set("v.Vision_Insurance_Plan_Value",resp.Vision_Insurance_Plan_Value != null ? resp.Vision_Insurance_Plan_Value : '0.00' );
                    component.set("v.Life_Insurance_Plan_Value",resp.Life_Insurance_Plan_Value != null ? resp.Life_Insurance_Plan_Value : '0.00' );
                    component.set("v.Short_Term_Disability_Insurance_Value",resp.Short_Term_Disability_Insurance_Value != null ? resp.Short_Term_Disability_Insurance_Value : '0.00');
                    component.set("v.Long_Term_Disability_Insurance_Value",resp.Long_Term_Disability_Insurance_Value != null ? resp.Long_Term_Disability_Insurance_Value : '0.00');
                    component.set("v.Monthly_Cost_Of_Benefits__c",resp.Monthly_Cost_Of_Benefits__c != null ? resp.Monthly_Cost_Of_Benefits__c : '0.00');
                }else{
                    console.log(response.getError());
                    console.log('error');
                }
            });
            $A.enqueueAction(action);
        }
    },
    
    hraclicked : function(component, event, helper){
        component.set("v.BenefitElection",false);
        component.set("v.HRAacc",true);
        component.set("v.Retirementacc",false);
        component.set("v.PremiumReserve",false);
        component.set("v.PaidLeaveacc",false);
        component.set("v.Hourbankacc",false); 
        document.getElementById("mySidebar").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
        console.log("closenav");
        
    },
    
    benefitelectionsclicked : function(component, event, helper){
        component.set("v.BenefitElection",true);
        component.set("v.HRAacc",false);
        component.set("v.Retirementacc",false);
        component.set("v.PremiumReserve",false);
        component.set("v.PaidLeaveacc",false);
        component.set("v.Hourbankacc",false); 
        document.getElementById("mySidebar").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
        console.log("closenav");
        
    },
    retirementclicked : function(component, event, helper){
        component.set("v.BenefitElection",false);
        component.set("v.HRAacc",false);
        component.set("v.Retirementacc",true);
        component.set("v.PremiumReserve",false);
        component.set("v.PaidLeaveacc",false);
        component.set("v.Hourbankacc",false); 
        document.getElementById("mySidebar").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
        console.log("closenav");
        
    },
    PRAclicked : function(component, event, helper){
        component.set("v.BenefitElection",false);
        component.set("v.HRAacc",false);
        component.set("v.Retirementacc",false);
        component.set("v.PremiumReserve",true);
        component.set("v.PaidLeaveacc",false);
        component.set("v.Hourbankacc",false); 
        document.getElementById("mySidebar").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
        console.log("closenav");
        
    },
    Paidleaveclicked : function(component, event, helper){
        component.set("v.BenefitElection",false);
        component.set("v.HRAacc",false);
        component.set("v.Retirementacc",false);
        component.set("v.PremiumReserve",false);
        component.set("v.PaidLeaveacc",true);
        component.set("v.Hourbankacc",false); 
        document.getElementById("mySidebar").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
        console.log("closenav");        
    },
    Hourbankclicked : function(component, event, helper){
        component.set("v.BenefitElection",false);
        component.set("v.HRAacc",false);
        component.set("v.Retirementacc",false);
        component.set("v.PremiumReserve",false);
        component.set("v.PaidLeaveacc",false);
        component.set("v.Hourbankacc",true); 
        document.getElementById("mySidebar").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
        console.log("closenav");        
    },
    //Open URL in New Browser Tab
    handleOpenInNewWindow : function(component, event, helper) {
        window.open(component.get("v.HRALinks"), '_blank');
    },
    handleOpenRedInNewWindow : function(component, event, helper) {
        window.open(component.get("v.RetirementLinks"), '_blank');
    },
    
})