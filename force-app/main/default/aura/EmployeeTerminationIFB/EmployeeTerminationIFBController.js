({
	createIFB : function(component, event, helper) {
        //alert('======='+component.get("v.recordId"));
        var action = component.get("c.updateAllValuesToZero");
        action.setParams({
            "empId":component.get("v.recordId"),
        });
        action.setCallback(this,function(response){
            var state=response.getState();
            if(component.isValid() && state==="SUCCESS"){
                var result = response.getReturnValue();
                if(result.success){
                    component.set("v.successData",true);
                }else if(result.failed){
                    component.set("v.successData",true);
                    component.set("v.excep",result.failed);
                    //result.failed
                    //excep
                }else if(result.RecordFound){
                    component.set("v.recordFound",true);
                }
                //component.destroy();
            }
        });
        $A.enqueueAction(action);
		
	}
})