global with sharing class EmployeeMonthlyDetailsUpdate implements Database.batchable<Id>,Database.Stateful,Database.AllowsCallouts{ 
    List<Id> recordList = new List<Id>();
    
    global EmployeeMonthlyDetailsUpdate(List<Id> recordIdList){
        recordList = recordIdList;
    }
    global List<Id> start(Database.BatchableContext BC){
        return recordList;
    }
    
    global void execute(Database.BatchableContext BC, List<Id> recordList){
        if(recordList.size() > 0){       
            for(Id recordId : recordList){
                String monthAndYear;
                EmployeeMonthlyCalculationHandler.updateEmployeeFromMonthlyHours(recordId, monthAndYear);
                // Method_c
                //System.debug('Method c working');
                //MethodC.method(recordId);
                // Method_d
                //MethodDHandler.methodD(recordId);
            }
        }
    }
    global void finish(Database.BatchableContext BC){
      
    }
}