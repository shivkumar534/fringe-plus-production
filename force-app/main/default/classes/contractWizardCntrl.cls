//<!--
//@author Mohammad Shoeb 
//*/-->
public class contractWizardCntrl {
    
    @AuraEnabled 
    Public static string createContractWizardSetup(Fringe_Contract__c objContract,List<Individual_Fringe_Benefits_Percentages__c> fcObjtBenefitsList){
        system.debug('>>>>>'+objContract);
        system.debug('>>>>>'+fcObjtBenefitsList);
        List<Individual_Fringe_Benefits_Percentages__c> individList = new List<Individual_Fringe_Benefits_Percentages__c>();
        //Map<Id,Fringe_Contract__c> fObjMap = new Map<Id, Fringe_Contract__c>();  
        try{
            insert objContract;
            for(Individual_Fringe_Benefits_Percentages__c fringeBene :fcObjtBenefitsList){
                Individual_Fringe_Benefits_Percentages__c individualBenefits =new Individual_Fringe_Benefits_Percentages__c();
                individualBenefits.Fringe_Contract__c = objContract.Id;
                individualBenefits.Individual_Fringe_Benefit_Account__c =fringeBene.Individual_Fringe_Benefit_Account__c;
                individualBenefits.Percentage__c = fringeBene.Percentage__c;
                //individualBenefits.Monthly_Fringe_Obligations__c = fringeBene.Monthly_Fringe_Obligations__c;
                //individualBenefits.Credit_Debit_Rules__c = fringeBene.Credit_Debit_Rules__c;
                individList.add(individualBenefits);            
            }
            if(individList!=null && !individList.isEmpty())
                insert individList;                
            
        }        
        catch(Exception e){
            System.debug(e.getMessage());
            System.debug(e.getLineNumber());
        }
        return objContract.Id;
    }
    @auraEnabled
    public static Fringe_Allocation_Rules__c fetchAllocationDetails( Id AllocationId) {
        return [Select Id,Name,FringeBenefits__c,UnderspentCategory__c,OverspentCategory__c From Fringe_Allocation_Rules__c  Where Id =: AllocationId];   
    }  
    
    @AuraEnabled
    public static String getLabelEmployee() {        
        List<String> plValues = new List<String>();
        Schema.SObjectType objType = Schema.getGlobalDescribe().get('Employee__c');
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        Schema.DescribeFieldResult objFieldInfo = objDescribe.fields.getMap().get('Plan_1__c').getDescribe();
        String fieldTypeName = Schema.getGlobalDescribe().get('Employee__c').getDescribe().fields.getMap().get('Plan_1__c').getDescribe().getLabel(); 
        System.debug('*** fieldTypeName = ' + fieldTypeName);
        return fieldTypeName;
        
    }
    
    @AuraEnabled
    public static List <String> getPiklistValues() {
        List<String> plValues = new List<String>();
        Schema.SObjectType objType = Schema.getGlobalDescribe().get('Fringe_Contract__c');
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        Schema.DescribeFieldResult objFieldInfo = objDescribe.fields.getMap().get('Benefit_Plan__c').getDescribe();
        List<Schema.PicklistEntry> picklistvalues = objFieldInfo.getPicklistValues();
        for(Schema.PicklistEntry plv: picklistvalues) {
            plValues.add(plv.getValue());
        }
        plValues.sort();
        return plValues;
    }
    @AuraEnabled
    public static List <String> getFringeMultiLocation() {
        List<String> plValues = new List<String>();
        Schema.SObjectType objType = Schema.getGlobalDescribe().get('Fringe_Contract__c');
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        Schema.DescribeFieldResult objFieldInfo = objDescribe.fields.getMap().get('Benefit_Plan__c').getDescribe();
        List<Schema.PicklistEntry> picklistvalues = objFieldInfo.getPicklistValues();
        for(Schema.PicklistEntry plv: picklistvalues) {
            plValues.add(plv.getValue());
        }
        plValues.sort();
        return plValues;
    }
    @AuraEnabled     
    public static Map<String, String> getcontractType(){
        Map<String, String> options = new Map<String, String>();        
        Schema.DescribeFieldResult fieldResult = Fringe_Contract__c.Contract_Type__c.getDescribe();        
        List<Schema.PicklistEntry> pValues = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pValues) {            
            options.put(p.getValue(), p.getLabel());
        }
        return options;
    }   
    
    @AuraEnabled     
    public static Map<String, String> getFringeAccRule(){
        Map<String, String> options = new Map<String, String>();        
        Schema.DescribeFieldResult fieldResult = Fringe_Contract__c.Fr__c.getDescribe();        
        List<Schema.PicklistEntry> pValues = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pValues) {            
            options.put(p.getValue(), p.getLabel());
        }
        return options;
    }
    @AuraEnabled     
    public static Map<String, String> getFringeAllocationMethd(){
        Map<String, String> options = new Map<String, String>();        
        Schema.DescribeFieldResult fieldResult = Fringe_Contract__c.Fr__c.getDescribe();        
        List<Schema.PicklistEntry> pValues = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pValues) {            
            options.put(p.getValue(), p.getLabel());
        }
        return options;
    }
    @AuraEnabled     
    public static Map<String, String> getFringeMonthly(){
        Map<String, String> options = new Map<String, String>();        
        Schema.DescribeFieldResult fieldResult = Fringe_Contract__c.Monthly_Hours_Report__c.getDescribe();        
        List<Schema.PicklistEntry> pValues = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pValues) {            
            options.put(p.getValue(), p.getLabel());
        }
        return options;
    }
    @AuraEnabled
    public static Individual_Fringe_Benfit__c getindividualBenefits(Id recId) {
        return [Select id,name,HRA__c,Retirement__c,Rolling_Hour_Bank_Account__c,Employee__r.Unique_Identifier__c,Employee__r.Job_Site_Locations__r.Name,Employee__r.Govt_Contractor__r.Name,Employee__r.Fringe_Contract_Type__r.Contract_Name__c,Employee__r.Coverage_Tier__c,Employee__r.Health_Insurance_Plan__c,Employee__r.Dental_Insurance_Plan__c,Employee__r.Vision_Insurance_Plan__c,Employee__r.Name,Employee__r.Id,Employee__r.Month_and_Year__c,Employee__r.Fringe_Contract_Type__r.Name From Individual_Fringe_Benfit__c Where Employee__r.Id =: recId  order BY CreatedDate desc limit 1];   
    }
    @AuraEnabled 
    Public static void createBenefits(Individual_Fringe_Benfit__c objBenefits,Employee__c empObj){
        system.debug('>>>>>'+objBenefits);
        system.debug('>>>>>empObj'+empObj);
        try{
            insert objBenefits;
            empObj.Id = objBenefits.Employee__c;
            update empObj;
        }catch(Exception e){
            System.debug(e.getMessage());
            System.debug(e.getLineNumber());
        } 
    }
    @AuraEnabled 
    Public static void getAvgQtrDetails(Id fId, string qtr){
        List<Id> fcId = new List<Id>();
        List<Id> empIds = new List<Id>();
        List<Fringe_Contract__c> fcList =[SELECT Id, Fringe_Allocation_Rules__r.Id, Set_Maximum_Funding_Level__c,Individual_Premium_Reserve_Account__c,Fringe_Allocation_Rules__r.Name, Fringe_Allocation_Rules__r.FringeBenefits__c, Fringe_Allocation_Rules__r.OverspentCategory__c, Fringe_Allocation_Rules__r.UnderspentCategory__c,Average_Cost_Accounting_Methods__c,Fr__c FROM Fringe_Contract__c WHERE Id =:fId];
        for(Fringe_Contract__c fc : fcList){
            fcId.add(fc.Id);
        }
        if(qtr =='Q1'){
            for(Employee__c emp: [SELECT Id, Name,Curr_Reg_Hrs_NYC__c,Average_Cost_Accounting__c,Month_and_Year__c,Fringe_Contract_Type__r.Fr__c FROM Employee__c WHERE Fringe_Contract_Type__c in : fcId]){
                if(emp.Month_and_Year__c.contains('March') || emp.Month_and_Year__c.contains('February') || emp.Month_and_Year__c.contains('January')){
                    empIds.add(emp.Id);  
                }               
            }
        }else if(qtr =='Q2'){
            for(Employee__c emp: [SELECT Id, Name,Curr_Reg_Hrs_NYC__c,Average_Cost_Accounting__c,Month_and_Year__c,Fringe_Contract_Type__r.Fr__c FROM Employee__c WHERE Fringe_Contract_Type__c in : fcId]){
                if(emp.Month_and_Year__c.contains('April') || emp.Month_and_Year__c.contains('May')  ||emp.Month_and_Year__c.contains('June')){
                    empIds.add(emp.Id);    
                }               
            }            
        }else if(qtr =='Q3'){            
            for(Employee__c emp: [SELECT Id, Name,Curr_Reg_Hrs_NYC__c,Average_Cost_Accounting__c,Month_and_Year__c,Fringe_Contract_Type__r.Fr__c FROM Employee__c WHERE Fringe_Contract_Type__c in : fcId]){
                if(emp.Month_and_Year__c.contains('August') || emp.Month_and_Year__c.contains('September') || emp.Month_and_Year__c.contains('July')){
                    empIds.add(emp.Id);    
                }               
            }
        }else if(qtr =='Q4'){            
            for(Employee__c emp: [SELECT Id, Name,Curr_Reg_Hrs_NYC__c,Average_Cost_Accounting__c,Month_and_Year__c,Fringe_Contract_Type__r.Fr__c FROM Employee__c WHERE Fringe_Contract_Type__c in : fcId]){
                if(emp.Month_and_Year__c.contains('October') || emp.Month_and_Year__c.contains('November') || emp.Month_and_Year__c.contains('December')){
                    empIds.add(emp.Id);    
                }               
            }
        } 
        if(empIds.size() > 0)
            Database.executeBatch(new avgQtrCostBatchClass(empIds), 1);
    }
}