global class EmployeeUpdateOnCOBUpdate implements Database.batchable<Id>,Database.Stateful,Database.AllowsCallouts{ 
    List<Id> recordList = new List<Id>();
    
    global EmployeeUpdateOnCOBUpdate(List<Id> recordIdList){
        recordList = recordIdList;
    }
    global List<Id> start(Database.BatchableContext BC){
        return recordList;
    }
    
    global void execute(Database.BatchableContext BC, List<Id> recordList){
        if(recordList.size() > 0){       
            for(Id recordId : recordList){
                updateEmployeeCOB(recordId);
            }
        }
    }
    global void finish(Database.BatchableContext BC){
        
    }
    
    public static void updateEmployeeCOB(Id empId){
        Decimal monthlycostOfBenefit;
        try{
            Employee__c emp = [SELECT Id,Monthly_Fringe_Contribution__c,After_Retro_Cost_Of_Benefit_Adjustments__c,Fringe_Contract__c,Fringe_Contract_Type__r.Fr__c,Coverage_Tier__c,Monthly_fringe_obligtion_result__c,Health_Insurance_Plan__c,Dental_Insurance_Plan__c,Life_Insurance_Plan__c,Vision_Insurance_Plan__c,Plan_1__c,Plan_2__c,Plan_3__c,Name,Actual_Monthly_Hours__c,Fringe_Rate_lookup__c, Curr_Reg_Hrs_NYC__c,Monthly_Cost_Of_Benefits__c, Fringe_Contract_Type__r.Hour_Bank__c,Benefit_Amount_Allocated__c,Fringe_Contract_Type__r.Fringe_Allocation_Method__c, Roll_Back_Hours__c,Fringe_Contract_Type__r.Id FROM Employee__c WHERE Id =: empId];
            List<String> cobList = new List<String>{emp.Health_Insurance_Plan__c,emp.Dental_Insurance_Plan__c,emp.Life_Insurance_Plan__c,emp.Vision_Insurance_Plan__c,emp.Plan_1__c,emp.Plan_2__c,emp.Plan_3__c};
                System.debug('----'+cobList);
            for(PricebookEntry pbe :[SELECT Id, Name, Pricebook2Id, Product2Id, UnitPrice, IsActive,Product2.TPA_Fee_on_Premium_Rat__c,Product2.TPA_Fee_PEPM__c,Product2.TPA_FEE_Per_Hour_Per_Employee__c, ProductCode FROM PricebookEntry WHERE Pricebook2.Fringe_Contract__c =: emp.Fringe_Contract_Type__c AND ProductCode IN: cobList AND Name =: emp.Coverage_Tier__c]){
                if(pbe != null){
                    if(monthlycostOfBenefit == 0){
                        if(pbe.Product2.TPA_Fee_on_Premium_Rat__c != null){
                            monthlycostOfBenefit = pbe.UnitPrice +(pbe.UnitPrice * pbe.Product2.TPA_Fee_on_Premium_Rat__c)/100;
                        }else if(pbe.Product2.TPA_Fee_PEPM__c != null){
                            monthlycostOfBenefit = pbe.UnitPrice + pbe.Product2.TPA_Fee_PEPM__c;
                        }else if(pbe.Product2.TPA_FEE_Per_Hour_Per_Employee__c != null){
                            monthlycostOfBenefit = pbe.UnitPrice + pbe.Product2.TPA_FEE_Per_Hour_Per_Employee__c;
                        }else{
                            monthlycostOfBenefit = pbe.UnitPrice;
                        }
                    }else{
                        if(pbe.Product2.TPA_Fee_on_Premium_Rat__c != null){
                            monthlycostOfBenefit += (pbe.UnitPrice * pbe.Product2.TPA_Fee_on_Premium_Rat__c)/100;
                        }else if(pbe.Product2.TPA_Fee_PEPM__c != null){
                            monthlycostOfBenefit += pbe.UnitPrice + pbe.Product2.TPA_Fee_PEPM__c;
                        }else if(pbe.Product2.TPA_FEE_Per_Hour_Per_Employee__c != null){
                            monthlycostOfBenefit += pbe.UnitPrice + pbe.Product2.TPA_FEE_Per_Hour_Per_Employee__c;
                        } else{
                            monthlycostOfBenefit += pbe.UnitPrice;
                        }
                    }
                }
            }
            emp.Monthly_Cost_Of_Benefit__c = monthlycostOfBenefit;
            
            try{ update emp;}catch(Exception e){e.getMessage();}
        }catch(Exception e){e.getMessage();}
    }
}