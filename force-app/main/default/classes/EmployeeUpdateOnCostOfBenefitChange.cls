global class EmployeeUpdateOnCostOfBenefitChange implements Database.batchable<Id>,Database.Stateful,Database.AllowsCallouts{ 
    public static  boolean firstRun = true;
    List<Id> recordList = new List<Id>();
	List<Id> contractIdList = new List<Id>(); 
    List<String> monthAndYearList = new List<String>();
    Map<Id,Id> pbMap = new Map<Id,Id>();
    global EmployeeUpdateOnCostOfBenefitChange(List<String> monthAndyear,List<Id> recordIdList){
        
        for(Monthly_Hours__c mon : [SELECT Id, Name, EE__c,Job_Classification_Zip_code__c,Employee__c,Month_and_Year__c,Cost_Of_Benefit__c,Job_Classification__c,Zip_Code__c,Fringe_Contract__c, Coverage_Tier__c,Curr_Reg_Hrs_NYC__c FROM Monthly_Hours__c WHERE Employee__c =: recordIdList AND Month_and_Year__c =: monthAndyear]){
            recordList.add(mon.Id);
        }
        
    }
    global List<Id> start(Database.BatchableContext BC){
        return recordList;
    }
    
    global void execute(Database.BatchableContext BC, List<Id> recordList){
        if(recordList.size() > 0){       
            for(Id recordId : recordList){
                EmployeeMonthlyCalculationHandler.updateEmployeeFromMonthlyHours(recordId, ''); 
                //updateEmployeeFringeRate(recordId);
            }
        }
    }
    global void finish(Database.BatchableContext BC){
      
    }
    
}