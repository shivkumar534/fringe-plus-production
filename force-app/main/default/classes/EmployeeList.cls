public class EmployeeList {
    @AuraEnabled
        public static List<String> getData(){
        List<String> options= new List<String>();
        
        for(Account acc:[select id,name from Account]){
            options.add(acc.name);
        }
        return options;
    }
}