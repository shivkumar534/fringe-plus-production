public class UpdateIndividualFringeBenefits {
    
   /* public static void method1(List<String> empIds) {
        
        List<Employee__c> empList = new List<Employee__c>();
        Map<String, Decimal> monthlyHoursregMap = new Map<String, Decimal>();
        Map<String, Employee__c> monthlyOrWeeklyHoursMap = new Map<String, Employee__c>();
        Map<String, String> monthAndYear = new Map<String, String>();
        Map<String, decimal> fringecontractBH = new  Map<String, decimal>();
        Map<String, decimal> fringeEmpCob = new  Map<String, decimal>();
        List<Hours_Bank_Account__c> hbaList = new List<Hours_Bank_Account__c>();
        for(Monthly_Hours__c mh : [SELECT Id, Name, EE__c,Month_and_Year__c, Curr_Reg_Hrs_NYC__c FROM Monthly_Hours__c WHERE EE__c In: empIds]){
            monthlyHoursregMap.put(mh.EE__c, mh.Curr_Reg_Hrs_NYC__c);
            monthAndYear.put(mh.EE__c,mh.Month_and_Year__c);
        }
        for(Employee__c emp: [SELECT Id, Name,Actual_Monthly_Hours__c,Fringe_Rate_lookup__c, Curr_Reg_Hrs_NYC__c,Monthly_Cost_Of_Benefits__c, Fringe_Contract_Type__r.Capping_Hours__c, Fringe_Contract_Type__r.Fringe_Allocation_Method__c, Roll_Back_Hours__c FROM Employee__c WHERE Name IN: empIds]){
            if(empIds.contains(emp.Name)){
                emp.Curr_Reg_Hrs_NYC__c = monthlyHoursregMap.get(emp.Name) > emp.Fringe_Contract_Type__r.Capping_Hours__c ? emp.Fringe_Contract_Type__r.Capping_Hours__c : monthlyHoursregMap.get(emp.Name);
                emp.Actual_Monthly_Hours__c = monthlyHoursregMap.get(emp.Name);
                //emp.Roll_Back_Hours__c = monthlyHoursregMap.get(emp.Name) > emp.Fringe_Contract_Type__r.Hour_Bank__c ? (monthlyHoursregMap.get(emp.Name) - emp.Fringe_Contract_Type__r.Hour_Bank__c)+emp.Roll_Back_Hours__c  : emp.Roll_Back_Hours__c + (monthlyHoursregMap.get(emp.Name) - emp.Fringe_Contract_Type__r.Hour_Bank__c);
                monthlyOrWeeklyHoursMap.put(emp.Name, emp);
                fringeEmpCob.put(emp.Name, emp.Monthly_Cost_Of_Benefits__c);
                fringecontractBH.put(emp.Name, emp.Fringe_Contract_Type__r.Capping_Hours__c);
                empList.add(emp);
            }
        }   
        
        try{
            if(empList.size() > 0){
                Upsert empList;
            }
        }catch(Exception e){System.debug('---exception---'+e.getMessage());}
        
        
        integer count = 1;
        integer recordcount =empIds.size();
        Map<Integer,String> monthNameMap = new Map<Integer, String>{1 =>'January', 2=>'February', 3=>'March', 4=>'April', 5=>'May',6=>'June', 7=>'July', 8=>'August', 9=>'September',10=>'October',11=>'November', 12=>'December'};
        Map<Id,Employee__c> empMap = new Map<Id, Employee__c>();
        List<Individual_Fringe_Benfit__c> ifbList = new List<Individual_Fringe_Benfit__c>();
        for(Individual_Fringe_Benfit__c ifb: [SELECT Id,Employee__c,HRA__c,Individual_Premium_Reserve__c,Insurance__c,Current_Balance_Of_Individual_Premium_Ac__c,Medical__c,Retirement__c,Month_and_Year__c,Rolling_HRA__c,
                                              Employee__r.Monthly_Fringe_Amount_Allocated__c,employee__r.Fringe_Contract_Type__r.Credit_Rule__c,employee__r.Fringe_Contract_Type__r.Debit_Rule__c,employee__r.Fringe_Contract_Type__r.Credit_Rule_2__c,employee__r.Fringe_Contract_Type__r.Credit_Rule_1__c,employee__r.Fringe_Contract_Type__r.Debit_Rule_2__c,
                                              employee__r.Fringe_Contract_Type__r.Debit_Rule_1__c,Rolling_Individual_Premium_Reserve__c,Rolling_Insurance__c,Rolling_Medical__c,Rolling_Retirement__c, employee__r.Status__c
                                               FROM Individual_Fringe_Benfit__c 
                                             where Month_and_Year__c =: monthNameMap.get(System.now().month())+' '+System.now().year() AND Employee__r.Name in : empIds ]) {
                                                  
                                                  if(count <= recordcount){
                                                      empMap.put(ifb.Employee__r.Id, ifb.Employee__r );
                                                      if(ifb.Employee__r.Status__c == 'Underspent' && ifb.employee__r.Fringe_Contract_Type__r.Credit_Rule__c == 'Credit Rule 1'){
                                                           ifb.Individual_Premium_Reserve__c = ifb.Individual_Premium_Reserve__c != null ?ifb.Individual_Premium_Reserve__c + ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c : ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c;
                                     						ifbList.add(ifb);
                                                      }
                                                      else if(ifb.Employee__r.Status__c == 'Underspent' && ifb.employee__r.Fringe_Contract_Type__r.Credit_Rule__c == 'Credit Rule 2') {
                                                          ifb.Individual_Premium_Reserve__c =  ifb.Individual_Premium_Reserve__c != null ? ifb.Individual_Premium_Reserve__c + (ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*60)/100 : (ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*60)/100;
                                                          ifb.HRA__c      = (ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*10)/100;
                                                          ifb.Insurance__c = (ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*10)/100;
                                                          ifb.Medical__c         = (ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*10)/100;
                                                          ifb.Retirement__c         = (ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*10)/100;
                                                          ifbList.add(ifb);
                                                      }else 
                                                      if(ifb.Employee__r.Status__c == 'Overspent' && ifb.employee__r.Fringe_Contract_Type__r.Debit_Rule__c == 'Debit Rule 1'){
                                                          System.debug('Status :'+ifb.Employee__r.Status__c);
                                                          //System.debug('Debit Rule-1 : '+ifb.Employee__r.Debit_1__c);
                                                          system.debug('Monthly fringe Amount allocated : '+ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c);
                                                          system.debug('Current balance of Individual premium Account : '+ifb.Current_Balance_Of_Individual_Premium_Ac__c);
                                                          ifb.Individual_Premium_Reserve__c = ifb.Individual_Premium_Reserve__c + ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c;
                                                          ifb.Individual_Primum_Account__c = ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c;
                                                          ifbList.add(ifb);
                                                      }    
                                                      else if(ifb.Employee__r.Status__c == 'Overspent'  && ifb.employee__r.Fringe_Contract_Type__r.Debit_Rule__c == 'Debit Rule 2'){
                                                          ifb.Individual_Premium_Reserve__c = (ifb.Individual_Premium_Reserve__c + (ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*60)/100)-((ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*10)/100+(ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*10)/100+(ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*10)/100+(ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*10)/100);
                                                          ifb.HRA__c      = (ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*10)/100;
                                                          ifb.Insurance__c = (ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*10)/100;
                                                          ifb.Medical__c         = (ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*10)/100;
                                                          ifb.Retirement__c         = (ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*10)/100;
                                                          ifbList.add(ifb);
                                                      }      
                                                      System.debug('------'+ifb);
                                                      count++;
                                                      
                                                         
                                                  }
                                                      
                                                  
                                              }
        
        if(count <= recordCount){
            for(Employee__c famIds: [SELECT Id,Name,Status__c, Fringe_Contract_Type__r.Credit_Rule_1__c,Fringe_Contract_Type__r.Debit_Rule_1__c,Fringe_Contract_Type__r.Debit_Rule_2__c,
                                     Fringe_Contract_Type__r.Credit_Rule__c,Fringe_Contract_Type__r.Debit_Rule__c, Fringe_Contract_Type__r.Credit_Rule_2__c,Monthly_Fringe_Amount_Allocated__c FROM Employee__c WHERE Name IN: empIds]){
                if(!empMap.containsKey(famIds.Id)){
                    System.debug('-----'+famIds.Status__c+'     '+famIds.Fringe_Contract_Type__r.Credit_Rule__c);
                    Individual_Fringe_Benfit__c ifbinsert = new Individual_Fringe_Benfit__c();
                    ifbinsert.Employee__c = famIds.Id;
                    ifbinsert.Individual_Primum_Account__c = famIds.Monthly_Fringe_Amount_Allocated__c;
                    ifbinsert.Month_and_Year__c = monthNameMap.get(System.now().month())+' '+System.now().year();
                    if(famIds.Status__c == 'Underspent' && famIds.Fringe_Contract_Type__r.Credit_Rule__c == 'Credit Rule 1'){
                        ifbinsert.Individual_Premium_Reserve__c = famIds.Monthly_Fringe_Amount_Allocated__c;
                        ifbList.add(ifbinsert);
                    }else if(famIds.Status__c == 'Underspent' && famIds.Fringe_Contract_Type__r.Credit_Rule__c == 'Credit Rule 2') {
                        ifbinsert.Individual_Premium_Reserve__c = ((famIds.Monthly_Fringe_Amount_Allocated__c*60)/100) - (((famIds.Monthly_Fringe_Amount_Allocated__c*10)/100)*4) ;
                        ifbinsert.HRA__c      = (famIds.Monthly_Fringe_Amount_Allocated__c*10)/100;
                        ifbinsert.Insurance__c = (famIds.Monthly_Fringe_Amount_Allocated__c*10)/100;
                        ifbinsert.Medical__c         = (famIds.Monthly_Fringe_Amount_Allocated__c*10)/100;
                        ifbinsert.Retirement__c         = (famIds.Monthly_Fringe_Amount_Allocated__c*10)/100;
                        ifbList.add(ifbinsert);
                    }else if(famIds.Status__c == 'Overspent' && famIds.Fringe_Contract_Type__r.Debit_Rule__c == 'Debit Rule 1'){
                        System.debug('Status :'+famIds.Status__c);
                        system.debug('Monthly fringe Amount allocated : '+famIds.Monthly_Fringe_Amount_Allocated__c);
                        //system.debug('Current balance of Individual premium Account : '+famIds.Current_Balance_Of_Individual_Premium_Ac__c);
                        ifbinsert.Individual_Primum_Account__c = famIds.Monthly_Fringe_Amount_Allocated__c;
                        ifbList.add(ifbinsert);
                    }    
                    else if(famIds.Status__c == 'Overspent'  && famIds.Fringe_Contract_Type__r.Debit_Rule__c == 'Debit Rule 2'){
                        ifbinsert.Individual_Premium_Reserve__c = (famIds.Monthly_Fringe_Amount_Allocated__c*60)/100;
                        ifbinsert.HRA__c      = (famIds.Monthly_Fringe_Amount_Allocated__c*10)/100;
                        ifbinsert.Insurance__c = (famIds.Monthly_Fringe_Amount_Allocated__c*10)/100;
                        ifbinsert.Medical__c         = (famIds.Monthly_Fringe_Amount_Allocated__c*10)/100;
                        ifbinsert.Retirement__c         = (famIds.Monthly_Fringe_Amount_Allocated__c*10)/100;
                        ifbList.add(ifbinsert);
                    } 
                   
                    
                }
                
                count++;
            }
        }
        
        if(ifbList.size() > 0){
            try{
                upsert ifbList;
            }catch(Exception e){e.getMessage();}
        }
    }
    */
}