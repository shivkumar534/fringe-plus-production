global with sharing class MethodDbatchclass implements Database.batchable<Id>,Database.Stateful,Database.AllowsCallouts{ 
    List<Id> recordList = new List<Id>();
    
    global MethodDbatchclass(List<Id> recordIdList){
        recordList = recordIdList;
    }
    global List<Id> start(Database.BatchableContext BC){
        return recordList;
    }
    
    global void execute(Database.BatchableContext BC, List<Id> recordList){
        if(recordList.size() > 0){       
            for(Id recordId : recordList){
                System.debug('This method-D from Batch class');
                MethodDHandler.methodD(recordId);
            }
        }
    }
    global void finish(Database.BatchableContext BC){
      
    }
}