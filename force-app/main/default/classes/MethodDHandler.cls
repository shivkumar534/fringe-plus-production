// Author : Diwakar
// Date : 21th April 2021
public class MethodDHandler {
    public static void methodD(Id monthlyHourId) {
        System.debug('This is From Method D Handler');
        System.debug('Monthly Hours id is :'+monthlyHourId);
        String empUniqId,EmployeeId,listOfFields,EmpName,benefitmonth;
        decimal downlevel,MonthHours,fringeRate,HourlyCOB = 0,HourlyCOB1 = 0,obligationResult = 0, benefitTotal = 0;
        String contractId, Month_and_Year;
        Monthly_Hours__c mon = [SELECT Id, Name, EE__c,Benefit_Period__c,Job_Classification_Zip_code__c,Month_and_Year__c,Cost_Of_Benefit__c,Job_Classification__c,Zip_Code__c,Fringe_Contract__c, Coverage_Tier__c,Curr_Reg_Hrs_NYC__c FROM Monthly_Hours__c WHERE Id =: monthlyHourId];
        EmpName     = mon.EE__c;
        MonthHours  = mon.Curr_Reg_Hrs_NYC__c;
        Month_and_Year = mon.Month_and_Year__c;
        benefitmonth = mon.Benefit_Period__c;
        Employee__c emp = [SELECT Id,Fringe_Contract_Type__c,Monthly_Fringe_Contribution__c,MEC_Basic_Hourly_Rate__c,Fringe_Contract_Type__r.Capping_Hours__c,Govt_Contractor__r.Id,Job_Classification__c,After_Retro_Cost_Of_Benefit_Adjustments__c,Fringe_Contract_Type__r.Price_Book__c,Fringe_Contract_Type__r.Wage_Parity_Price_Book__c,Fringe_Contract_Type__r.Job_Classification_Price_Book__c,Fringe_Contract__c,Benefit_Period_Month_and_year__c,Fringe_Contract_Type__r.Fr__c,Coverage_Tier__c,Avg_Cost_Hrs__c,Monthly_fringe_obligtion_result__c,Health_Insurance_Plan__c,Dental_Insurance_Plan__c,Life_Insurance_Plan__c,Vision_Insurance_Plan__c,Plan_1__c,Plan_2__c,Plan_3__c,Name,Actual_Monthly_Hours__c,Fringe_Rate_lookup__c, Curr_Reg_Hrs_NYC__c,Monthly_Cost_Of_Benefits__c, Fringe_Contract_Type__r.Hour_Bank__c,Benefit_Amount_Allocated__c,Fringe_Contract_Type__r.Fringe_Allocation_Method__c,Fringe_Contract_Type__r.Avg_Accounting_Rules__c,Roll_Back_Hours__c,Fringe_Contract_Type__r.Id FROM Employee__c WHERE Name =: EmpName];
        ContractId = emp.Fringe_Contract_Type__r.Id;
        EmployeeId = emp.Id;
        Individual_Fringe_Benfit__c ifv;
        HourlyCOB1 = emp.MEC_Basic_Hourly_Rate__c != NULL ? emp.MEC_Basic_Hourly_Rate__c : 0.00;
        try{ifv = [SELECT Id, Name, Fringe_Contract__c, Monthly_Fringe_Obligation_Result__c, Retro_Active__c, Employee__c, Month_and_Year__c FROM Individual_Fringe_Benfit__c WHERE Employee__c =: emp.Id AND Month_and_Year__c =: mon.Month_and_Year__c];}catch(Exception e){e.getMessage();}
        if(ifv != null){
            obligationResult = ifv.Monthly_Fringe_Obligation_Result__c;
        }else{
            obligationResult = 0;
        }
        Benefit_Plan__c preset;
        try{
            preset = [Select id,name, Total_Benefits__c from Benefit_Plan__c where Fringe_Contract__c =:emp.Fringe_Contract_Type__c and Coverage_Type__c =:emp.Coverage_Tier__c limit 1];
            benefitTotal = preset!= null ? preset.Total_Benefits__c : 0.00;
			system.debug('preset>>>>:'+preset);
        }catch(Exception e){e.getMessage();}
        if(mon.Job_Classification__c != null){
            for(PricebookEntry pbe :[SELECT Id, Name, Pricebook2Id,Product2.Name, Product2Id, UnitPrice, IsActive, ProductCode FROM PricebookEntry WHERE Product2.Job_Classification__c = True AND IsActive = True AND Product2.Name =: mon.Job_Classification__c AND Product2.Family = 'Job Classification' AND Pricebook2Id =: emp.Fringe_Contract_Type__r.Job_Classification_Price_Book__c]){
                if(pbe != null){
                    fringeRate = pbe.UnitPrice;
                }       
            }
        }else if(mon.Zip_Code__c != null){
            for(PricebookEntry pbe :[SELECT Id, Name, Pricebook2Id, Product2Id, UnitPrice, IsActive, ProductCode FROM PricebookEntry WHERE Product2.Job_Classification__c = false AND IsActive = True AND Product2.Name =: String.valueOf(mon.Zip_Code__c) AND Product2.Family = 'Zip Code' AND Pricebook2Id =: emp.Fringe_Contract_Type__r.Wage_Parity_Price_Book__c]){
                if(pbe != null){
                    fringeRate = pbe.UnitPrice;
                }
            }
        }
        Fringe_Contract__c fc;
        try{
            fc =[SELECT Id, Fringe_Allocation_Rules__r.Id, Set_Maximum_Funding_Level__c,Individual_Premium_Reserve_Account__c,Fringe_Allocation_Rules__r.Name, Fringe_Allocation_Rules__r.FringeBenefits__c,Avg_Accounting_Rules__c, Fringe_Allocation_Rules__r.OverspentCategory__c, Fringe_Allocation_Rules__r.UnderspentCategory__c,Average_Cost_Accounting_Methods__c,Fr__c FROM Fringe_Contract__c WHERE Id =: ContractId];
            
        }catch(Exception e){
            e.getMessage();
        }
        List<String> cobList = new List<String>{emp.Health_Insurance_Plan__c,emp.Dental_Insurance_Plan__c,emp.Life_Insurance_Plan__c,emp.Vision_Insurance_Plan__c};
            System.debug('before price book query');
        for(PricebookEntry pbe :[SELECT Id, Name, Pricebook2Id, Product2Id, Product2.TPA_Fee_on_Premium_Rat__c, Product2.TPA_Fee_PEPM__c, Product2.TPA_FEE_Per_Hour_Per_Employee__c, UnitPrice, IsActive, ProductCode FROM PricebookEntry WHERE Pricebook2.Fringe_Contract__c =: mon.Fringe_Contract__c AND ProductCode IN: cobList  AND Name =: emp.Coverage_Tier__c]){
            System.debug('after pricebook query');
            if(pbe != null){
                if(HourlyCOB == 0){
                    if(pbe.Product2.TPA_Fee_on_Premium_Rat__c != null){
                        HourlyCOB = (pbe.UnitPrice * pbe.Product2.TPA_Fee_on_Premium_Rat__c)/100;
                    }else if(pbe.Product2.TPA_Fee_PEPM__c != null){
                        HourlyCOB = pbe.UnitPrice + pbe.Product2.TPA_Fee_PEPM__c;
                    }else if(pbe.Product2.TPA_FEE_Per_Hour_Per_Employee__c != null){
                        HourlyCOB = pbe.UnitPrice + pbe.Product2.TPA_FEE_Per_Hour_Per_Employee__c;
                    }else{
                        HourlyCOB = pbe.UnitPrice;
                    }
                }else{
                    if(pbe.Product2.TPA_Fee_on_Premium_Rat__c != null){
                        HourlyCOB += (pbe.UnitPrice * pbe.Product2.TPA_Fee_on_Premium_Rat__c)/100;
                    }else if(pbe.Product2.TPA_Fee_PEPM__c != null){
                        HourlyCOB += pbe.UnitPrice + pbe.Product2.TPA_Fee_PEPM__c;
                    }else if(pbe.Product2.TPA_FEE_Per_Hour_Per_Employee__c != null){
                        HourlyCOB += pbe.UnitPrice + pbe.Product2.TPA_FEE_Per_Hour_Per_Employee__c;
                    } else{
                        HourlyCOB = pbe.UnitPrice;
                    }                  
                }
            }
        }
        System.debug('After pricebook query');
        System.debug('Hcob is :'+HourlyCOB);
        listOfFields = Dynamic_Fields__c.getValues(emp.Fringe_Contract_Type__r.Id) == null ? '' : Dynamic_Fields__c.getValues(emp.Fringe_Contract_Type__r.Id).List_of_Fields__c;
        emp.Curr_Reg_Hrs_NYC__c = MonthHours;
        emp.Davice_and_Wage_Fringe_Rate__c = fringeRate;
        emp.Month_and_Year__c = Month_and_Year;
        emp.Zip_Code__c = mon.Zip_Code__c != null ? mon.Zip_Code__c : 0;
        emp.Hourly_Rate2__c = HourlyCOB;        
        System.debug('Hourly Rate is :'+emp.Hourly_Rate2__c);
        //emp.Curr_Reg_Hrs_NYC__c = MonthHours;
        decimal capping = emp.Fringe_Contract_Type__r.Capping_Hours__c != null ? emp.Fringe_Contract_Type__r.Capping_Hours__c : 0.00;
        emp.Curr_Reg_Hrs_NYC__c =  MonthHours > capping ? capping == 0.00 ?MonthHours:capping : MonthHours;
        downlevel = MonthHours > capping ? capping : MonthHours;
        emp.Monthly_Cost_Of_Benefit__c = HourlyCOB * (MonthHours > capping ? capping == 0.00 ?MonthHours:capping : MonthHours);
        emp.Actual_Monthly_Hours__c = MonthHours;
        if(Test.isRunningTest()){listOfFields = 'HRA__c,PaidLeave__c';}
        System.debug('mob :'+emp.Monthly_Cost_Of_Benefit__c);
        //emp.Month_and_Year__c = MonthYear;
        try{
            System.debug('emp size is Updated:'+emp);
            Update emp;
        }catch(Exception e){e.getMessage();}
        Map<Integer,String> monthNameMap = new Map<Integer, String>{1 =>'January', 2=>'February', 3=>'March', 4=>'April', 5=>'May',6=>'June', 7=>'July', 8=>'August', 9=>'September',10=>'October',11=>'November', 12=>'December'};
            System.debug('----'+monthNameMap.get(System.now().month()));
        System.debug('-----'+System.now().year());
        String dateForMonth = Month_and_Year != null ? Month_and_Year : monthNameMap.get(System.now().month())+' '+System.now().year();
        Integer fieldsCount = listOfFields.split('~').size();
        String queryContractString = 'SELECT '+listOfFields+' FROM Fringe_Contract__c WHERE Id =:  contractId';
        System.debug('-----'+queryContractString);
        sObject cusContractSetRec;
        Employee__c emp1;
        try {
            emp1 = [SELECT Id, Name,Status__c,IPRA__c,Admin_fee__c,Hourly_Cost_Of_benefit__c,Fringe_Result__c,Monthly_Fringe_Contribution__c,Fringe_Contract_Type__c,After_Retro_Cost_Of_Benefit_Adjustments__c,Monthly_fringe_obligtion_result__c,Actual_Monthly_Hours__c,HRA_Card_Monthly_Deposit__c,Fringe_Rate_lookup__c,Fringe_Contract_Type__r.Preset_Type__c,Cash_Equivalent_Payment__c,Average_Cost_Accounting__c,Curr_Reg_Hrs_NYC__c,Monthly_Cost_Of_Benefits__c, Fringe_Contract_Type__r.Hour_Bank__c, Fringe_Contract_Type__r.Fringe_Allocation_Method__c, Roll_Back_Hours__c,Individual_Premium_Reserve_Account_Month__c,Rolling_Balance_of_IPRA__c,Month_and_Year__c,Fringe_Contract_Type__r.Credit_Rule__c,Fringe_Contract_Type__r.Debit_Rule__c,Amount_Allocated_to_Paid_Leave_Account__c, Monthly_Fringe_Amount_Allocated__c,Benefit_Amount_Allocated__c,Benefit_Plan__r.Total_Benefits__c,Benefit_Plan__c,Benefit_Plan__r.Coverage_Type__c,Benefit_Plan__r.Contribution_to_HRA_Benefit__c,Benefit_Plan__r.Contribution_to_RetBenefit__c,Benefit_Plan__r.Contribution_to_Paid_Leave_Benefit__c,Fringe_Result_for_Paid_Leave__c,Fringe_Contract_Type__r.SELECT_COST_OF_BENEFITS__c FROM Employee__c Where Id =: EmployeeId];
            System.debug('Fringe result is :'+emp1.Fringe_Result__c);
            cusContractSetRec= Database.query(queryContractString);
        }catch(Exception e){e.getMessage();}
        String queryString = 'SELECT '+listOfFields+',Month_and_Year__c,Curr_Reg_Hours_NYC__c,Employee__r.Id, Employee__r.Name  FROM Individual_Fringe_Benfit__c WHERE Month_and_Year__c =: Month_and_Year  AND Employee__r.Id =:  employeeId';
        System.debug('-----'+queryString);
        sObject cusSetRec; 
        try{
            cusSetRec= Database.query(queryString);
        }catch(Exception e){e.getMessage();}
        // Class Level Coverage
        //RuleBasedOnClassLevelBenfits.benefits(mon.id);
        //System.debug('Monthly Hours id :'+mon.id);
        Decimal excessAmount  = 0;
        decimal presetbenefitPlans = 0.00;
        Decimal totalBenefit = emp1.Benefit_Plan__r.Total_Benefits__c== null ? 0.00:emp1.Benefit_Plan__r.Total_Benefits__c;
        
        excessAmount  =  emp1.Monthly_fringe_obligtion_result__c - benefitTotal;
        Fringe_Contract__c Fringrules = [SELECT Id, Fringe_Allocation_Rules__r.Id,Fringe_Allocation_Rules__r.Name,
                                         Fringe_Allocation_Rules__r.Class_I_From_Hours__c,
                                         Fringe_Allocation_Rules__r.Class_II_From_Hours__c,
                                         Fringe_Allocation_Rules__r.Class_III_From_Hours__c,
                                         Fringe_Allocation_Rules__r.Class_IV_From_Hours__c,
                                         Fringe_Allocation_Rules__r.Class_V_From_Hours__c,
                                         Fringe_Allocation_Rules__r.Class_I_To_Hours__c,
                                         Fringe_Allocation_Rules__r.Class_II_To_Hours__c,
                                         Fringe_Allocation_Rules__r.Class_III_To_Hours__c,
                                         Fringe_Allocation_Rules__r.Class_IV_To_Hours__c,
                                         Fringe_Allocation_Rules__r.Class_V_To_Hours__c
                                         FROM Fringe_Contract__c WHERE Id =: ContractId];
        System.debug('1 from Hours is :'+Fringrules.Fringe_Allocation_Rules__r.Class_I_From_Hours__c);
        System.debug('2 from Hours is :'+Fringrules.Fringe_Allocation_Rules__r.Class_II_From_Hours__c);
        System.debug('3 from Hours is :'+Fringrules.Fringe_Allocation_Rules__r.Class_III_From_Hours__c);
        System.debug('4 from Hours is :'+Fringrules.Fringe_Allocation_Rules__r.Class_IV_From_Hours__c);
        System.debug('5 from Hours is :'+Fringrules.Fringe_Allocation_Rules__r.Class_V_From_Hours__c);
        System.debug('1 To Hours is :'+Fringrules.Fringe_Allocation_Rules__r.Class_I_To_Hours__c);
        System.debug('2 To Hours is :'+Fringrules.Fringe_Allocation_Rules__r.Class_II_To_Hours__c);
        System.debug('3 To Hours is :'+Fringrules.Fringe_Allocation_Rules__r.Class_III_To_Hours__c);
        System.debug('3 To Hours is :'+Fringrules.Fringe_Allocation_Rules__r.Class_IV_To_Hours__c);
        System.debug('3 To Hours is :'+Fringrules.Fringe_Allocation_Rules__r.Class_V_To_Hours__c);
        if(cusSetRec != null){
            cusSetRec = new Individual_Fringe_Benfit__c();
            cusSetRec.put('Month_and_Year__c',Month_and_Year);
            cusSetRec.put('Retro_Active__c',True);//Retro_Active__c
            cusSetRec.put('After_Retro_Hours_Adjustments__c',emp1.Month_and_Year__c == Month_and_Year ? emp1.Monthly_fringe_obligtion_result__c != null ? emp1.Monthly_fringe_obligtion_result__c:0.00 - obligationResult != null ? obligationResult:0.00:0);
            emp1.After_Retro_Hours_Adjustments__c = emp1.Month_and_Year__c == Month_and_Year ? emp1.Monthly_fringe_obligtion_result__c != null ? emp1.Monthly_fringe_obligtion_result__c:0.00 - obligationResult != null ? obligationResult:0.00:0;
            System.debug('for existing of individual fringe benefits');
            // Fringe allocation rules on Fringe contracts
            if( MonthHours > Fringrules.Fringe_Allocation_Rules__r.Class_I_From_Hours__c   && MonthHours <= Fringrules.Fringe_Allocation_Rules__r.Class_I_To_Hours__c) {
                cusSetRec.put('Curr_Reg_Hours_NYC__c',MonthHours);
                cusSetRec.put('Fringe_Contract__c',emp1.Fringe_Contract_Type__c);
                cusSetRec.put('Class_Level_Of_Benefits2__c','Class I Benefits');
                cusSetRec.put('Admin_fee__c',emp1.Admin_fee__c);
                cusSetRec.put('Benefit_Period_Month_and_Year__c',benefitmonth);
                cusSetRec.put('Hourly_Cost_Of_Benefit__c',HourlyCOB1);
                cusSetRec.put('Fringe_Result_Category__c',emp1.Status__c);
                cusSetRec.put('Monthly_Cost_Of_Benefit__c',emp1.Hourly_Cost_Of_benefit__c); // lable name differnt
                cusSetRec.put('Fringe_Result__c',emp1.Fringe_Result__c);
                System.debug('Updated : Class I Benefits');
            }else if(MonthHours >= Fringrules.Fringe_Allocation_Rules__r.Class_II_From_Hours__c && MonthHours <= Fringrules.Fringe_Allocation_Rules__r.Class_II_To_Hours__c) {
                cusSetRec.put('Curr_Reg_Hours_NYC__c',MonthHours);
                cusSetRec.put('Fringe_Contract__c',emp1.Fringe_Contract_Type__c);
                cusSetRec.put('Class_Level_Of_Benefits2__c','Class II Benefits');
                cusSetRec.put('Admin_fee__c',emp1.Admin_fee__c);
                cusSetRec.put('Benefit_Period_Month_and_Year__c',benefitmonth);
                cusSetRec.put('Fringe_Result_Category__c',emp1.Status__c);
                cusSetRec.put('Hourly_Cost_Of_Benefit__c',HourlyCOB1);
                cusSetRec.put('Monthly_Cost_Of_Benefit__c',emp1.Hourly_Cost_Of_benefit__c); // lable name differnt
                cusSetRec.put('Fringe_Result__c',emp1.Fringe_Result__c);
                System.debug('Updated : Class II Benefits');
            }else if(MonthHours > Fringrules.Fringe_Allocation_Rules__r.Class_III_From_Hours__c && MonthHours <= Fringrules.Fringe_Allocation_Rules__r.Class_III_To_Hours__c) {
                cusSetRec.put('Curr_Reg_Hours_NYC__c',MonthHours);
                cusSetRec.put('Class_Level_Of_Benefits2__c','Class III Benefits');
                cusSetRec.put('Fringe_Contract__c',emp1.Fringe_Contract_Type__c);
                cusSetRec.put('Admin_fee__c',emp1.Admin_fee__c);
                cusSetRec.put('Fringe_Result_Category__c',emp1.Status__c);
                cusSetRec.put('Hourly_Cost_Of_Benefit__c',HourlyCOB1);
                cusSetRec.put('Benefit_Period_Month_and_Year__c',benefitmonth);
                cusSetRec.put('Monthly_Cost_Of_Benefit__c',emp1.Hourly_Cost_Of_benefit__c); // lable name differnt
                cusSetRec.put('Fringe_Result__c',emp1.Fringe_Result__c);
                System.debug('Updated : Class III Benefits');
            }else if(MonthHours > Fringrules.Fringe_Allocation_Rules__r.Class_IV_From_Hours__c && MonthHours <= Fringrules.Fringe_Allocation_Rules__r.Class_IV_To_Hours__c) {
                cusSetRec.put('Curr_Reg_Hours_NYC__c',MonthHours);
                cusSetRec.put('Fringe_Contract__c',emp1.Fringe_Contract_Type__c);
                cusSetRec.put('Class_Level_Of_Benefits2__c','Class IV Benefits');
                cusSetRec.put('Admin_fee__c',emp1.Admin_fee__c);
                cusSetRec.put('Benefit_Period_Month_and_Year__c',benefitmonth);
                cusSetRec.put('Fringe_Result_Category__c',emp1.Status__c);
                cusSetRec.put('Hourly_Cost_Of_Benefit__c',HourlyCOB1);
                cusSetRec.put('Monthly_Cost_Of_Benefit__c',emp1.Hourly_Cost_Of_benefit__c); // lable name differnt
                cusSetRec.put('Fringe_Result__c',emp1.Fringe_Result__c);
                System.debug('Updated : Class IV Benefits');
            }else if(MonthHours > Fringrules.Fringe_Allocation_Rules__r.Class_V_From_Hours__c && MonthHours <= Fringrules.Fringe_Allocation_Rules__r.Class_V_To_Hours__c) {
                cusSetRec.put('Curr_Reg_Hours_NYC__c',MonthHours);
                cusSetRec.put('Fringe_Contract__c',emp1.Fringe_Contract_Type__c);
                cusSetRec.put('Class_Level_Of_Benefits2__c','Class V Benefits');
                cusSetRec.put('Benefit_Period_Month_and_Year__c',benefitmonth);
                cusSetRec.put('Admin_fee__c',emp1.Admin_fee__c);
                cusSetRec.put('Fringe_Result_Category__c',emp1.Status__c);
                cusSetRec.put('Hourly_Cost_Of_Benefit__c',HourlyCOB1);
                cusSetRec.put('Monthly_Cost_Of_Benefit__c',emp1.Hourly_Cost_Of_benefit__c); // lable name differnt
                cusSetRec.put('Fringe_Result__c',emp1.Fringe_Result__c);
                System.debug('Updated : Class V Benefits');
            }else if(fc.Fringe_Allocation_Rules__r.UnderspentCategory__c == 'Hourly cost of Benefits without Hour Bank Accounts'){
                cusSetRec.put('Month_and_Year__c',Month_and_Year);
                cusSetRec.put('Employee__c',emp.Id);
                cusSetRec.put('Fringe_Contract__c',emp1.Fringe_Contract_Type__c);
                cusSetRec.put('Fringe_Rate__c',emp1.Fringe_Rate_lookup__c);
                cusSetRec.put('Benefit_Period_Month_and_Year__c',benefitmonth);
                cusSetRec.put('Curr_Reg_Hours_NYC__c',emp1.Curr_Reg_Hrs_NYC__c);
                cusSetRec.put('Fringe_Result_Category__c',emp1.Status__c);
                cusSetRec.put('Hourly_Cost_Of_Benefit__c',HourlyCOB1);
                cusSetRec.put('Monthly_Fringe_Obligation_Result__c',emp1.Monthly_fringe_obligtion_result__c);
                cusSetRec.put('Monthly_Cost_Of_Benefit__c',emp1.Monthly_Cost_Of_Benefits__c);
                cusSetRec.put('Monthly_Fringe_Contribution__c',emp1.Monthly_Fringe_Contribution__c);
            }
            
            for(String indiviField : listOfFields.split(',')){
                System.debug('After for loop');
                System.debug('individual fileds'+indiviField);
                if(!String.isBlank(indiviField)){
                    system.debug('String is not blank');
                    String objectName = 'Individual_Fringe_Benfit__c';
                    String fieldName = indiviField;
                    SObjectType r = ((SObject)(Type.forName('Schema.'+objectName).newInstance())).getSObjectType();                            
                    DescribeSObjectResult d = r.getDescribe();
                    String fieldType = String.valueOf(d.fields.getMap().get(fieldName).getDescribe().getType());
                    System.debug('Field type is :'+fieldType);
                    If(fieldType.equalsIgnoreCase('STRING')){
                        cusSetRec.put(indiviField,String.valueOf(0.00));
                    }else{
                        cusSetRec.put(indiviField,0.00);
                        System.debug('its allocated to cusSetRec'+cusSetRec);
                    }
                    String percen = (String)cusContractSetRec.get(indiviField);
                    System.debug('percentages is :'+percen);
                    decimal benefitPlans = 0.00;
                    if(emp1.Fringe_Contract_Type__r.Preset_Type__c =='Preset Amount Monthly'){
                        if(fieldName.split('__c')[0].contains('HRA')){
                            benefitPlans = emp1.Benefit_Plan__r.Contribution_to_HRA_Benefit__c== null ? 0.00:emp1.Benefit_Plan__r.Contribution_to_HRA_Benefit__c;
                        }else if(fieldName.split('__c')[0].contains('Retirement')){
                            benefitPlans = emp1.Benefit_Plan__r.Contribution_to_RetBenefit__c== null ? 0.00:emp1.Benefit_Plan__r.Contribution_to_RetBenefit__c;
                        }else if(fieldName.split('__c')[0].contains('Paid')){
                            benefitPlans = emp1.Benefit_Plan__r.Contribution_to_Paid_Leave_Benefit__c== null ? 0.00:emp1.Benefit_Plan__r.Contribution_to_Paid_Leave_Benefit__c;
                        }else{
                            
                        }
                    }else if(emp1.Fringe_Contract_Type__r.Preset_Type__c == 'Preset Amount Per Hour'){
                        if(fieldName.split('__c')[0].contains('HRA')){
                            benefitPlans = mon.Curr_Reg_Hrs_NYC__c * emp1.Benefit_Plan__r.Contribution_to_HRA_Benefit__c== null ? 0.00:emp1.Benefit_Plan__r.Contribution_to_HRA_Benefit__c;
                        }else if(fieldName.split('__c')[0].contains('Retirement')){
                            benefitPlans = mon.Curr_Reg_Hrs_NYC__c * emp1.Benefit_Plan__r.Contribution_to_RetBenefit__c== null ? 0.00:emp1.Benefit_Plan__r.Contribution_to_RetBenefit__c;
                        }else if(fieldName.split('__c')[0].contains('Paid')){
                            benefitPlans = mon.Curr_Reg_Hrs_NYC__c * emp1.Benefit_Plan__r.Contribution_to_Paid_Leave_Benefit__c== null ? 0.00:emp1.Benefit_Plan__r.Contribution_to_Paid_Leave_Benefit__c;
                        }else{
                            
                        }
                    }
                    decimal cal = (excessAmount)*decimal.valueOf(percen)/100;
                    cal = cal + benefitPlans;
                    if(fieldName.equalsIgnoreCase('IndividualPremiumReserveAccountMon__c')){
                        emp1.IPRA__c = cal.setScale(2);
                        cusSetRec.put('IndividualPremiumReserveAccountMon__c', cal.setScale(2));
                        cusSetRec.put('Rolling_Individual_Premium_Reserve__c', emp1.Rolling_Balance_of_IPRA__c != null ?emp1.Rolling_Balance_of_IPRA__c + cal.setScale(2) : cal.setScale(2));
                        emp1.Rolling_Balance_of_IPRA__c = emp1.Rolling_Balance_of_IPRA__c != null ?emp1.Rolling_Balance_of_IPRA__c + cal.setScale(2) : cal.setScale(2);
                        
                        
                    }
                    
                    System.debug('after calucaltion of "fringe result" is :'+cal);
                    if(String.valueOf(cal.setScale(2)) != null){
                        If(fieldType.equalsIgnoreCase('STRING')){
                            cusSetRec.put(indiviField, String.valueOf(cal.setScale(2)));
                        }else{
                            cusSetRec.put(indiviField, cal.setScale(2));
                            System.debug('after allocating percentages :'+cusSetRec);
                        }
                    }
                }
                
            } 
            try{
                System.debug('Existing individual benefit is updated');
                insert cusSetRec;
            }catch(Exception e){
                System.debug('--'+e.getMessage());
            }
        }else {
            System.debug('for new creation of individual fringe benefits');
            sObject cusSetcreateRec = new Individual_Fringe_Benfit__c();
            
            // Fringe allocation rules on Fringe contracts
            if( MonthHours > Fringrules.Fringe_Allocation_Rules__r.Class_I_From_Hours__c   && MonthHours <= Fringrules.Fringe_Allocation_Rules__r.Class_I_To_Hours__c) {
                cusSetcreateRec.put('Curr_Reg_Hours_NYC__c',MonthHours);
                cusSetcreateRec.put('Fringe_Contract__c',emp1.Fringe_Contract_Type__c);
                cusSetcreateRec.put('Class_Level_Of_Benefits2__c','Class I Benefits');
                cusSetcreateRec.put('Admin_fee__c',emp1.Admin_fee__c);
                cusSetcreateRec.put('Benefit_Period_Month_and_Year__c',benefitmonth);
                cusSetcreateRec.put('Fringe_Result_Category__c',emp1.Status__c);
                cusSetcreateRec.put('Hourly_Cost_Of_Benefit__c',HourlyCOB1);
                cusSetcreateRec.put('Monthly_Cost_Of_Benefit__c',emp1.Hourly_Cost_Of_benefit__c);
                cusSetcreateRec.put('Fringe_Result__c',emp1.Fringe_Result__c);
                System.debug('Inserted : Class I Benefits ');
            }else if(MonthHours >= Fringrules.Fringe_Allocation_Rules__r.Class_II_From_Hours__c && MonthHours <= Fringrules.Fringe_Allocation_Rules__r.Class_II_To_Hours__c) {
                cusSetcreateRec.put('Curr_Reg_Hours_NYC__c',MonthHours);
                cusSetcreateRec.put('Fringe_Contract__c',emp1.Fringe_Contract_Type__c);
                cusSetcreateRec.put('Class_Level_Of_Benefits2__c','Class II Benefits');
                cusSetcreateRec.put('Fringe_Result_Category__c',emp1.Status__c);
                cusSetcreateRec.put('Benefit_Period_Month_and_Year__c',benefitmonth);
                cusSetcreateRec.put('Hourly_Cost_Of_Benefit__c',HourlyCOB1);
                cusSetcreateRec.put('Admin_fee__c',emp1.Admin_fee__c);
                cusSetcreateRec.put('Monthly_Cost_Of_Benefit__c',emp1.Hourly_Cost_Of_benefit__c);
                cusSetcreateRec.put('Fringe_Result__c',emp1.Fringe_Result__c);
                System.debug('Inserted : Class II Benefits ');
            }else if(MonthHours > Fringrules.Fringe_Allocation_Rules__r.Class_III_From_Hours__c && MonthHours <= Fringrules.Fringe_Allocation_Rules__r.Class_III_To_Hours__c) {
                cusSetcreateRec.put('Curr_Reg_Hours_NYC__c',MonthHours);
                cusSetcreateRec.put('Fringe_Contract__c',emp1.Fringe_Contract_Type__c);
                cusSetcreateRec.put('Class_Level_Of_Benefits2__c','Class III Benefits');
                cusSetcreateRec.put('Benefit_Period_Month_and_Year__c',benefitmonth);
                cusSetcreateRec.put('Fringe_Result_Category__c',emp1.Status__c);
                cusSetcreateRec.put('Hourly_Cost_Of_Benefit__c',HourlyCOB1);
                cusSetcreateRec.put('Admin_fee__c',emp1.Admin_fee__c);
                cusSetcreateRec.put('Monthly_Cost_Of_Benefit__c',emp1.Hourly_Cost_Of_benefit__c);
                cusSetcreateRec.put('Fringe_Result__c',emp1.Fringe_Result__c);
                System.debug('Inserted : Class III Benefits ');
            }else if(MonthHours > Fringrules.Fringe_Allocation_Rules__r.Class_IV_From_Hours__c && MonthHours <= Fringrules.Fringe_Allocation_Rules__r.Class_IV_To_Hours__c) {
                cusSetcreateRec.put('Curr_Reg_Hours_NYC__c',MonthHours);
                cusSetcreateRec.put('Fringe_Contract__c',emp1.Fringe_Contract_Type__c);
                cusSetcreateRec.put('Class_Level_Of_Benefits2__c','Class IV Benefits');
                cusSetcreateRec.put('Benefit_Period_Month_and_Year__c',benefitmonth);
                cusSetcreateRec.put('Fringe_Result_Category__c',emp1.Status__c);
                cusSetcreateRec.put('Hourly_Cost_Of_Benefit__c',HourlyCOB1);
                cusSetcreateRec.put('Admin_fee__c',emp1.Admin_fee__c);
                cusSetcreateRec.put('Monthly_Cost_Of_Benefit__c',emp1.Hourly_Cost_Of_benefit__c);
                cusSetcreateRec.put('Fringe_Result__c',emp1.Fringe_Result__c);
                System.debug('Inserted : Class IV Benefits ');
            }else if(MonthHours > Fringrules.Fringe_Allocation_Rules__r.Class_V_From_Hours__c && MonthHours <= Fringrules.Fringe_Allocation_Rules__r.Class_V_To_Hours__c) {
                cusSetcreateRec.put('Curr_Reg_Hours_NYC__c',MonthHours);
                cusSetcreateRec.put('Fringe_Contract__c',emp1.Fringe_Contract_Type__c);
                cusSetcreateRec.put('Class_Level_Of_Benefits2__c','Class V Benefits');
                cusSetcreateRec.put('Benefit_Period_Month_and_Year__c',benefitmonth);
                cusSetcreateRec.put('Hourly_Cost_Of_Benefit__c',HourlyCOB1);
                cusSetcreateRec.put('Fringe_Result_Category__c',emp1.Status__c);
                cusSetcreateRec.put('Admin_fee__c',emp1.Admin_fee__c);
                cusSetcreateRec.put('Monthly_Cost_Of_Benefit__c',emp1.Hourly_Cost_Of_benefit__c);
                cusSetcreateRec.put('Fringe_Result__c',emp1.Fringe_Result__c);
                System.debug('Inserted : Class V Benefits ');
            }else {
                cusSetcreateRec.put('Month_and_Year__c',Month_and_Year);
                cusSetcreateRec.put('Employee__c',emp.Id);
                cusSetcreateRec.put('Fringe_Contract__c',emp1.Fringe_Contract_Type__c);
                cusSetcreateRec.put('Fringe_Rate__c',emp1.Fringe_Rate_lookup__c);
                cusSetcreateRec.put('Benefit_Period_Month_and_Year__c',benefitmonth );
                cusSetcreateRec.put('Hourly_Cost_Of_Benefit__c',HourlyCOB1);
                cusSetcreateRec.put('Fringe_Result_Category__c',emp1.Status__c);
                cusSetcreateRec.put('Curr_Reg_Hours_NYC__c',emp1.Curr_Reg_Hrs_NYC__c);
                cusSetcreateRec.put('Monthly_Fringe_Obligation_Result__c',emp1.Monthly_fringe_obligtion_result__c);
                cusSetcreateRec.put('Monthly_Cost_Of_Benefit__c',emp1.Monthly_Cost_Of_Benefits__c);
                cusSetcreateRec.put('Monthly_Fringe_Contribution__c',emp1.Monthly_Fringe_Contribution__c);
            }
            for(String indiviField : listOfFields.split(',')){
                if(!String.isBlank(indiviField)){
                    // Employee Id
                    cusSetcreateRec.put('Employee__c',emp1.id);
                    //Month_and_Year__c
                    cusSetcreateRec.put('Month_and_Year__c',dateForMonth);
                    String objectName = 'Individual_Fringe_Benfit__c';
                    String fieldName = indiviField;
                    SObjectType r = ((SObject)(Type.forName('Schema.'+objectName).newInstance())).getSObjectType();
                    DescribeSObjectResult d = r.getDescribe();
                    String fieldType = String.valueOf(d.fields.getMap().get(fieldName).getDescribe().getType());
                    If(fieldType.equalsIgnoreCase('STRING')){
                        cusSetcreateRec.put(indiviField,String.valueOf(0.00));
                    }else{
                        cusSetcreateRec.put(indiviField,0.00);
                    }
                    String percen = (String)cusContractSetRec.get(indiviField);
                    decimal benefitPlans = 0.00;
                    if(emp1.Fringe_Contract_Type__r.Preset_Type__c =='Preset Amount Monthly'){
                        if(fieldName.split('__c')[0].contains('HRA')){
                            benefitPlans = emp1.Benefit_Plan__r.Contribution_to_HRA_Benefit__c== null ? 0.00:emp1.Benefit_Plan__r.Contribution_to_HRA_Benefit__c;
                        }else if(fieldName.split('__c')[0].contains('Retirement')){
                            benefitPlans = emp1.Benefit_Plan__r.Contribution_to_RetBenefit__c== null ? 0.00:emp1.Benefit_Plan__r.Contribution_to_RetBenefit__c;
                        }else if(fieldName.split('__c')[0].contains('Paid')){
                            benefitPlans = emp1.Benefit_Plan__r.Contribution_to_Paid_Leave_Benefit__c== null ? 0.00:emp1.Benefit_Plan__r.Contribution_to_Paid_Leave_Benefit__c;
                        }else{
                            
                        }
                    }else if(emp1.Fringe_Contract_Type__r.Preset_Type__c == 'Preset Amount Per Hour'){
                        if(fieldName.split('__c')[0].contains('HRA')){
                             Decimal hraBenefits = emp1.Benefit_Plan__r.Contribution_to_HRA_Benefit__c== null ? 0.00:emp1.Benefit_Plan__r.Contribution_to_HRA_Benefit__c;
                            benefitPlans = mon.Curr_Reg_Hrs_NYC__c * hraBenefits;
                        }else if(fieldName.split('__c')[0].contains('Retirement')){
                            Decimal retirementBenefits = emp1.Benefit_Plan__r.Contribution_to_RetBenefit__c== null ? 0.00:emp1.Benefit_Plan__r.Contribution_to_RetBenefit__c;
                            benefitPlans = mon.Curr_Reg_Hrs_NYC__c * retirementBenefits;
                        }else if(fieldName.split('__c')[0].contains('Paid')){
                            Decimal paidBenefits = emp1.Benefit_Plan__r.Contribution_to_Paid_Leave_Benefit__c== null ? 0.00:emp1.Benefit_Plan__r.Contribution_to_Paid_Leave_Benefit__c;
                            benefitPlans = mon.Curr_Reg_Hrs_NYC__c * paidBenefits;
                        }else{
                            
                        }
                    }
                    decimal cal = (excessAmount)*decimal.valueOf(percen)/100;
                    cal = cal + benefitPlans;
                    if(fieldName.equalsIgnoreCase('IndividualPremiumReserveAccountMon__c')){
                        emp1.IPRA__c = cal.setScale(2);
                        cusSetcreateRec.put('IndividualPremiumReserveAccountMon__c', cal.setScale(2));
                        cusSetcreateRec.put('Rolling_Individual_Premium_Reserve__c', emp1.Rolling_Balance_of_IPRA__c != null ?emp1.Rolling_Balance_of_IPRA__c + cal.setScale(2) : cal.setScale(2));
                        emp1.Rolling_Balance_of_IPRA__c = emp1.Rolling_Balance_of_IPRA__c != null ?emp1.Rolling_Balance_of_IPRA__c + cal.setScale(2) : cal.setScale(2);
                        
                        
                    }
                    if(String.valueOf(cal.setScale(2)) != null){
                        If(fieldType.equalsIgnoreCase('STRING')){
                            cusSetcreateRec.put(indiviField, String.valueOf(cal.setScale(2)));
                        }else{
                            cusSetcreateRec.put(indiviField, cal.setScale(2));
                        }
                    }
                    
                }
            }
            try{
                insert cusSetcreateRec;
                System.debug('New individual benefit is created :'+cusSetcreateRec);
                System.debug('New individual benefit is created :'+cusSetcreateRec.id);
            }catch(Exception e){
                System.debug('--'+e.getMessage());
            }
        }
        try{
            update emp1;
        }catch(Exception e){
            System.debug('--'+e.getMessage());
        }
        
    }
}