@isTest
private class ClassLevelBenefitsTest {
    @isTest
    static void myTestMethod() {
         //Test Account Creation
        Account acc = new Account();
        acc.name = 'testAcc1';
        
        insert acc;
        
        //Fringe Allocation Rules
        Fringe_Allocation_Rules__c far = new Fringe_Allocation_Rules__c();
        far.Name = 'classlevelBenefits';
        far.UnderspentCategory__c = 'Class Level Coverage';
        far.Class_I_From_Hours__c = 1;
        far.Class_I_To_Hours__c = 90;
        far.Class_II_From_Hours__c= 91;
        far.Class_II_To_Hours__c=120;
        far.Class_III_From_Hours__c=121;
        far.Class_III_To_Hours__c =200;
        
        insert far;
        //Test Fringe contract creation
        Fringe_Contract__c fc = new Fringe_Contract__c();
        fc.Fringe_Allocation_Rules__c = far.id;
        fc.Contract_Name__c = 'testContract';
        fc.Is_Active__c = True;
          
        insert fc;
        
        //Test Employee creation
        employee__C emp = new employee__c();
        emp.Name = 'testEmp';
        
        
        emp.Fringe_Contract_Type__c = fc.id;
        
        
        
        
        insert emp;
       
        
        //Test Monthly hours creation
        Monthly_Hours__c mh = new Monthly_Hours__c();
        mh.Employee__c = emp.id;
        mh.EE__c = 'testEmp';
        mh.Curr_Reg_Hrs_NYC__c = 120;
        mh.Month_and_Year__c = 'May 2021';
        insert mh;
		
		List<id> ids = new List<id>();
        ids.add(mh.id);
        
        ClassLevelBenfits.benefits(ids);
        mh.Curr_Reg_Hrs_NYC__c = 20;
        update mh;
        ClassLevelBenfits.benefits(ids);
         mh.Curr_Reg_Hrs_NYC__c = 220;
        update mh;
        ClassLevelBenfits.benefits(ids);
        
    }

}