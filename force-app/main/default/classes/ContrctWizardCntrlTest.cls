@isTest
public class ContrctWizardCntrlTest {
    static testMethod void testContractClass(){
        String [] qtr=new String[]{'Q1','Q2','Q3','Q4'};
                
        Govt_Contractor__c gc= new Govt_Contractor__c(Name = 'FBA');
        insert gc;
        
        Fringe_Allocation_Rules__c farObj = new Fringe_Allocation_Rules__c(Name = 'Method A1',FringeBenefits__c = 'HRA;Paid Leave Fringe Reserve Account',UnderspentCategory__c = 'Average Cost Accounting');
        insert farObj;
        
        Fringe_Contract__c ObjFc= new Fringe_Contract__c(Contract_Name__c = 'testContract',Average_Cost_Accounting_Methods__c = 'Equivalent contributions',Fr__c='Average Cost Fringe Accounting Rule',Avg_Accounting_Rules__c ='Quarterly',HRA__c = '50',
        Retirement__c = '50',Govt_Contractor__c=gc.ID,Fringe_Allocation_Rules__c=farObj.Id,Fringe_Rate__c =5);
        insert ObjFc;
        
        Average_Cost_Accounting__c acaObj = new Average_Cost_Accounting__c(TOTAL_MONTHLY_COST_OF_FRINGE_BENEFITS__c=130,TOTAL_NUMBER_OF_HOURS_WORKED__c=100);
        insert acaObj;
        
        acaObj.TOTAL_MONTHLY_COST_OF_FRINGE_BENEFITS__c = 135;
        update acaObj;
        
        List<Individual_Fringe_Benefits_Percentages__c> individList = new List<Individual_Fringe_Benefits_Percentages__c>();
        
        Individual_Fringe_Benefits_Percentages__c ifbp1 = new Individual_Fringe_Benefits_Percentages__c(Fringe_Contract__c = ObjFc.Id,Individual_Fringe_Benefit_Account__c='HRA');
        individList.add(ifbp1);
        
        Individual_Fringe_Benefits_Percentages__c ifbp2 = new Individual_Fringe_Benefits_Percentages__c(Fringe_Contract__c = ObjFc.Id,Individual_Fringe_Benefit_Account__c='HRA1');
        individList.add(ifbp2);
        
        //insert individList;
        Employee__c empObj = new Employee__c(Name='TestName',Curr_Reg_Hrs_NYC__c=120,Govt_Contractor__c= gc.Id,Unique_Identifier__c='123q',Fringe_Contract_Type__c =ObjFc.Id,Month_and_Year__c ='April 2021',Average_Cost_Accounting__c=acaObj.Id,Status__c='Underspent');
        Insert empObj;
        empObj.Unique_Identifier__c = '780q';
        update empObj;
        
        Monthly_Hours__c mh = new Monthly_Hours__c();
        mh.Name = 'David';
        mh.EE__c = 'David';
        mh.EmpStatus__c = 'A';
        mh.Month_and_Year__c = 'March 2021';
        mh.Curr_Reg_Hrs_NYC__c = 100;
        mh.Govt_Contractors__c = gc.Id;
        mh.Fringe_Contract__c = ObjFc.Id;
        mh.Employee__c = empObj.Id;
        mh.Coverage_Tier__c = 'Employee Only';
        insert mh;
        
        Individual_Fringe_Benfit__c objBenefits=new Individual_Fringe_Benfit__c (HRA__C=34,Employee__c=empObj.Id);
        insert objBenefits;
        
        List<Individual_Fringe_Benefits_Percentages__c> ifbpList = new List<Individual_Fringe_Benefits_Percentages__c>();
         Dynamic_Fields__c df = new Dynamic_Fields__c();
         for(Integer i = 1; i<=2;i++){
             Individual_Fringe_Benefits_Percentages__c ifbp = new Individual_Fringe_Benefits_Percentages__c();
             df.Name = String.valueOf(ObjFc.Id);
             ifbp.Percentage__c = 50;
             if(i == 1){
                 ifbp.Individual_Fringe_Benefit_Account__c = 'HRA';
                 df.List_of_Fields__c = 'HRA__c';
             }else{
                 ifbp.Individual_Fringe_Benefit_Account__c = 'Retirement';
                 df.List_of_Fields__c = 'HRA__c,Retirement__c';
             }
             ifbp.Fringe_Contract__c = ObjFc.Id;
             ifbpList.add(ifbp);
         }
         insert df;
         insert ifbpList;
        
        test.startTest();
        for (Integer i = 0; i < qtr.size(); i++) {
          contractWizardCntrl.getAvgQtrDetails(ObjFc.Id, qtr[i]);          
        }
        contractWizardCntrl.getLabelEmployee();
        contractWizardCntrl.createBenefits(objBenefits, empObj);
        contractWizardCntrl.getindividualBenefits(empObj.Id);
        contractWizardCntrl.fetchAllocationDetails(farObj.Id);
        contractWizardCntrl.createContractWizardSetup(ObjFc, individList);
        List<string> options = contractWizardCntrl.getPiklistValues();
        List<string> multiOptions = contractWizardCntrl.getFringeMultiLocation();
        Map<String, String> getContrct = contractWizardCntrl.getcontractType();
        Map<String, String> getContrctMethod = contractWizardCntrl.getFringeAllocationMethd();
        Map<String, String> getContrctMontly = contractWizardCntrl.getFringeMonthly();
        Map<String, String> getContrctAccRule = contractWizardCntrl.getFringeAccRule();
        UpdateAvgCostToFringeBenefits.updateAvgCostBenefits(empObj.Id);
        test.stopTest();        
    }
    static testMethod void testCustomLookClass() {
      list<Job_Site_Location__c>  jobLocList = [select id,name from Job_Site_Location__c]; 
      
      string seachKey = 'abc';
      Account acc = new Account(Name = 'test',Phone = '1234567897');        
      insert acc;  
      string whereCondition = 'AND Id =\''+ acc.Id+'\'';  
      
      reUsableMultiSelectLookupCtrl.fetchLookUpValues(seachKey, 'Account', jobLocList);
      customLookUpController.fetchLookUpValues(seachKey,'Account',whereCondition);      
  }
}