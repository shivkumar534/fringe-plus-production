global class UpdateFringeBenefitPercentageBatch implements Database.batchable<Id>,Database.Stateful,Database.AllowsCallouts{ 
    List<Id> recordList = new List<Id>();
    
    global UpdateFringeBenefitPercentageBatch(List<Id> recordIdList){
        recordList = recordIdList;
    }
    global List<Id> start(Database.BatchableContext BC){
        return recordList;
    }
    
    global void execute(Database.BatchableContext BC, List<Id> recordList){
        if(recordList.size() > 0){       
            for(Id recordId : recordList){
                updateFringeBenefitPecentage(recordId);
            }
        }
    }
    global void finish(Database.BatchableContext BC){}
    
    public static void updateFringeBenefitPecentage (Id monthlyHoursId){
        Set<String> ifbp = new Set<String>();
        Set<String> listcomp = new Set<String>();
        List<Individual_Fringe_Benefits_Percentages__c> ip = new List<Individual_Fringe_Benefits_Percentages__c>();
        try{
            Benefit_Plan__c fal = [SELECT Id, Fringe_Contract__c, Contribution_to_HRA_Benefit__c, Contribution_to_Paid_Leave_Benefit__c, Contribution_to_RetBenefit__c FROM Benefit_Plan__c WHERE Id =:monthlyHoursId];
            if(fal.Contribution_to_HRA_Benefit__c != null){
                listcomp.add('HRA');
            }if(fal.Contribution_to_Paid_Leave_Benefit__c != null){
                listcomp.add('Paid Leave Fringe Reserve Account');
            }if(fal.Contribution_to_RetBenefit__c != null){
                listcomp.add('Retirement');   
            }
            for(Individual_Fringe_Benefits_Percentages__c per : [SELECT Id, Name, Percentage__c, Fringe_Contract__c, Individual_Fringe_Benefit_Account__c FROM Individual_Fringe_Benefits_Percentages__c WHERE Fringe_Contract__c =: fal.Fringe_Contract__c]){
                ifbp.add(per.Individual_Fringe_Benefit_Account__c);
            }
            
            Set<String> set1 = new Set<String>(listcomp);
            set1.removeAll(ifbp);
            for(String pname : set1){
                Individual_Fringe_Benefits_Percentages__c op = new Individual_Fringe_Benefits_Percentages__c();
                op.Individual_Fringe_Benefit_Account__c = pname;
                op.Percentage__c = 0.00;
                op.Fringe_Contract__c = fal.Fringe_Contract__c;
                ip.add(op);
            }
            
            Fringe_Contract__c fc = [SELECT Id, Name, HRA__c, Retirement__c, PaidLeaveFringeReserveAccount__c FROM Fringe_Contract__c WHERE Id =: fal.Fringe_Contract__c];
            if(fc.HRA__c == null){
                fc.HRA__c = String.valueOf(0.00);
            }if(fc.PaidLeaveFringeReserveAccount__c == null){
                fc.PaidLeaveFringeReserveAccount__c = String.valueOf(0.00);
            }if(fc.Retirement__c == null){
                fc.Retirement__c = String.valueOf(0.00);
            }
            insert ip; 
            update fc;
        }catch(Exception e){e.getMessage();}
    }
}