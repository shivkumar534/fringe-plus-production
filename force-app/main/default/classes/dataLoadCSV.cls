public class dataLoadCSV {
    @AuraEnabled
    public static List<Communityobjects__c> getCustomSettingDetails() {
        List<Communityobjects__c> mcs = Communityobjects__c.getall().values();
        return mcs;
    }
    @AuraEnabled
    public static void bulkapijob( Id recordId,string ObjectName,String operation){
        system.debug('recordId'+recordId);
        ContentVersion cv = [SELECT Id, ContentDocumentId, VersionData FROM ContentVersion WHERE ContentDocumentId =: recordId ];
        bulkapijob1(cv.VersionData.toString(),ObjectName,operation);
    }
    @future (callout=true)
    public static void bulkapijob1(string csvFileBody,string ObjectName ,string operation){        
        system.debug('csvFileLines'+csvFileBody);
        system.debug('csvFileLines'+ObjectName);
        system.debug('csvFileLines'+operation);
        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setEndpoint(URl.getOrgDomainUrl().toExternalForm()+'/services/data/v50.0/jobs/ingest');
        request.setHeader('content-type', 'application/json' );
        request.setHeader('Authorization' ,'Bearer '+userInfo.getSessionId() );          
        request.setBody('{"operation":"'+operation+'", "object":"'+ObjectName+'", "contentType":"CSV", "lineEnding":"CRLF"}');
        Http h = new Http();
        HttpResponse resp = new HttpResponse();
        if(Test.isRunningTest()){
            resp.setBody('{"id":"7506t000000B7EvAAK","operation":"insert","object":"Employee_Line_Item_Hours__c","createdById":"0056t000000Q9NWAA0","createdDate":"2021-08-04T14:26:06.000+0000","systemModstamp":"2021-08-04T14:26:06.000+0000","state":"Open","concurrencyMode":"Parallel","contentType":"CSV","apiVersion":50.0,"contentUrl":"services/data/v50.0/jobs/ingest/7506t000000B7EvAAK/batches","lineEnding":"CRLF","columnDelimiter":"COMMA"}');
            resp.setStatusCode(200); 
            resp.setStatus('OK');
        }else{
            resp = h.send(request);
        }
        System.debug('------'+resp.getBody());
        if(resp.getStatusCode()==200){        
            Map<String, Object> respMap = (Map<String, Object>) Json.deserializeUntyped(resp.getBody());
            String jobId = (String)respMap.get('id');
            system.debug('bulkapijob id'+jobId);
            
            system.debug('DATA ADDED SUCESSFULLY');
            //system.debug('url'+URl.getOrgDomainUrl().toExternalForm()+'/services/data/v50.0/jobs/ingest/'+jobID+'/batches');
            HttpRequest request1 = new HttpRequest();
            request1.setMethod('PUT');
            request1.setEndpoint(URl.getOrgDomainUrl().toExternalForm()+'/services/data/v50.0/jobs/ingest/'+jobID+'/batches');
            request1.setHeader('Content-Type', 'text/csv' );
            request1.setHeader('Authorization' ,'Bearer '+userInfo.getSessionId());          
            request1.setBody(csvFileBody);
            Http h1 = new Http();
            HttpResponse resp1 = new HttpResponse();
            if(Test.isRunningTest()){
                resp1.setStatusCode(200); 
                resp1.setStatus('OK');
            }else{
                resp1 = h1.send(request1);
            }
            System.debug('------'+resp1.getBody());
            if(resp1.getStatusCode()<299){ 
                system.debug('DATA COMPLETED');
                HttpRequest request2 = new HttpRequest();
                request2.setMethod('PATCH');
                request2.setEndpoint(URl.getOrgDomainUrl().toExternalForm()+'/services/data/v50.0/jobs/ingest/'+jobID);
                request2.setHeader('Content-Type','application/json');
                request2.setHeader('Authorization' ,'Bearer '+userInfo.getSessionId());
                request2.setBody('{"state" : "UploadComplete"}');
                Http h2 = new Http();
                HttpResponse resp2= new HttpResponse();
                if(Test.isRunningTest()){
                    resp2.setBody('{"id":"7506t000000B7EvAAK","operation":"insert","object":"Employee_Line_Item_Hours__c","createdById":"0056t000000Q9NWAA0","createdDate":"2021-08-04T14:26:06.000+0000","systemModstamp":"2021-08-04T14:26:06.000+0000","state":"UploadComplete","concurrencyMode":"Parallel","contentType":"CSV","apiVersion":50.0}');
                    resp2.setStatusCode(201); 
                    resp2.setStatus('OK');
                }else{
                    resp2 = h2.send(request2);
                }
                System.debug('------'+resp2.getBody());
                if(resp2.getStatusCode()<299){
                    system.debug('response'+resp2.getStatusCode());
                    Http h3 = new Http();
                    HttpRequest request3 = new HttpRequest();
                    request3.setMethod('GET');
                    request3.setEndpoint(URl.getOrgDomainUrl().toExternalForm()+'/services/data/v50.0/jobs/ingest/'+jobID+'/');
                    request3.setHeader('Content-Type','application/json');
                    //request3.setBody('{"state" : "Closed"}');
                    request3.setHeader('Authorization' ,'Bearer '+userInfo.getSessionId());                                
                    HttpResponse response3 = new HttpResponse();
                    if(Test.isRunningTest()){
                        response3.setStatusCode(201); 
                        response3.setStatus('OK');
                    }else{
                        response3 = h3.send(request3);
                    }
                    system.debug('Beforeresponse3:'+response3.getStatusCode()); 
                    System.debug('------'+response3.getBody());
                    if(response3.getStatusCode()<299){
                        //blob csvFile = response3.getBody();
                        system.debug('response3:-----'+response3.getBody()); 
                        //Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response3.getBody());
                        //system.debug('results----'+results.get('state'));
                        
                        //if((results.get('state')=='UploadComplete')){
                        Http h4 = new http();
                        HttpRequest request4 = new HttpRequest();
                        request4.setMethod('GET');
                        request4.setEndpoint(URl.getOrgDomainUrl().toExternalForm()+'/services/data/v50.0/jobs/ingest/'+jobID+'/successfulResults');
                        request4.setHeader('Content-Type','application/json; charset=UTF-8');
                        request4.setHeader('Authorization' ,'Bearer '+userInfo.getSessionId());                                
                        HttpResponse response4 = new HttpResponse();
                        
                        if(Test.isRunningTest()){
                            response4.setStatusCode(201); 
                            response4.setStatus('OK');
                        }else{
                            response4 = h4.send(request4);
                        }
                        System.debug('------'+response4.getBody());
                        if(response4.getStatusCode()<299){
                            blob csvFile = Blob.valueOf(response4.getBody());
                            system.debug('response4:csvFile----'+csvFile);
                            system.debug('response4:----'+String.valueOf(csvFile)); 
                            //Map<String, Object> results1 = (Map<String, Object>) JSON.deserializeUntyped(response4.getBody());
                            ///system.debug('results4------'+results1);
                        }
                        // }
                        
                    }
                }
            }
        }        
    }    
}