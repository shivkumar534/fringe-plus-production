global with sharing class MethodCbatchclass implements Database.batchable<Id>,Database.Stateful,Database.AllowsCallouts{ 
    List<Id> recordList = new List<Id>();
    
    global MethodCbatchclass(List<Id> recordIdList){
        recordList = recordIdList;
    }
    global List<Id> start(Database.BatchableContext BC){
        return recordList;
    }
    
    global void execute(Database.BatchableContext BC, List<Id> recordList){
        if(recordList.size() > 0){       
            for(Id recordId : recordList){
                MethodCHandler.Method(recordId);
                System.debug('This method-c from Batch class');
            }
        }
    }
    global void finish(Database.BatchableContext BC){
      
    }
}