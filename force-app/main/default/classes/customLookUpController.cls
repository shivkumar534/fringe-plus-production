public class customLookUpController {
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String ObjectName,string whereCondition) {
        system.debug('ObjectName-->' + ObjectName);
        system.debug('ObjectName-->' + whereCondition);
        String searchKey = searchKeyWord + '%';
        
        List < sObject > returnList = new List < sObject > ();
      	String sQuery ='';
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        
        
        if(whereCondition != '' || whereCondition != null)
           sQuery =  'select id, Name from ' +ObjectName + ' where Name LIKE: searchKey '+whereCondition+' order by createdDate DESC limit 5'; 
        else
           sQuery =  'select id, Name from ' +ObjectName + ' where Name LIKE: searchKey order by createdDate DESC limit 5';    
        
        system.debug('sQuery-->' + sQuery);
        List < sObject > lstOfRecords = Database.query(sQuery);        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
}