global with sharing class EmployeeTerminationInvoiceController {
    
    public String currentRecordId {get;set;}
    public decimal allcomponents {get;set;}
    public decimal fringeallIPRA {get;set;}
    public decimal hourBankAcc {get;set;}
    public decimal fringeallHBA {get;set;}
    public Long invoicedateTime {get;set;}
    public Date invoicedate {get;set;}
    public decimal totalOfComponents {get;set;}
    public String selectedInvoiceId { get; set; }
    public Fringe_Remittance_Invoice__c fri {get;set;}
    public Employee__c emp {get;set;}
    public Date Today { get { return Date.today(); }}
    
    public EmployeeTerminationInvoiceController(){
        if(Test.isRunningTest()){
            currentRecordId = [SELECT Id FROM Employee__c LIMIT 1].Id;
        }else{
            currentRecordId  = ApexPages.CurrentPage().getparameters().get('id');
        }
    }
    
    public void fetchPDFData(){
        if(currentRecordId != null){
            emp = [SELECT Id,Monthly_Fringe_Contribution__c,LastModifiedDate,Rolling_Individual_Premium_Reserve__c,FP_Employee_Status__c,First_Name__c, Last_Name__c,Unique_Identifier__c,Emp_Status__c,Zip_Code__c,Monthly_fringe_obligtion_result__c,Name,Fringe_Rate_lookup__c, Govt_Contractor__c,Govt_Contractor__r.Name,Curr_Reg_Hrs_NYC__c,Monthly_Cost_Of_Benefits__c,Fringe_Contract_Type__r.Contract_Name__c,Fringe_Contract_Type__r.Id FROM Employee__c WHERE Id =: currentRecordId];
            if(emp != null){
                decimal totalvalue = 0;
                Id empId = emp.Id;
                DateTime dt = System.now();
                invoicedate = Date.today();
                invoicedateTime = dt.getTime();
                String listOfFields = Dynamic_Fields__c.getValues(emp.Fringe_Contract_Type__r.Id) == null ? '' : Dynamic_Fields__c.getValues(emp.Fringe_Contract_Type__r.Id).List_of_Fields__c;
                String queryString = 'SELECT '+listOfFields+',Rolling_Hour_Bank_Account_Balance__c,Monthly_Short_List__c,Fringe_Rate__c,RetroActive_Adjustment_By_Hours__c,Monthly_Fringe_Obligation_Result__c,Monthly_Cost_Of_Benefit__c,Retro_Active__c,Curr_Reg_Hours_NYC__c,Month_and_Year__c,Employee__r.Id, Employee__r.Name  FROM Individual_Fringe_Benfit__c WHERE Employee__r.Id =:  empId AND Retro_Active__c = false';
                System.debug('-----'+queryString);
                List<Individual_Fringe_Benfit__c> cusSetRec;
                try{
                    cusSetRec= Database.query(queryString);
                    fringeallIPRA = 0;
                    fringeallHBA = 0;
                    if(cusSetRec.size() > 0){
                        for(Individual_Fringe_Benfit__c ind : cusSetRec){
                            System.debug('----'+ind);
                            fringeallIPRA = emp.Rolling_Individual_Premium_Reserve__c != null?fringeallIPRA+emp.Rolling_Individual_Premium_Reserve__c:fringeallIPRA;//Rolling_Individual_Premium_Reserve__c
                            fringeallHBA = (decimal)ind.get('Rolling_Hour_Bank_Account_Balance__c') != null?fringeallHBA+(decimal)ind.get('Rolling_Hour_Bank_Account_Balance__c'):fringeallHBA;
                            if(ind != null){
                                for(String indiviField : listOfFields.split(',')){
                                    if(!String.isBlank(indiviField)){
                                        String objectName = 'Individual_Fringe_Benfit__c';
                                        String fieldName = indiviField;
                                        SObjectType r = ((SObject)(Type.forName('Schema.'+objectName).newInstance())).getSObjectType();
                                        DescribeSObjectResult d = r.getDescribe();
                                        String fieldType = String.valueOf(d.fields.getMap().get(fieldName).getDescribe().getType());
                                        If(fieldType.equalsIgnoreCase('STRING')){
                                            String strvalue = (String)ind.get(fieldName) != null?(String)ind.get(fieldName):'0.00';
                                            totalvalue = totalvalue == 0 || totalvalue == null ?Decimal.valueOf(strvalue):totalvalue+Decimal.valueOf(strvalue);
                                        }else{
                                            decimal decvalue = (decimal)ind.get(fieldName) != null ? (decimal)ind.get(fieldName) : 0.00;
                                            totalvalue = totalvalue == 0 || totalvalue == null ?decvalue:totalvalue+decvalue;
                                        }
                                    }
                                    //totalvalue = totalvalue = 0?
                                }
                            }
                        }
                        allcomponents = totalvalue;
                        hourBankAcc = fringeallHBA;
                        fringeallIPRA = fringeallIPRA;
                        fringeallHBA = allcomponents+ fringeallIPRA+hourBankAcc;
                    }
                }catch(Exception e){
                    e.getMessage();
                }
            }
        }
    }
    
     WebService static void updateStatus(Id recordId) {  
        System.debug('----'+recordId);
        //add logic here
    }
    @RemoteAction @AuraEnabled
    public static Map<String, String> updateAllValuesToZero(Id empId){
        System.debug('------'+empId);
        Date createIFB = Date.today();
        Integer newmon = createIFB.month();
        Integer year = createIFB.year();
        Map<String, String> result = new Map<String, String>();
         Map<Integer,String> monthNameMap = new Map<Integer, String>{1 =>'January', 2=>'February', 3=>'March', 4=>'April', 5=>'May',6=>'June', 7=>'July', 8=>'August', 9=>'September',10=>'October',11=>'November', 12=>'December'};
                                    String monthyear = monthNameMap.get(newmon)+' '+year;
        if(empId != null){
            Individual_Fringe_Benfit__c ifb;
            try{
                 ifb = [SELECT Id, Name, Month_and_Year__c, Employee__c FROM Individual_Fringe_Benfit__c WHERE Employee__c =: empId AND Month_and_Year__c =:monthyear];
            }catch(Exception e){e.getMessage();}
            if(ifb == null){
                Employee__c emp = [SELECT Id,Fringe_Contract_Type__c,Hourly_Cost_Of_benefit2__c,Monthly_Fringe_Contribution__c,Rate__c,Rolling_Balance_of_IPRA__c,Monthly_Cost_Of_Benefit__c,Govt_Contractor__r.Id,Rolling_Hour_Bank_Account_Balance__c,Job_Classification__c,After_Retro_Cost_Of_Benefit_Adjustments__c,Fringe_Contract__c,Fringe_Contract_Type__r.Fr__c,Coverage_Tier__c,Avg_Cost_Hrs__c,Monthly_fringe_obligtion_result__c,Health_Insurance_Plan__c,Dental_Insurance_Plan__c,Life_Insurance_Plan__c,Vision_Insurance_Plan__c,Plan_1__c,Plan_2__c,Plan_3__c,Name,Actual_Monthly_Hours__c,Fringe_Rate_lookup__c, Curr_Reg_Hrs_NYC__c,Monthly_Cost_Of_Benefits__c, Fringe_Contract_Type__r.Hour_Bank__c,Benefit_Amount_Allocated__c,Fringe_Contract_Type__r.Fringe_Allocation_Method__c,Fringe_Contract_Type__r.Avg_Accounting_Rules__c,Roll_Back_Hours__c,Fringe_Contract_Type__r.Id FROM Employee__c WHERE Id =: empId];
                Fringe_Contract__c fc = [SELECT Id, Fringe_Allocation_Rules__r.Id,Fringe_Allocation_Rules__r.Name,Fringe_Allocation_Rules__r.OverspentCategory__c, Fringe_Allocation_Rules__r.UnderspentCategory__c FROM Fringe_Contract__c WHERE Id =: emp.Fringe_Contract_Type__c ];
                    
                if((emp.Rolling_Balance_of_IPRA__c != null || emp.Rolling_Hour_Bank_Account_Balance__c != null) && (emp.Rolling_Balance_of_IPRA__c > 0 || emp.Rolling_Hour_Bank_Account_Balance__c > 0)){
                    Individual_Fringe_Benfit__c ifbnew =new Individual_Fringe_Benfit__c();
                    ifbnew.Employee__c = emp.Id;
                    if(emp.Rolling_Balance_of_IPRA__c != null){
                        emp.Rolling_Balance_of_IPRA__c = emp.Rolling_Balance_of_IPRA__c != null && (emp.Rolling_Balance_of_IPRA__c - emp.Monthly_Cost_Of_Benefit__c) > 0? emp.Rolling_Balance_of_IPRA__c - emp.Monthly_Cost_Of_Benefit__c : emp.Rolling_Balance_of_IPRA__c;    
                        ifbnew.Rolling_Balance_of_IPRA__c = emp.Rolling_Balance_of_IPRA__c;
                        
                    }else{
                        if(emp.Rolling_Hour_Bank_Account_Balance__c != null){
                            if(fc.Fringe_Allocation_Rules__r.OverspentCategory__c == 'Debit from Hour Bank Account and assign it to "Number of Hours Withdrawn"' || Fc.Fringe_Allocation_Rules__r.UnderspentCategory__c == 'Allocate to Hour Bank Reserve Account Balance'){
                                decimal rollingHours = emp.Rolling_Hour_Bank_Account_Balance__c != null && emp.Rate__c != null ? ((emp.Rolling_Hour_Bank_Account_Balance__c * emp.Rate__c) - emp.Hourly_Cost_Of_benefit2__c) < 0?emp.Rolling_Hour_Bank_Account_Balance__c:(emp.Rolling_Hour_Bank_Account_Balance__c * emp.Rate__c) - emp.Hourly_Cost_Of_benefit2__c : emp.Rolling_Hour_Bank_Account_Balance__c != null?emp.Rolling_Hour_Bank_Account_Balance__c:0.00;    
                                emp.Rolling_Hour_Bank_Account_Balance__c = rollingHours > 0 && emp.Rate__c != null ? rollingHours/ emp.Rate__c:0.00;
                                ifbnew.Monthly_Short_List__c = (rollingHours - (emp.Hourly_Cost_Of_benefit2__c != null?emp.Hourly_Cost_Of_benefit2__c:0.00)) < 0 ? rollingHours - (emp.Hourly_Cost_Of_benefit2__c != null?emp.Hourly_Cost_Of_benefit2__c:0.00):0.00;
                                ifbnew.Fringe_Result_Category__c = (rollingHours - (emp.Hourly_Cost_Of_benefit2__c != null?emp.Hourly_Cost_Of_benefit2__c:0.00)) < 0.00 ? 'Overspent':'Underspent';
                            }
                        }
                    }
                    try{ update emp;}catch(Exception e){result.put('failed',e.getMessage());}
                    ifbnew.Month_and_Year__c = monthyear;
                    ifbnew.Monthly_Cost_Of_Benefit__c = emp.Monthly_Cost_Of_Benefit__c;
                    try{ insert ifbnew;}catch(Exception e){result.put('failed',e.getMessage());}
                    result.put('success', 'IFB record created now.');
                }else{
                    Individual_Fringe_Benfit__c ifbnew =new Individual_Fringe_Benfit__c();
                    ifbnew.Month_and_Year__c = monthyear;
                    ifbnew.Employee__c = emp.Id;
                    ifbnew.Monthly_Cost_Of_Benefit__c = emp.Monthly_Cost_Of_Benefit__c;
                    ifbnew.Monthly_Short_List__c = -(emp.Monthly_Cost_Of_Benefit__c != null?emp.Monthly_Cost_Of_Benefit__c:emp.Hourly_Cost_Of_benefit2__c != null?emp.Hourly_Cost_Of_benefit2__c:0.00);
                    ifbnew.Fringe_Result_Category__c = 'Overspent';
                    try{ insert ifbnew;}catch(Exception e){result.put('failed',e.getMessage());}
                    result.put('success', 'IFB record created now.');
                }
            }else{
                result.put('RecordFound', 'IFB record already created');
                //already records are created
            }
            
        }
       return result;
    }
    
    public static void createIFBRecordsAfterTermination(List<Id> empIdList){
        if(empIdList.size() > 0){
            List<Employee__c> empList = new List<Employee__c>();
            for(Employee__c emp: [SELECT Id, Curr_Reg_Hrs_NYC__c FROM Employee__c WHERE Id IN: empIdList]){
                emp.Curr_Reg_Hrs_NYC__c = 0;
                empList.add(emp);
            }
            
            update empList;
        }
    }
}