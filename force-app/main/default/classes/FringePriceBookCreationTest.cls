@isTest
public class FringePriceBookCreationTest {
    @isTest
    public static void createpricebook(){
        Govt_Contractor__c acc= new Govt_Contractor__c();
        acc.Name = 'FBA';
        //acc.Job_Classification__c = 'Diver 1 (Wet);Diver 2 (Standby);Drywall Installer/Lather;Drywall Stocker/Scrapper;Carpenter 1 (Bridge)';
        insert acc;
        
        Fringe_Allocation_Rules__c far = new Fringe_Allocation_Rules__c();
        far.Name = 'Method A1';
        far.FringeBenefits__c = 'HRA;Paid Leave Fringe Reserve Account';
        far.OverspentCategory__c = 'Maintain Monthly Short List/Report';
        far.UnderspentCategory__c = 'Allocate to Individual Fringe Benefit Account Types';
        
        insert far;
        
        Fringe_Contract__c fc = new Fringe_Contract__c();
        fc.Contract_Name__c = 'Test1';
        fc.Govt_Contractor__c = acc.Id;
        fc.Fringe_Allocation_Rules__c = far.Id;
        fc.Fringe_Rate__c = 5;
        fc.Hour_Bank__c = 120;
         fc.HRA__c = '50';
         fc.PaidLeave__c = '50';
        fc.Contract_Type__c = 'SCA - Service Contract Act';
        fc.Benefit_Plan__c = 'Health Insurance;Health Reimbursement Accounts (HRAs);Dental Insurance';
        //fc.Fringe_Rate_Based_on_Job_Classification__c = true;
        fc.SELECT_COST_OF_BENEFITS__c = 'Monthly Cost of Benefits';
        fc.Reporting_Type__c = 'Monthly Hours Report';
        
        insert fc;
        
        Fringe_PriceBook__c fp = new Fringe_PriceBook__c();
        fp.Name = 'Name123';
        fp.Description__c = 'Hello';
        fp.Fringe_Contract__c = fc.Id;
        fp.Govt_Contractor__c = acc.Id;
        fp.Is_Job_Classification__c = false;
        fp.Hourly_Cost_of_Benefit__c = false;
        fp.Monthly_Cost_Of_Benefit__c = true;
        
        insert fp;
        
        Fringe_Product__c fpc = new Fringe_Product__c();
        fpc.Name = 'Employee only';
        fpc.Active__c = true;
        fpc.Description__c = 'Hello';
        fpc.Job_Classification__c = false;
        fpc.Fringe_PriceBook__c = fp.Id;
        fpc.Product_Code_Product_Type__c = 'BCBS SILVER PLAN';
        fpc.Product_Name__c = 'Cost of Benefit';
        //fpc.Product_Name_Coverage_Type__c = 'Employee Only';
        fpc.Unit_Price__c = 100;
        
        insert fpc;
        
       fpc.Unit_Price__c = 110;
        update fpc;
        // Create a Pricebook
        
        /*Pricebook2 priceBook = new Pricebook2(
            Name = 'Example Price Book',
            Description = 'This is the Price Book description.',
            IsActive = true
        );
        
        insert priceBook;
        
        // Create a Product
        
        Product2 product = new Product2(
            Name = 'Example Product',
            Description = 'This is the Product description.',
            ProductCode = 'EX1234',
            StockKeepingUnit = 'EX5678',
            Family = 'Example Product Family',
            QuantityUnitOfMeasure = 'inches',
            DisplayUrl = 'https://www.example.com/',
            ExternalId = 'ID #1234',
            ExternalDataSourceId = '0XCXXXXXXXXXXXXXXX',
            IsActive = true
        );
        
        insert product;
        
        // Get the Standard Price Book ID
        
        Pricebook2 standardPriceBook = [
            SELECT Id
            FROM Pricebook2
            WHERE isStandard = true
            LIMIT 1
        ];
        
        // Insert the Product in the Standard Price Book (if necessary)
        
        PricebookEntry standardPriceBookEntry = new PricebookEntry(
            Pricebook2Id = standardPriceBook.Id,
            Product2Id = product.Id,
            UnitPrice = 100.00,
            UseStandardPrice = false,
            IsActive = true
        );
        
        insert standardPriceBookEntry;
        
        // Insert the Product in the New Price Book
        
        PricebookEntry priceBookEntry = new PricebookEntry(
            Pricebook2Id = priceBook.Id,
            Product2Id = product.Id,
            UnitPrice = 100.00,
            UseStandardPrice = false,
            IsActive = true
        );
        
        insert priceBookEntry;*/
    }
	
    @isTest
    public static void createpricebook1(){
        Govt_Contractor__c acc= new Govt_Contractor__c();
        acc.Name = 'FBA';
        //acc.Job_Classification__c = 'Diver 1 (Wet);Diver 2 (Standby);Drywall Installer/Lather;Drywall Stocker/Scrapper;Carpenter 1 (Bridge)';
        insert acc;
        
        Fringe_Allocation_Rules__c far = new Fringe_Allocation_Rules__c();
        far.Name = 'Method A1';
        far.FringeBenefits__c = 'HRA;Paid Leave Fringe Reserve Account';
        far.OverspentCategory__c = 'Maintain Monthly Short List/Report';
        far.UnderspentCategory__c = 'Allocate to Individual Fringe Benefit Account Types';
        
        insert far;
        
        Fringe_Contract__c fc = new Fringe_Contract__c();
        fc.Contract_Name__c = 'Test1';
        fc.Govt_Contractor__c = acc.Id;
        fc.Fringe_Allocation_Rules__c = far.Id;
        fc.Fringe_Rate__c = 5;
        fc.Hour_Bank__c = 120;
         fc.HRA__c = '50';
         fc.PaidLeave__c = '50';
        fc.Contract_Type__c = 'SCA - Service Contract Act';
        fc.Benefit_Plan__c = 'Health Insurance;Health Reimbursement Accounts (HRAs);Dental Insurance';
        //fc.Fringe_Rate_Based_on_Job_Classification__c = true;
        fc.SELECT_COST_OF_BENEFITS__c = 'Monthly Cost of Benefits';
        fc.Reporting_Type__c = 'Monthly Hours Report';
        
        insert fc;
        
        Fringe_PriceBook__c fp = new Fringe_PriceBook__c();
        fp.Name = 'Name123';
        fp.Description__c = 'Hello';
        fp.Fringe_Contract__c = fc.Id;
        fp.Govt_Contractor__c = acc.Id;
        fp.Is_Job_Classification__c = false;
        fp.Hourly_Cost_of_Benefit__c = false;
        fp.Monthly_Cost_Of_Benefit__c = true;
        
        insert fp;
        
        Fringe_Product__c fpc = new Fringe_Product__c();
        fpc.Name = 'Employee only';
        fpc.Active__c = true;
        fpc.Description__c = 'Hello';
        fpc.Job_Classification__c = false;
        fpc.Fringe_PriceBook__c = fp.Id;
        fpc.Product_Code_Product_Type__c = 'BCBS SILVER PLAN';
        fpc.Product_Name__c = 'Cost of Benefit';
        //fpc.Product_Name_Coverage_Type__c = 'Employee Only';
        fpc.Unit_Price__c = 100;
        
        insert fpc;
        
       fpc.Unit_Price__c = 110;
        update fpc;
        
        fp.Effactive_Date__c = System.today();
        update fp;
      }
}