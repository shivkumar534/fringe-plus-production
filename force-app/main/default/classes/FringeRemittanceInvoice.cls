public class FringeRemittanceInvoice {
    
    @AuraEnabled
    public static Fringe_Contract__c getLocationValue(Id recId){
        return [SELECT Id, name,Location__c,Fr__c FROM Fringe_Contract__c WHERE Id =:recId];
    }
    @AuraEnabled
    public static List<RecordType> getRecordTypeValue(){
        return [Select Id,Name From RecordType where sobjecttype ='Fringe_Remittance_Invoice__c'];
    }
    @AuraEnabled
    public static string createInvoice( Fringe_Remittance_Invoice__c objInvoice){
        system.debug('>>>>>'+objInvoice);
        try{
            insert objInvoice;
        } catch(Exception e){
            System.debug(e.getMessage());
            System.debug(e.getLineNumber());
        }
        return objInvoice.Id;
    }
    
    @AuraEnabled
    public static ListView getListViews() {
        return[SELECT Id, Name FROM ListView WHERE SobjectType = 'Fringe_Remittance_Invoice__c' and Name='All'];        
    }
    
    public List<Account> accList = new List<Account>();
    
    public List<SelectOption> AccountOptionList
    {
        get
        {
            accList = [Select ID,Name from Account ];
            
            AccountOptionList = new List<SelectOption>();
            
            for(Account acc : accList)
            {
                AccountOptionList.add(new SelectOption(acc.Id, acc.Name));
            }
            return AccountOptionList;
        }
        set;
    }
    // List<Fringe_Remittance_Invoice__c> frList {set;get;}
    public String AccountName {get; set;}
    public ID locationID {get;set;}   
    public String selectedInvoiceId { get; set; }
    public Fringe_Remittance_Invoice__c fri {get;set;}
    public List<Employee__c> empList {get;set;}
    public String benefitmonth { get; set; }
    public String benefitmon { get; set; }
    public List<Recordtype> rt{get;set;}
    public List<String> js{get;set;}
    public FringeRemittanceInvoice(){
        //frList=(List<Fringe_Remittance_Invoice__c>)controller.getSelected();
        selectedInvoiceId = ApexPages.currentPage().getParameters().get('Id'); 
    }
    public void fetchPDFData(){
        List<string> jsList = new List<string>();
        Id fcID ;
        Id UsId = UserInfo.getUserId();
        try{
            User us= [SELECT Id, ContactId from User where Id=:UsId and ContactId != null]; 
            if(us != null){
                Contact con = [SELECT Id, Name, Account.Name from Contact where Id=:us.ContactId and AccountId != null];
                AccountName = con.Account.Name;
            }
        }catch(Exception e){
            e.getMessage();
        }
        fri = [ SELECT Id,Fringe_Contract__c,Fringe_Contract__r.Contract_Name__c,Job_Site_Location__c,Job_Site_Locations__c,Fringe_Contract__r.Location__c,Name,Bills_To__c,Bills_To__r.Name,Bills_To__r.Phone__c,Bills_To__r.Fax__c,Bills_To__r.Website__c,Bills_To__r.Billing_Street__c,Bills_To__r.Billing_City__c,Bills_To__r.Billing_State_Province__c,Bills_To__r.Billing_Country__c,Bills_To__r.Billing_Zip_Postal_Code__c,Job_Site_Location__r.Name,Benefit_Month__c,Benefit_Termination_Date__c FROM Fringe_Remittance_Invoice__c  WHERE Id =:selectedInvoiceId ];
        
        fcID = fri.Fringe_Contract__c;
        system.debug(fcID);
        benefitmonth = fri.Benefit_Month__c;
        system.debug(benefitmonth);
        rt = [SELECT Id,Name from RecordType where sObjectType='Fringe_Remittance_Invoice__c'];  
        
        String JobSiteLocations = fri.Job_Site_Locations__c;
        jsList = JobSiteLocations.split(';');
        system.debug(jsList);
        js = new List<String>();
        for(string jsVal : jsList)
        {
            js.add(jsVal.trim());
            system.debug(js);
        }
        if(fri.Benefit_Termination_Date__c == null){
        if (js != null){
            empList = [SELECT Id, Name,Unique_Identifier__c,Monthly_Fringe_Contribution__c,Job_Site_Locations__r.Name,Job_Site_Locations__c,Monthly_Cost_Of_Benefits__c,COBRA_Status__c,Benefit_Period_Month_and_year__c,Month_and_Year__c,Coverage_Effective_From_Date__c,Coverage_Effective_To_Date__c FROM Employee__c WHERE Fringe_Contract_Type__c =: fcID AND Job_Site_Locations__r.Name IN :js AND Benefit_Period_Month_and_year__c =: benefitmonth];
            system.debug(empList);
            if(empList.size() > 0){
                Employee__c em = empList[0];
                benefitmon = em.Month_and_Year__c;  
            }else
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter valid Employee details'));
        }
        else{
            empList = [SELECT Id, Name,Unique_Identifier__c,Monthly_Fringe_Contribution__c,Job_Site_Locations__r.Name,Monthly_Cost_Of_Benefits__c,Job_Site_Locations__c,COBRA_Status__c,Month_and_Year__c,Benefit_Period_Month_and_year__c,Coverage_Effective_To_Date__c,Coverage_Effective_From_Date__c FROM Employee__c WHERE Fringe_Contract_Type__c =: fcID];
            if(empList.size() > 0){
                Employee__c em = empList[0];
                benefitmon = em.Month_and_Year__c; 
            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter valid Employee details'));
            }
            
        }
        }else{
        if (js != null ){
            empList = [SELECT Id, Name,Unique_Identifier__c,Monthly_Fringe_Contribution__c,Job_Site_Locations__r.Name,Job_Site_Locations__c,Monthly_Cost_Of_Benefits__c,COBRA_Status__c,Benefit_Period_Month_and_year__c,Month_and_Year__c,Coverage_Effective_From_Date__c,Coverage_Effective_To_Date__c FROM Employee__c WHERE Fringe_Contract_Type__c =: fcID AND Job_Site_Locations__r.Name IN :js];
            system.debug(empList);
            if(empList.size() > 0){
                Employee__c em = empList[0];
                benefitmon = em.Month_and_Year__c;  
            }else
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter valid Employee details'));
        }
        else{
            empList = [SELECT Id, Name,Unique_Identifier__c,Monthly_Fringe_Contribution__c,Job_Site_Locations__r.Name,Monthly_Cost_Of_Benefits__c,Job_Site_Locations__c,COBRA_Status__c,Month_and_Year__c,Benefit_Period_Month_and_year__c,Coverage_Effective_To_Date__c,Coverage_Effective_From_Date__c FROM Employee__c WHERE Fringe_Contract_Type__c =: fcID];
            if(empList.size() > 0){
                Employee__c em = empList[0];
                benefitmon = em.Month_and_Year__c; 
            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter valid Employee details'));
            }
            
        }
    }
}

    public Date Today { get { return Date.today(); }}   
}