global with sharing class avgQtrCostBatchClass implements Database.batchable<Id>,Database.Stateful,Database.AllowsCallouts{ 
    List<Id> recordList = new List<Id>();
    
    global avgQtrCostBatchClass(List<Id> recordIdList){
        recordList = recordIdList;
    }
    global List<Id> start(Database.BatchableContext BC){
        return recordList;
    }
    
    global void execute(Database.BatchableContext BC, List<Id> recordList){
        if(recordList.size() > 0){       
            for(Id recordId : recordList){
                UpdateAvgCostToFringeBenefits.updateAvgCostBenefits(recordId);                
            }
        }
    }
    global void finish(Database.BatchableContext BC){
      
    }
}