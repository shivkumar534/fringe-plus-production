// Author : Diwakar
// Date : 20th April 2021
public class ClassLevelBenfits {
     
    Public Static Void benefits(List<Id> monthlyHourId) {
        System.debug('Monthly Hours id is :'+monthlyHourId);
        String empUniqId,employeeId;
        decimal MonthHours;
        String contractId, Month_and_Year;
        try {
            // Monthly Hours List
				Monthly_Hours__c mon = [SELECT Id, Name, EE__c,Month_and_Year__c, Curr_Reg_Hrs_NYC__c FROM Monthly_Hours__c WHERE Id =: monthlyHourId];
					empUniqId = mon.EE__c;
            			System.debug('Employee Name is :'+empUniqId);
					MonthHours = mon.Curr_Reg_Hrs_NYC__c;
            			System.debug('Hours is :'+MonthHours);
					Month_and_Year = mon.Month_and_Year__c;
            			System.debug('Month and Year is :'+Month_and_Year);
            // Employee List
				Employee__c emp = [SELECT Id,Name,Month_and_Year__c,Fringe_Contract_Type__r.Id from employee__C Where Name =: empUniqId];
					contractId = emp.Fringe_Contract_Type__r.Id;
            			System.debug('Contract id is :'+contractId);
					employeeId = emp.Id;
            			System.debug('Employee id is :'+employeeId);
					//Month_and_Year = emp.Month_and_Year__c ;
            		emp.Curr_Reg_Hrs_NYC__c = MonthHours;
            		try{
                        	System.debug('emp size is :'+emp);
                			Update emp;
            			}catch(Exception e){e.getMessage();}
            // Fringe allocation rules on Fringe contracts
            	Fringe_Contract__c Fringrules = [SELECT Id, Fringe_Allocation_Rules__r.Id,Fringe_Allocation_Rules__r.Name,
                                                 Fringe_Allocation_Rules__r.Class_I_From_Hours__c,
                                                 Fringe_Allocation_Rules__r.Class_II_From_Hours__c,
                                                 Fringe_Allocation_Rules__r.Class_III_From_Hours__c,
                                                 Fringe_Allocation_Rules__r.Class_I_To_Hours__c,
                                                 Fringe_Allocation_Rules__r.Class_II_To_Hours__c,
                                                 Fringe_Allocation_Rules__r.Class_III_To_Hours__c
                                                 FROM Fringe_Contract__c WHERE Id =: contractId];
            		System.debug('1 from Hours is :'+Fringrules.Fringe_Allocation_Rules__r.Class_I_From_Hours__c);
            		System.debug('2 from Hours is :'+Fringrules.Fringe_Allocation_Rules__r.Class_II_From_Hours__c);
            		System.debug('3 from Hours is :'+Fringrules.Fringe_Allocation_Rules__r.Class_III_From_Hours__c);
            		System.debug('1 To Hours is :'+Fringrules.Fringe_Allocation_Rules__r.Class_I_To_Hours__c);
            		System.debug('2 To Hours is :'+Fringrules.Fringe_Allocation_Rules__r.Class_II_To_Hours__c);
            		System.debug('3 To Hours is :'+Fringrules.Fringe_Allocation_Rules__r.Class_III_To_Hours__c);
            // Callender Map
            Map<Integer,String> monthNameMap = new Map<Integer, String>{1 =>'January', 2=>'February', 3=>'March', 4=>'April', 5=>'May',6=>'June', 7=>'July', 8=>'August', 9=>'September',10=>'October',11=>'November', 12=>'December'};
				String dateForMonth = monthNameMap.get(System.now().month())+' '+System.now().year();
				//Month_and_Year = Month_and_Year != null ? Month_and_Year : dateForMonth;
            	System.debug('Month and year After Callnder Map is :'+Month_and_Year);
            
            // Individual Fringe Benefits
            System.debug('empUniqId is :'+empUniqId);
            System.debug('dateformonth is :'+dateForMonth);
            Individual_Fringe_Benfit__c ifb;
            try {
             ifb = [SELECT Id,Class_Level_Of_Benefits2__c,Month_and_Year__c from Individual_Fringe_Benfit__c
                                              WHERE Month_and_Year__c  =: Month_and_Year AND Employee__r.Name =: empUniqId];
            }catch(Exception e){e.getMessage();}
            	System.debug('After Individual Fringe Benefits is :'+ifb);
            
            // Indidviual Fringe Benefits for Updating
            List<Individual_Fringe_Benfit__c> IfbLsit = new List<Individual_Fringe_Benfit__c>();
            
            if(ifb != Null ) {
                System.debug('If benefit is here then condition crossed');
             	if( MonthHours > Fringrules.Fringe_Allocation_Rules__r.Class_I_From_Hours__c   && MonthHours <= Fringrules.Fringe_Allocation_Rules__r.Class_I_To_Hours__c) {
                	ifb.Class_Level_Of_Benefits2__c = 'Class I Benefits';
                 	IfbLsit.add(ifb);
            	}else if(MonthHours >= Fringrules.Fringe_Allocation_Rules__r.Class_II_From_Hours__c && MonthHours <= Fringrules.Fringe_Allocation_Rules__r.Class_II_To_Hours__c) {
                	ifb.Class_Level_Of_Benefits2__c = 'Class II Benefits';
                	IfbLsit.add(ifb);
            	}else if(MonthHours > Fringrules.Fringe_Allocation_Rules__r.Class_III_From_Hours__c && MonthHours <= Fringrules.Fringe_Allocation_Rules__r.Class_III_To_Hours__c) {
                	ifb.Class_Level_Of_Benefits2__c = 'Class III Benefits';
                	IfbLsit.add(ifb);
            }
                Update IfbLsit;
            }else {
                System.debug('else id is not here then else crossed');
                Individual_Fringe_Benfit__c ifbinsert = new Individual_Fringe_Benfit__c();
                if( MonthHours > Fringrules.Fringe_Allocation_Rules__r.Class_I_From_Hours__c   && MonthHours <= Fringrules.Fringe_Allocation_Rules__r.Class_I_To_Hours__c) {
                		ifbinsert.Employee__c = employeeId;
                    	ifbinsert.Month_and_Year__c = Month_and_Year;//monthNameMap.get(System.now().month())+' '+System.now().year();
                        ifbinsert.Class_Level_of_Benefits2__c = 'Class I Benefits';
                    	IfbLsit.add(ifbinsert);
            	}else if(MonthHours > Fringrules.Fringe_Allocation_Rules__r.Class_II_From_Hours__c && MonthHours <= Fringrules.Fringe_Allocation_Rules__r.Class_II_To_Hours__c) {
                		ifbinsert.Employee__c = employeeId;
                    	ifbinsert.Month_and_Year__c = Month_and_Year;//monthNameMap.get(System.now().month())+' '+System.now().year();
                        ifbinsert.Class_Level_of_Benefits2__c = 'Class II Benefits';
                    	IfbLsit.add(ifbinsert);
            	}else if(MonthHours > Fringrules.Fringe_Allocation_Rules__r.Class_III_From_Hours__c && MonthHours <= Fringrules.Fringe_Allocation_Rules__r.Class_III_To_Hours__c) {
                		ifbinsert.Employee__c = employeeId;
                    	ifbinsert.Month_and_Year__c = Month_and_Year;//monthNameMap.get(System.now().month())+' '+System.now().year();
                        ifbinsert.Class_Level_of_Benefits2__c = 'Class III Benefits';
                    	IfbLsit.add(ifbinsert);
                }
                if(IfbLsit.size() > 0) {
                System.debug('IfbList is :'+IfbLsit);
                insert IfbLsit;
            }
            }
            
            
        }catch(Exception e){e.getMessage();}
        
    }

}