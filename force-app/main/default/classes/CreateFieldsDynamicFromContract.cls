//https://fringeplus--fringeqa.my.salesforce.com
public class CreateFieldsDynamicFromContract {
    @future (callout=true)
    public static void toolingApiToCreateField(List<Id> monthlyHoursList){
        Map<String, String> monthlyHoursMap = new Map<String, String>();
        List<Id> contractIds = new List<Id>();
        String fieldsList;
        String metadataFieldList;
        for(FringeBenefits__c ifbp : [SELECT Id, Name FROM FringeBenefits__c WHERE Id IN: monthlyHoursList]){
            String appendval ;
            //if(ifbp.Fringe_Contract__r.Fringe_Allocation_Method__c == 'Hourly Cost of Benefit Plans without Hour Bank Accounts'){
                
                /*if(appendval.contains('Underspent')){
                    appendval = '_U';
                }else if(appendval.contains('Overspent')){
                    appendval = '_O';
                }else{
                    appendval = '';
                } */
            //(ifbp.Individual_Fringe_Benefit_Account__c).deleteWhitespace()).replaceAll('[^a-zA-Z0-9\\s+]', '')
                appendval = '';
            	System.debug('-----'+ UserInfo.getSessionID());
            	System.debug('-----'+ UserInfo.getName());
                HttpRequest req1 = new HttpRequest();
                req1.setEndpoint('https://fringeplus--fringeqa.my.salesforce.com/services/data/v51.0/tooling/sobjects/CustomField');
                req1.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
                req1.setHeader('Content-Type', 'application/json');
                req1.setMethod('POST');
                req1.setBody('{"FullName":"Individual_Fringe_Benfit__c.'+(((ifbp.Name).deleteWhitespace()).replaceAll('[^a-zA-Z0-9\\s+]', ''))+'__c","Metadata":{"label":"'+ifbp.Name+'","description":"my new test field","required":false,"externalId":false,"type":"Text","length":32}}');
                if(String.isBlank(metadataFieldList)){
                    metadataFieldList = 'Individual_Fringe_Benfit__c.'+(((ifbp.Name).deleteWhitespace()).replaceAll('[^a-zA-Z0-9\\s+]', ''))+'__c';
                }else{
                    metadataFieldList += '~'+'Individual_Fringe_Benfit__c.'+(((ifbp.Name).deleteWhitespace()).replaceAll('[^a-zA-Z0-9\\s+]', ''))+'__c';
                }
                Http htp = new Http();
                HttpResponse resp= new HttpResponse();
                if(!Test.isRunningTest()){
                    resp = htp.send(req1);
                }else{
                    resp.setBody('{"namespace": null,"name": "MyTestClass","methodName": "testMethod2","id": "N0tARealTestId2","time": 47,"seeAllData": false}');                            
                    resp.setStatusCode(201); 
                    resp.setStatus('OK');
                }
                if(resp.getStatusCode() == 201 && resp.getStatus() == 'OK'){
                    
                }
                System.debug('====='+resp.getStatus());
                System.debug('====='+resp.getStatusCode());
                system.debug(resp.getBody());
             
            /*else if(ifbp.Fringe_Contract__r.Fringe_Allocation_Method__c == 'Individual Fringe Benefit  & Individual Premium Reserve Accounts'){
                appendval = ifbp.Credit_Debit_Rules__c;
                if(appendval.contains('Debit 1')){
                    appendval = '_D1';
                }else if(appendval.contains('Credit 1')){
                    appendval = '_C1';
                }else if(appendval.contains('Credit 2')){
                    appendval = '_C2';
                }else if(appendval.contains('Debit 2')){
                    appendval = '_D2';
                }else{
                    appendval = '';
                }
                appendval = '';
                HttpRequest req1 = new HttpRequest();
                req1.setEndpoint('https://fringeplus--fringedev.my.salesforce.com/services/data/v51.0/tooling/sobjects/CustomField');
                req1.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
                req1.setHeader('Content-Type', 'application/json');
                req1.setMethod('POST');
                req1.setBody('{"FullName":"Individual_Fringe_Benfit__c.'+(ifbp.Individual_Fringe_Benefit_Account__c).deleteWhitespace()+'__c","Metadata":{"label":"'+ifbp.Individual_Fringe_Benefit_Account__c+'","description":"my new test field","required":false,"externalId":false,"type":"Text","length":32}}');
                if(String.isBlank(metadataFieldList)){
                    metadataFieldList = 'Individual_Fringe_Benfit__c.'+(ifbp.Individual_Fringe_Benefit_Account__c).deleteWhitespace()+'__c';
                }else{
                    metadataFieldList += '~'+'Individual_Fringe_Benfit__c.'+(ifbp.Individual_Fringe_Benefit_Account__c).deleteWhitespace()+'__c';
                }
                Http htp = new Http();
                HttpResponse resp= new HttpResponse();
                if(!Test.isRunningTest()){
                    resp = htp.send(req1);
                }else{
                    resp.setBody('{"namespace": null,"name": "MyTestClass","methodName": "testMethod2","id": "N0tARealTestId2","time": 47,"seeAllData": false}');                            
                    resp.setStatusCode(201); 
                    resp.setStatus('OK');
                }
                if(resp.getStatusCode() == 201 && resp.getStatus() == 'OK'){
                    
                }
                System.debug('====='+resp.getStatus());
                System.debug('====='+resp.getStatusCode());
                system.debug(resp.getBody());
            }else if(ifbp.Fringe_Contract__r.Fringe_Allocation_Method__c == 'Hour Bank Accounting'){
                appendval = ifbp.Monthly_Fringe_Obligations__c;
                if(appendval.contains('Underspent')){
                    appendval = '_U';
                }else if(appendval.contains('Overspent')){
                    appendval = '_O';
                }else{
                    appendval = '';
                }
                //Hours_Bank_Account__c
                HttpRequest req1 = new HttpRequest();
                req1.setEndpoint('https://fringeplus--fringedev.my.salesforce.com/services/data/v51.0/tooling/sobjects/CustomField');
                req1.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
                req1.setHeader('Content-Type', 'application/json');
                req1.setMethod('POST');
                req1.setBody('{"FullName":"Hours_Bank_Account__c.'+(ifbp.Individual_Fringe_Benefit_Account__c).deleteWhitespace()+'__c","Metadata":{"label":"'+ifbp.Individual_Fringe_Benefit_Account__c+'","description":"my new test field","required":false,"externalId":false,"type":"Text","length":32}}');
                if(String.isBlank(metadataFieldList)){
                    metadataFieldList = 'Hours_Bank_Account__c.'+(ifbp.Individual_Fringe_Benefit_Account__c).deleteWhitespace()+'__c';
                }else{
                    metadataFieldList += '~'+'Hours_Bank_Account__c.'+(ifbp.Individual_Fringe_Benefit_Account__c).deleteWhitespace()+'__c';
                }
                Http htp = new Http();
                HttpResponse resp= new HttpResponse();
                if(!Test.isRunningTest()){
                    resp = htp.send(req1);
                }else{
                    resp.setBody('{"namespace": null,"name": "MyTestClass","methodName": "testMethod2","id": "N0tARealTestId2","time": 47,"seeAllData": false}');               
                    resp.setStatusCode(201); 
                    resp.setStatus('OK');
                }
                if(resp.getStatusCode() == 201 && resp.getStatus() == 'OK'){
                    
                }
                System.debug('====='+resp.getStatus());
                System.debug('====='+resp.getStatusCode());
                system.debug(resp.getBody());
            }*/
            //else{
                /*HttpRequest req1 = new HttpRequest();
                req1.setEndpoint('https://fringeplus--fringedev.my.salesforce.com/services/data/v51.0/tooling/sobjects/CustomField');
                req1.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
                req1.setHeader('Content-Type', 'application/json');
                req1.setMethod('POST');
                req1.setBody('{"FullName":"Individual_Fringe_Benfit__c.'+(ifbp.Name).deleteWhitespace()+'__c","Metadata":{"label":"'+ifbp.Name+'","description":"my new test field","required":false,"externalId":false,"type":"Text","length":32}}');
                if(String.isBlank(metadataFieldList)){
                    metadataFieldList = 'Individual_Fringe_Benfit__c.'+(ifbp.Name).deleteWhitespace()+'__c';
                }else{
                    metadataFieldList += '~'+'Individual_Fringe_Benfit__c.'+(ifbp.Name).deleteWhitespace()+'__c';
                }
                Http htp = new Http();
                HttpResponse resp = new HttpResponse();
                if(!Test.isRunningTest()){
                    resp = htp.send(req1);
                }else{
                    resp.setBody('{"successes": [{"namespace": "","name": "MyTestClass","methodName": "testMethod2","id": "N0tARealTestId2","time": 47,"seeAllData": false}]}');               
                    resp.setStatusCode(201); 
                    resp.setStatus('OK');
                }
                if(resp.getStatusCode() == 201 && resp.getStatus() == 'OK'){
                    
                }
                appendval = '';
                System.debug('====='+resp.getStatus());
                System.debug('====='+resp.getStatusCode());
                system.debug(resp.getBody());
            */
            HttpRequest req = new HttpRequest();
            req.setEndpoint('https://fringeplus--fringeqa.my.salesforce.com/services/data/v51.0/tooling/sobjects/CustomField');
            req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
            req.setHeader('Content-Type', 'application/json');
            req.setMethod('POST');
            req.setBody('{"FullName":"Fringe_Contract__c.'+(((ifbp.Name).deleteWhitespace()).replaceAll('[^a-zA-Z0-9\\s+]', ''))+appendval+'__c","Metadata":{"label":"'+ifbp.Name+appendval+'","description":"my new test field","required":false,"externalId":false,"type":"Text","length":32}}');
            if(String.isBlank(metadataFieldList)){
                metadataFieldList = 'Fringe_Contract__c.'+(((ifbp.Name).deleteWhitespace()).replaceAll('[^a-zA-Z0-9\\s+]', ''))+appendval+'__c';
            }else{
                metadataFieldList += '~'+'Fringe_Contract__c.'+(((ifbp.Name).deleteWhitespace()).replaceAll('[^a-zA-Z0-9\\s+]', ''))+appendval+'__c';
            }
            Http h = new Http();
            HttpResponse res= new HttpResponse();
                if(!Test.isRunningTest()){
                    res = h.send(req);
                }else{
                    res.setBody('{"namespace": null,"name": "MyTestClass","methodName": "testMethod2","id": "N0tARealTestId2","time": 47,"seeAllData": false}');               
                    res.setStatusCode(201); 
                    res.setStatus('OK');
                }
            if(res.getStatusCode() == 201 && res.getStatus() == 'OK'){
                System.debug('====='+res.getStatus());
                System.debug('====='+res.getStatusCode());
                
                system.debug(res.getBody());
            }else if(res.getStatusCode() == 400){
                
            }
            if(String.isBlank(fieldsList)){
                fieldsList = (ifbp.Name).deleteWhitespace()+appendval+'__c';
            }else{
                fieldsList += ','+(ifbp.Name).deleteWhitespace()+appendval+'__c';
            }
            //contractIds.add(ifbp.Fringe_Contract__c);
            //monthlyHoursMap.put((ifbp.Name).deleteWhitespace()+appendval+'__c', String.valueOf(ifbp.Percentage__c));
            
        }
        System.debug('----'+metadataFieldList);
        String results;
        if(!Test.isRunningTest()){
             results = updateMetadataToField(metadataFieldList);
        }else{
             results = 'success';
        }
       
    }
    
    public static void handleSaveResults(MetadataService.SaveResult saveResult){
        // Nothing to see?
        if(saveResult==null || saveResult.success)
            return;
        // Construct error message and throw an exception
        if(saveResult.errors!=null)
        {
            List<String> messages = new List<String>();
            messages.add(
                (saveResult.errors.size()==1 ? 'Error ' : 'Errors ') +
                'occured processing component ' + saveResult.fullName + '.');
            for(MetadataService.Error error : saveResult.errors)
                messages.add(
                    error.message + ' (' + error.statusCode + ').' +
                    ( error.fields!=null && error.fields.size()>0 ?
                     ' Fields ' + String.join(error.fields, ',') + '.' : '' ) );
            if(messages.size()>0)
                System.debug('--error---'+messages);
        }
        if(!saveResult.success)
            System.debug('--Request failed with no specified error.---');
    }
    
    public static String updateMetadataToField(String listfieldOj){
        for(String fieldobj: listfieldOj.split('~')){
            if(!String.isBlank(fieldobj)){
                MetadataService.MetadataPort service =  new MetadataService.MetadataPort();
                service.SessionHeader = new MetadataService.SessionHeader_element();
                service.SessionHeader.sessionId = UserInfo.getSessionId();
                MetadataService.Profile admin = new MetadataService.Profile();
                admin.fullName = 'Admin';
                admin.custom = false;
                MetadataService.ProfileFieldLevelSecurity fieldSec = new MetadataService.ProfileFieldLevelSecurity();
                fieldSec.field= fieldobj;
                fieldSec.editable=true;
                admin.fieldPermissions  = new MetadataService.ProfileFieldLevelSecurity[] {fieldSec} ;
                    List<MetadataService.SaveResult> results =
                    service.updateMetadata(
                        new MetadataService.Metadata[] { admin });
                CreateFieldsDynamicFromContract.handleSaveResults(results[0]);
            }
        }
        
        return 'success';
    }
    public static void createCustomSettingDynamicFields(List<Id> monthlyHoursList){
        Map<String, String> monthlyHoursMap = new Map<String, String>();
        List<Id> contractIds = new List<Id>();
        String fieldsList;
        String metadataFieldList;
        for(Individual_Fringe_Benefits_Percentages__c ifbp : [SELECT Id, Individual_Fringe_Benefit_Account__c, Percentage__c, Fringe_Contract__c,Fringe_Contract__r.Fringe_Allocation_Method__c FROM Individual_Fringe_Benefits_Percentages__c WHERE Id IN: monthlyHoursList]){
        	contractIds.add(ifbp.Fringe_Contract__c);
            monthlyHoursMap.put(((ifbp.Individual_Fringe_Benefit_Account__c).deleteWhitespace()).replaceAll('[^a-zA-Z0-9\\s+]', '')+'__c', String.valueOf(ifbp.Percentage__c));
            if(String.isBlank(fieldsList)){
                fieldsList = ((ifbp.Individual_Fringe_Benefit_Account__c).deleteWhitespace()).replaceAll('[^a-zA-Z0-9\\s+]', '')+'__c';
            }else{
                fieldsList += ','+((ifbp.Individual_Fringe_Benefit_Account__c).deleteWhitespace()).replaceAll('[^a-zA-Z0-9\\s+]', '')+'__c';
            }
        }
        String queryString = 'SELECT Id,'+fieldsList+' FROM Fringe_Contract__c WHERE Id IN: contractIds';
        Dynamic_Fields__c dfl;
        try{
            dfl = [SELECT Id, Name,List_of_Fields__c FROM Dynamic_Fields__c WHERE Name IN: contractIds LIMIT 1];
        }catch(Exception e){e.getMessage();}
        System.debug('-----'+queryString);
        if(dfl == null){
            List<sObject> cusSetRec = Database.query(queryString);
            Integer count = fieldsList.split(',').size();
            List<sObject> upsertListObj = new List<sObject>();
            List<Dynamic_Fields__c> dfList = new List<Dynamic_Fields__c>();
            for(sObject fc : cusSetRec){
                
                //upsertListObj.add
                Dynamic_Fields__c df = new Dynamic_Fields__c();
                df.Name = fc.Id;
                df.List_of_Fields__c = fieldsList;
                dfList.add(df);
                String checkFields;
                for(String fieldapi : fieldsList.split(',')){
                    if(!String.isBlank(fieldapi)){
                        fc.put(fieldapi,monthlyHoursMap.get(fieldapi));
                    }
                }
                upsertListObj.add(fc);
            }
            if(dfList.size() > 0){
                insert dfList;
            }
            upsert upsertListObj;
        }else{
            dfl.List_of_Fields__c = dfl.List_of_Fields__c+','+fieldsList;
            
            update dfl;
        }
    }
    
    
}