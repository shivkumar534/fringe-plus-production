@isTest
private class MethodDHandlerTest {
	@isTest
    static void myTest(){
        
        Govt_Contractor__c acc= new Govt_Contractor__c();
        acc.Name = 'FBA';
        //acc.Job_Classification__c = 'Diver 1 (Wet);Diver 2 (Standby);Drywall Installer/Lather;Drywall Stocker/Scrapper;Carpenter 1 (Bridge)';
        insert acc;
        
        Fringe_Allocation_Rules__c far = new Fringe_Allocation_Rules__c();
        far.Name = 'Method D1';
        far.FringeBenefits__c = 'HRA;Paid Leave Fringe Reserve Account';
        //far.OverspentCategory__c = 'Maintain Monthly Short List/Report';
        far.UnderspentCategory__c = 'Hourly cost of Benefits without Hour Bank Accounts';
        far.Class_I_From_Hours__c = 1;
        far.Class_I_To_Hours__c=30;
        far.Class_II_From_Hours__c=31;
        far.Class_II_To_Hours__c=60;
        far.Class_III_From_Hours__c=61;
        far.Class_III_To_Hours__c=90;
        far.Class_IV_From_Hours__c=91;
        far.Class_IV_To_Hours__c=120;
        far.Class_V_From_Hours__c=121;
        far.Class_V_To_Hours__c=150;
        insert far;
        
        Pricebook2 pb= new Pricebook2();
        pb.Name = 'FBA cost of Benefit';
        pb.Is_Job_Classification__c = false;
        pb.Is_Wage_Parity__c = false;
        pb.Govt_Contractors__c = acc.Id;
        pb.Monthly_Cost_Of_Benefit__c = false;
        pb.Hourly_Cost_of_Benefit__c = true;
        
        insert pb;
        
        List<Product2> pd = new List<Product2>();
        for(Integer i = 1; i <= 3;i++){
            Product2 p = new Product2();
            p.Name = 'Employee Only';
            p.Job_Classification__c = false;
            if(i == 1){
                p.ProductCode = 'BCBS PPO CHOICE';    
            }else if(i == 2){
                p.ProductCode = 'DELTA DENTAL PREMIER PLAN';   
            }else{
                p.ProductCode = '$10,000 PLAN';   
            }
            
            p.Family = 'Cost of Benefit';
            p.IsActive = true;
            p.TPA_Fee_on_Premium_Rat__c = 10;
            
            pd.add(p);
        }
        
        insert pd;
        
         List<Id> proIdList = new List<Id>();
         for(Product2 pd1 :[SELECT Id, Name, Job_Classification__c, ProductCode, Family, IsActive, ExternalDataSourceId FROM Product2]){
             proIdList.add(pd1.Id);
         }
        
        Fringe_Contract__c fc = new Fringe_Contract__c();
        fc.Contract_Name__c = 'Test1';
        fc.Govt_Contractor__c = acc.Id;
        fc.Fringe_Allocation_Rules__c = far.Id;
        fc.Price_Book__c = pb.Id;
        fc.Fringe_Rate__c = 5;
        fc.Hour_Bank__c = 120;
         fc.HRA__c = '50';
         fc.PaidLeave__c = '50';
        fc.Contract_Type__c = 'SCA - Service Contract Act';
        fc.Benefit_Plan__c = 'Health Insurance;Health Reimbursement Accounts (HRAs);Dental Insurance';
        //fc.Fringe_Rate_Based_on_Job_Classification__c = true;
        fc.SELECT_COST_OF_BENEFITS__c = 'Hourly Cost of Benefits';
        fc.Reporting_Type__c = 'Monthly Hours Report';
        
        insert fc;
         List<Individual_Fringe_Benefits_Percentages__c> ifbpList = new List<Individual_Fringe_Benefits_Percentages__c>();
         Dynamic_Fields__c df = new Dynamic_Fields__c();
         for(Integer i = 1; i<=2;i++){
             Individual_Fringe_Benefits_Percentages__c ifbp = new Individual_Fringe_Benefits_Percentages__c();
             df.Name = String.valueOf(fc.Id);
             ifbp.Percentage__c = 50;
             if(i == 1){
                 ifbp.Individual_Fringe_Benefit_Account__c = 'HRA';
                 df.List_of_Fields__c = 'HRA__c';
             }else{
                 ifbp.Individual_Fringe_Benefit_Account__c = 'PaidLeave';
                 df.List_of_Fields__c = 'HRA__c,PaidLeave__c';
             }
             ifbp.Fringe_Contract__c = fc.Id;
             ifbpList.add(ifbp);
         }
         insert df;
         insert ifbpList;
        
        pb.Fringe_Contract__c = fc.Id;
        update pb;
        
         Employee__c em = new Employee__c();
         em.Name = 'David';
         em.Fringe_Contract_Type__c = fc.Id;
         em.Unique_Identifier__c = 'David';
         em.Govt_Contractor__c = acc.Id;
         em.Emp_Status__c = 'Active';
        em.Fringe_Result_Category__c='Underspent';
        //em.Hour_Bank_cat__c = 'Underspent';
        em.Rolling_Hour_Bank_Account_Balance__c = 100;
        em.Hourly_Cost_Of_benefit2__c = 150;
         em.Dental_Insurance_Plan__c = 'DELTA DENTAL PREMIER PLAN';
         em.Health_Insurance_Plan__c = 'BCBS PPO CHOICE';
         em.Life_Insurance_Plan__c = '$10,000 PLAN';
         em.Coverage_Tier__c = 'Employee Only';
         Insert em;
         
         Monthly_Hours__c mh = new Monthly_Hours__c();
         mh.Name = 'David';
         mh.EE__c = 'David';
         //mh.zipcode = '';
         mh.EmpStatus__c = 'A';
         mh.Month_and_Year__c = 'March 2021';
         mh.Curr_Reg_Hrs_NYC__c = 10;
         mh.Govt_Contractors__c = acc.Id;
         mh.Fringe_Contract__c = fc.Id;
         mh.Employee__c = em.Id;
         mh.Coverage_Tier__c = 'Employee Only';
         mh.Cost_Of_Benefit__c = pb.Id;
         insert mh;
         List<Id> mhIdList = new List<Id>();
        mhIdList.add(mh.Id);
        system.assertEquals(mh != null,true);
        
		List<id> ids = new List<id>();
        ids.add(mh.id);
        
         MethodDHandler.methodD(mh.Id);
        
             
         mh.Curr_Reg_Hrs_NYC__c = 50;
         update mh;
         //MethodDHandler.methodD(mh.Id);
         RuleBasedOnClassLevelBenfits.benefits(mh.id);
        
         mh.Curr_Reg_Hrs_NYC__c = 80;
         update mh;
        //MethodDHandler.methodD(mh.Id);
        RuleBasedOnClassLevelBenfits.benefits(mh.id);
         mh.Curr_Reg_Hrs_NYC__c = 110;
         update mh;
        //MethodDHandler.methodD(mh.Id);
        RuleBasedOnClassLevelBenfits.benefits(mh.id);
         mh.Curr_Reg_Hrs_NYC__c = 140;
        update mh;
        //MethodDHandler.methodD(mh.Id);
        RuleBasedOnClassLevelBenfits.benefits(mh.id);
        
        Monthly_Hours__c mh1 = new Monthly_Hours__c();
         mh1.Name = 'David';
         mh1.EE__c = 'David';
         //mh.zipcode = '';
         mh1.EmpStatus__c = 'A';
         mh1.Month_and_Year__c = 'April 2021';
         mh1.Curr_Reg_Hrs_NYC__c = 40;
         mh1.Govt_Contractors__c = acc.Id;
         mh1.Fringe_Contract__c = fc.Id;
         mh1.Employee__c = em.Id;
         mh1.Coverage_Tier__c = 'Employee Only';
         mh1.Cost_Of_Benefit__c = pb.Id;
         insert mh1;
        MethodDHandler.methodD(mh1.Id);
        
       }
    
    @isTest
    static void myTest1(){
        
        Govt_Contractor__c acc= new Govt_Contractor__c();
        acc.Name = 'FBA';
        //acc.Job_Classification__c = 'Diver 1 (Wet);Diver 2 (Standby);Drywall Installer/Lather;Drywall Stocker/Scrapper;Carpenter 1 (Bridge)';
        insert acc;
        
        Fringe_Allocation_Rules__c far = new Fringe_Allocation_Rules__c();
        far.Name = 'Method D1';
        far.FringeBenefits__c = 'HRA;Paid Leave Fringe Reserve Account';
        //far.OverspentCategory__c = 'Maintain Monthly Short List/Report';
        far.UnderspentCategory__c = 'Hourly cost of Benefits without Hour Bank Accounts';
        far.Class_I_From_Hours__c = 1;
        far.Class_I_To_Hours__c=30;
        far.Class_II_From_Hours__c=31;
        far.Class_II_To_Hours__c=60;
        far.Class_III_From_Hours__c=61;
        far.Class_III_To_Hours__c=90;
        far.Class_IV_From_Hours__c=91;
        far.Class_IV_To_Hours__c=120;
        far.Class_V_From_Hours__c=121;
        far.Class_V_To_Hours__c=150;
        insert far;
        
        Pricebook2 pb= new Pricebook2();
        pb.Name = 'FBA cost of Benefit';
        pb.Is_Job_Classification__c = false;
        pb.Is_Wage_Parity__c = false;
        pb.Govt_Contractors__c = acc.Id;
        pb.Monthly_Cost_Of_Benefit__c = false;
        pb.Hourly_Cost_of_Benefit__c = true;
        
        insert pb;
        
        List<Product2> pd = new List<Product2>();
        for(Integer i = 1; i <= 3;i++){
            Product2 p = new Product2();
            p.Name = 'Employee Only';
            p.Job_Classification__c = false;
            if(i == 1){
                p.ProductCode = 'BCBS PPO CHOICE';    
            }else if(i == 2){
                p.ProductCode = 'DELTA DENTAL PREMIER PLAN';   
            }else{
                p.ProductCode = '$10,000 PLAN';   
            }
            
            p.Family = 'Cost of Benefit';
            p.IsActive = true;
            p.TPA_Fee_on_Premium_Rat__c = 10;
            
            pd.add(p);
        }
        
        insert pd;
        
         List<Id> proIdList = new List<Id>();
         for(Product2 pd1 :[SELECT Id, Name, Job_Classification__c, ProductCode, Family, IsActive, ExternalDataSourceId FROM Product2]){
             proIdList.add(pd1.Id);
         }
        List<Id> fcIds =new List<Id>();
        Fringe_Contract__c fc = new Fringe_Contract__c();
        fc.Contract_Name__c = 'Test1';
        fc.Govt_Contractor__c = acc.Id;
        fc.Fringe_Allocation_Rules__c = far.Id;
        fc.Price_Book__c = pb.Id;
        fc.Fringe_Rate__c = 5;
        fc.Hour_Bank__c = 120;
         fc.HRA__c = '50';
         fc.PaidLeave__c = '50';
        fc.Contract_Type__c = 'SCA - Service Contract Act';
        fc.Benefit_Plan__c = 'Health Insurance;Health Reimbursement Accounts (HRAs);Dental Insurance';
        //fc.Fringe_Rate_Based_on_Job_Classification__c = true;
        fc.SELECT_COST_OF_BENEFITS__c = 'Hourly Cost of Benefits';
        fc.Reporting_Type__c = 'Monthly Hours Report';
        
        insert fc;
        fcIds.add(fc.Id);
         List<Individual_Fringe_Benefits_Percentages__c> ifbpList = new List<Individual_Fringe_Benefits_Percentages__c>();
         Dynamic_Fields__c df = new Dynamic_Fields__c();
         for(Integer i = 1; i<=2;i++){
             Individual_Fringe_Benefits_Percentages__c ifbp = new Individual_Fringe_Benefits_Percentages__c();
             df.Name = String.valueOf(fc.Id);
             ifbp.Percentage__c = 50;
             if(i == 1){
                 ifbp.Individual_Fringe_Benefit_Account__c = 'HRA';
                 df.List_of_Fields__c = 'HRA__c';
             }else{
                 ifbp.Individual_Fringe_Benefit_Account__c = 'PaidLeave';
                 df.List_of_Fields__c = 'HRA__c,PaidLeave__c';
             }
             ifbp.Fringe_Contract__c = fc.Id;
             ifbpList.add(ifbp);
         }
         insert df;
         insert ifbpList;
        
        pb.Fringe_Contract__c = fc.Id;
        update pb;
        
         Employee__c emp1 = new Employee__c();
         emp1.Name = 'testEmp1';
         emp1.Fringe_Contract_Type__c = fc.Id;
         emp1.Unique_Identifier__c = 'testEmp1';
         emp1.Govt_Contractor__c = acc.Id;
         emp1.Emp_Status__c = 'Active';
        emp1.Fringe_Result_Category__c='Underspent';
        //em.Hour_Bank_cat__c = 'Underspent';
        emp1.Rolling_Hour_Bank_Account_Balance__c = 100;
        //em1.Hourly_Cost_Of_benefit2__c = 150;
         emp1.Dental_Insurance_Plan__c = 'DELTA DENTAL PREMIER PLAN';
         emp1.Health_Insurance_Plan__c = 'BCBS PPO CHOICE';
         emp1.Life_Insurance_Plan__c = '$10,000 PLAN';
         emp1.Coverage_Tier__c = 'Employee Only';
         Insert emp1;
        
        Monthly_Hours__c mh1 = new Monthly_Hours__c();
        mh1.Employee__c = emp1.id;
        mh1.EE__c = 'testEmp1';
        mh1.Curr_Reg_Hrs_NYC__c = 20;
        mh1.Month_and_Year__c = 'August 2021';
        mh1.Govt_Contractors__c = acc.Id;
        mh1.Fringe_Contract__c = fc.Id;
        mh1.Coverage_Tier__c = 'Employee Only';
        mh1.Cost_Of_Benefit__c = pb.Id;
        insert mh1;
        MethodDHandler.methodD(mh1.Id);
		
        Monthly_Hours__c mh5 = new Monthly_Hours__c();
        mh5.Employee__c = emp1.id;
        mh5.EE__c = 'testEmp1';
        mh5.Curr_Reg_Hrs_NYC__c = 140;
        mh5.Month_and_Year__c = 'december 2021';
        mh5.Govt_Contractors__c = acc.Id;
        mh5.Fringe_Contract__c = fc.Id;
        mh5.Coverage_Tier__c = 'Employee Only';
        mh5.Cost_Of_Benefit__c = pb.Id;
        insert mh5;
        MethodDHandler.methodD(mh5.Id);
       
              
        List<PricebookEntry> pdlist = new List<PricebookEntry>();
        Integer count =1;
        for(PricebookEntry pe :[SELECT UnitPrice, Id, Name, Pricebook2Id, Product2Id, IsActive, ProductCode FROM PricebookEntry WHERE Product2Id IN: proIdList]){
            if(count <= 3){
                pe.UnitPrice = 100;
                pdlist.add(pe);
                count++;
            }
        }
        update pdlist;
        
        Individual_Fringe_Benfit__c objBenefits=new Individual_Fringe_Benfit__c (HRA__C=34,Employee__c=emp1.Id,Curr_Reg_Hours_NYC__c=110,Class_Level_Of_Benefits2__c='Class I Benefits');
        insert objBenefits;
        RetroactiveAdjustments.fringeRate(fcIds);
        Test.startTest();
        //MethodDHandler.methodD(mh1.Id);
        //MethodDHandler.methodD(mh2.Id);
        //MethodDHandler.methodD(mh3.Id);
        RuleBasedOnClassLevelBenfits.benefits(mh1.id);
        RuleBasedOnClassLevelBenfits.benefits(mh5.id);
        Test.stopTest();
         
     }  
	
		
		 @isTest
    static void myTest2(){
        
        Govt_Contractor__c acc= new Govt_Contractor__c();
        acc.Name = 'FBA';
        //acc.Job_Classification__c = 'Diver 1 (Wet);Diver 2 (Standby);Drywall Installer/Lather;Drywall Stocker/Scrapper;Carpenter 1 (Bridge)';
        insert acc;
        
        Fringe_Allocation_Rules__c far = new Fringe_Allocation_Rules__c();
        far.Name = 'Method D1';
        far.FringeBenefits__c = 'HRA;Paid Leave Fringe Reserve Account';
        //far.OverspentCategory__c = 'Maintain Monthly Short List/Report';
        far.UnderspentCategory__c = 'Hourly cost of Benefits without Hour Bank Accounts';
        far.Class_I_From_Hours__c = 1;
        far.Class_I_To_Hours__c=30;
        far.Class_II_From_Hours__c=31;
        far.Class_II_To_Hours__c=60;
        far.Class_III_From_Hours__c=61;
        far.Class_III_To_Hours__c=90;
        far.Class_IV_From_Hours__c=91;
        far.Class_IV_To_Hours__c=120;
        far.Class_V_From_Hours__c=121;
        far.Class_V_To_Hours__c=150;
        insert far;
        
        Pricebook2 pb= new Pricebook2();
        pb.Name = 'FBA cost of Benefit';
        pb.Is_Job_Classification__c = false;
        pb.Is_Wage_Parity__c = false;
        pb.Govt_Contractors__c = acc.Id;
        pb.Monthly_Cost_Of_Benefit__c = false;
        pb.Hourly_Cost_of_Benefit__c = true;
        
        insert pb;
        
        List<Product2> pd = new List<Product2>();
        for(Integer i = 1; i <= 3;i++){
            Product2 p = new Product2();
            p.Name = 'Employee Only';
            p.Job_Classification__c = false;
            if(i == 1){
                p.ProductCode = 'BCBS PPO CHOICE';    
            }else if(i == 2){
                p.ProductCode = 'DELTA DENTAL PREMIER PLAN';   
            }else{
                p.ProductCode = '$10,000 PLAN';   
            }
            
            p.Family = 'Cost of Benefit';
            p.IsActive = true;
            p.TPA_Fee_on_Premium_Rat__c = 10;
            
            pd.add(p);
        }
        
        insert pd;
        
         List<Id> proIdList = new List<Id>();
         for(Product2 pd1 :[SELECT Id, Name, Job_Classification__c, ProductCode, Family, IsActive, ExternalDataSourceId FROM Product2]){
             proIdList.add(pd1.Id);
         }
        List<Id> fcIds =new List<Id>();
        Fringe_Contract__c fc = new Fringe_Contract__c();
        fc.Contract_Name__c = 'Test1';
        fc.Govt_Contractor__c = acc.Id;
        fc.Fringe_Allocation_Rules__c = far.Id;
        fc.Price_Book__c = pb.Id;
        fc.Fringe_Rate__c = 5;
        fc.Hour_Bank__c = 120;
         fc.HRA__c = '50';
         fc.PaidLeave__c = '50';
        fc.Contract_Type__c = 'SCA - Service Contract Act';
        fc.Benefit_Plan__c = 'Health Insurance;Health Reimbursement Accounts (HRAs);Dental Insurance';
        //fc.Fringe_Rate_Based_on_Job_Classification__c = true;
        fc.SELECT_COST_OF_BENEFITS__c = 'Hourly Cost of Benefits';
        fc.Reporting_Type__c = 'Monthly Hours Report';
        
        insert fc;
        fcIds.add(fc.Id);
         List<Individual_Fringe_Benefits_Percentages__c> ifbpList = new List<Individual_Fringe_Benefits_Percentages__c>();
         Dynamic_Fields__c df = new Dynamic_Fields__c();
         for(Integer i = 1; i<=2;i++){
             Individual_Fringe_Benefits_Percentages__c ifbp = new Individual_Fringe_Benefits_Percentages__c();
             df.Name = String.valueOf(fc.Id);
             ifbp.Percentage__c = 50;
             if(i == 1){
                 ifbp.Individual_Fringe_Benefit_Account__c = 'HRA';
                 df.List_of_Fields__c = 'HRA__c';
             }else{
                 ifbp.Individual_Fringe_Benefit_Account__c = 'PaidLeave';
                 df.List_of_Fields__c = 'HRA__c,PaidLeave__c';
             }
             ifbp.Fringe_Contract__c = fc.Id;
             ifbpList.add(ifbp);
         }
         insert df;
         insert ifbpList;
        
        pb.Fringe_Contract__c = fc.Id;
        update pb;
        
         Employee__c emp1 = new Employee__c();
         emp1.Name = 'testEmp1';
         emp1.Fringe_Contract_Type__c = fc.Id;
         emp1.Unique_Identifier__c = 'testEmp1';
         emp1.Govt_Contractor__c = acc.Id;
         emp1.Emp_Status__c = 'Active';
        emp1.Fringe_Result_Category__c='Underspent';
        //em.Hour_Bank_cat__c = 'Underspent';
        emp1.Rolling_Hour_Bank_Account_Balance__c = 100;
        //em1.Hourly_Cost_Of_benefit2__c = 150;
         emp1.Dental_Insurance_Plan__c = 'DELTA DENTAL PREMIER PLAN';
         emp1.Health_Insurance_Plan__c = 'BCBS PPO CHOICE';
         emp1.Life_Insurance_Plan__c = '$10,000 PLAN';
         emp1.Coverage_Tier__c = 'Employee Only';
         Insert emp1;
		 
		 
        Monthly_Hours__c mh2 = new Monthly_Hours__c();
        mh2.Employee__c = emp1.id;
        mh2.EE__c = 'testEmp1';
        mh2.Curr_Reg_Hrs_NYC__c = 50;
        mh2.Month_and_Year__c = 'september 2021';
        mh2.Govt_Contractors__c = acc.Id;
        mh2.Fringe_Contract__c = fc.Id;
        mh2.Coverage_Tier__c = 'Employee Only';
        mh2.Cost_Of_Benefit__c = pb.Id;
        insert mh2;
		MethodDHandler.methodD(mh2.Id);
		List<PricebookEntry> pdlist = new List<PricebookEntry>();
        Integer count =1;
        for(PricebookEntry pe :[SELECT UnitPrice, Id, Name, Pricebook2Id, Product2Id, IsActive, ProductCode FROM PricebookEntry WHERE Product2Id IN: proIdList]){
            if(count <= 3){
                pe.UnitPrice = 100;
                pdlist.add(pe);
                count++;
            }
        }
        update pdlist;
        
        Individual_Fringe_Benfit__c objBenefits=new Individual_Fringe_Benfit__c (HRA__C=34,Employee__c=emp1.Id,Curr_Reg_Hours_NYC__c=110,Class_Level_Of_Benefits2__c='Class I Benefits');
        insert objBenefits;
        RetroactiveAdjustments.fringeRate(fcIds);
        Test.startTest();
        //MethodDHandler.methodD(mh1.Id);
        //MethodDHandler.methodD(mh2.Id);
        //MethodDHandler.methodD(mh3.Id);
        RuleBasedOnClassLevelBenfits.benefits(mh2.id);
        Test.stopTest();
		 
		 }
		 
		  @isTest
    static void myTest3(){
        
        Govt_Contractor__c acc= new Govt_Contractor__c();
        acc.Name = 'FBA';
        //acc.Job_Classification__c = 'Diver 1 (Wet);Diver 2 (Standby);Drywall Installer/Lather;Drywall Stocker/Scrapper;Carpenter 1 (Bridge)';
        insert acc;
        
        Fringe_Allocation_Rules__c far = new Fringe_Allocation_Rules__c();
        far.Name = 'Method D1';
        far.FringeBenefits__c = 'HRA;Paid Leave Fringe Reserve Account';
        //far.OverspentCategory__c = 'Maintain Monthly Short List/Report';
        far.UnderspentCategory__c = 'Hourly cost of Benefits without Hour Bank Accounts';
        far.Class_I_From_Hours__c = 1;
        far.Class_I_To_Hours__c=30;
        far.Class_II_From_Hours__c=31;
        far.Class_II_To_Hours__c=60;
        far.Class_III_From_Hours__c=61;
        far.Class_III_To_Hours__c=90;
        far.Class_IV_From_Hours__c=91;
        far.Class_IV_To_Hours__c=120;
        far.Class_V_From_Hours__c=121;
        far.Class_V_To_Hours__c=150;
        insert far;
        
        Pricebook2 pb= new Pricebook2();
        pb.Name = 'FBA cost of Benefit';
        pb.Is_Job_Classification__c = false;
        pb.Is_Wage_Parity__c = false;
        pb.Govt_Contractors__c = acc.Id;
        pb.Monthly_Cost_Of_Benefit__c = false;
        pb.Hourly_Cost_of_Benefit__c = true;
        
        insert pb;
        
        List<Product2> pd = new List<Product2>();
        for(Integer i = 1; i <= 3;i++){
            Product2 p = new Product2();
            p.Name = 'Employee Only';
            p.Job_Classification__c = false;
            if(i == 1){
                p.ProductCode = 'BCBS PPO CHOICE';    
            }else if(i == 2){
                p.ProductCode = 'DELTA DENTAL PREMIER PLAN';   
            }else{
                p.ProductCode = '$10,000 PLAN';   
            }
            
            p.Family = 'Cost of Benefit';
            p.IsActive = true;
            p.TPA_Fee_on_Premium_Rat__c = 10;
            
            pd.add(p);
        }
        
        insert pd;
        
         List<Id> proIdList = new List<Id>();
         for(Product2 pd1 :[SELECT Id, Name, Job_Classification__c, ProductCode, Family, IsActive, ExternalDataSourceId FROM Product2]){
             proIdList.add(pd1.Id);
         }
        List<Id> fcIds =new List<Id>();
        Fringe_Contract__c fc = new Fringe_Contract__c();
        fc.Contract_Name__c = 'Test1';
        fc.Govt_Contractor__c = acc.Id;
        fc.Fringe_Allocation_Rules__c = far.Id;
        fc.Price_Book__c = pb.Id;
        fc.Fringe_Rate__c = 5;
        fc.Hour_Bank__c = 120;
         fc.HRA__c = '50';
         fc.PaidLeave__c = '50';
        fc.Contract_Type__c = 'SCA - Service Contract Act';
        fc.Benefit_Plan__c = 'Health Insurance;Health Reimbursement Accounts (HRAs);Dental Insurance';
        //fc.Fringe_Rate_Based_on_Job_Classification__c = true;
        fc.SELECT_COST_OF_BENEFITS__c = 'Hourly Cost of Benefits';
        fc.Reporting_Type__c = 'Monthly Hours Report';
        
        insert fc;
        fcIds.add(fc.Id);
         List<Individual_Fringe_Benefits_Percentages__c> ifbpList = new List<Individual_Fringe_Benefits_Percentages__c>();
         Dynamic_Fields__c df = new Dynamic_Fields__c();
         for(Integer i = 1; i<=2;i++){
             Individual_Fringe_Benefits_Percentages__c ifbp = new Individual_Fringe_Benefits_Percentages__c();
             df.Name = String.valueOf(fc.Id);
             ifbp.Percentage__c = 50;
             if(i == 1){
                 ifbp.Individual_Fringe_Benefit_Account__c = 'HRA';
                 df.List_of_Fields__c = 'HRA__c';
             }else{
                 ifbp.Individual_Fringe_Benefit_Account__c = 'PaidLeave';
                 df.List_of_Fields__c = 'HRA__c,PaidLeave__c';
             }
             ifbp.Fringe_Contract__c = fc.Id;
             ifbpList.add(ifbp);
         }
         insert df;
         insert ifbpList;
        
        pb.Fringe_Contract__c = fc.Id;
        update pb;
        
         Employee__c emp1 = new Employee__c();
         emp1.Name = 'testEmp1';
         emp1.Fringe_Contract_Type__c = fc.Id;
         emp1.Unique_Identifier__c = 'testEmp1';
         emp1.Govt_Contractor__c = acc.Id;
         emp1.Emp_Status__c = 'Active';
        emp1.Fringe_Result_Category__c='Underspent';
        //em.Hour_Bank_cat__c = 'Underspent';
        emp1.Rolling_Hour_Bank_Account_Balance__c = 100;
        //em1.Hourly_Cost_Of_benefit2__c = 150;
         emp1.Dental_Insurance_Plan__c = 'DELTA DENTAL PREMIER PLAN';
         emp1.Health_Insurance_Plan__c = 'BCBS PPO CHOICE';
         emp1.Life_Insurance_Plan__c = '$10,000 PLAN';
         emp1.Coverage_Tier__c = 'Employee Only';
         Insert emp1;
		 
		 
		 
		 
        Monthly_Hours__c mh3 = new Monthly_Hours__c();
        mh3.Employee__c = emp1.id;
        mh3.EE__c = 'testEmp1';
        mh3.Curr_Reg_Hrs_NYC__c = 80;
        mh3.Month_and_Year__c = 'october 2021';
        mh3.Govt_Contractors__c = acc.Id;
        mh3.Fringe_Contract__c = fc.Id;
        mh3.Coverage_Tier__c = 'Employee Only';
        mh3.Cost_Of_Benefit__c = pb.Id;
        insert mh3;
		MethodDHandler.methodD(mh3.Id);
		List<PricebookEntry> pdlist = new List<PricebookEntry>();
        Integer count =1;
        for(PricebookEntry pe :[SELECT UnitPrice, Id, Name, Pricebook2Id, Product2Id, IsActive, ProductCode FROM PricebookEntry WHERE Product2Id IN: proIdList]){
            if(count <= 3){
                pe.UnitPrice = 100;
                pdlist.add(pe);
                count++;
            }
        }
        update pdlist;
        
        Individual_Fringe_Benfit__c objBenefits=new Individual_Fringe_Benfit__c (HRA__C=34,Employee__c=emp1.Id,Curr_Reg_Hours_NYC__c=110,Class_Level_Of_Benefits2__c='Class I Benefits');
        insert objBenefits;
        RetroactiveAdjustments.fringeRate(fcIds);
        Test.startTest();
        //MethodDHandler.methodD(mh1.Id);
        //MethodDHandler.methodD(mh2.Id);
        //MethodDHandler.methodD(mh3.Id);
        RuleBasedOnClassLevelBenfits.benefits(mh3.id);
        Test.stopTest();
		 
		 }
	 @isTest
    static void myTest4(){
        
        Govt_Contractor__c acc= new Govt_Contractor__c();
        acc.Name = 'FBA';
        //acc.Job_Classification__c = 'Diver 1 (Wet);Diver 2 (Standby);Drywall Installer/Lather;Drywall Stocker/Scrapper;Carpenter 1 (Bridge)';
        insert acc;
        
        Fringe_Allocation_Rules__c far = new Fringe_Allocation_Rules__c();
        far.Name = 'Method D1';
        far.FringeBenefits__c = 'HRA;Paid Leave Fringe Reserve Account';
        //far.OverspentCategory__c = 'Maintain Monthly Short List/Report';
        far.UnderspentCategory__c = 'Hourly cost of Benefits without Hour Bank Accounts';
        far.Class_I_From_Hours__c = 1;
        far.Class_I_To_Hours__c=30;
        far.Class_II_From_Hours__c=31;
        far.Class_II_To_Hours__c=60;
        far.Class_III_From_Hours__c=61;
        far.Class_III_To_Hours__c=90;
        far.Class_IV_From_Hours__c=91;
        far.Class_IV_To_Hours__c=120;
        far.Class_V_From_Hours__c=121;
        far.Class_V_To_Hours__c=150;
        insert far;
        
        Pricebook2 pb= new Pricebook2();
        pb.Name = 'FBA cost of Benefit';
        pb.Is_Job_Classification__c = false;
        pb.Is_Wage_Parity__c = false;
        pb.Govt_Contractors__c = acc.Id;
        pb.Monthly_Cost_Of_Benefit__c = false;
        pb.Hourly_Cost_of_Benefit__c = true;
        
        insert pb;
        
        List<Product2> pd = new List<Product2>();
        for(Integer i = 1; i <= 3;i++){
            Product2 p = new Product2();
            p.Name = 'Employee Only';
            p.Job_Classification__c = false;
            if(i == 1){
                p.ProductCode = 'BCBS PPO CHOICE';    
            }else if(i == 2){
                p.ProductCode = 'DELTA DENTAL PREMIER PLAN';   
            }else{
                p.ProductCode = '$10,000 PLAN';   
            }
            
            p.Family = 'Cost of Benefit';
            p.IsActive = true;
            p.TPA_Fee_on_Premium_Rat__c = 10;
            
            pd.add(p);
        }
        
        insert pd;
        
         List<Id> proIdList = new List<Id>();
         for(Product2 pd1 :[SELECT Id, Name, Job_Classification__c, ProductCode, Family, IsActive, ExternalDataSourceId FROM Product2]){
             proIdList.add(pd1.Id);
         }
        List<Id> fcIds =new List<Id>();
        Fringe_Contract__c fc = new Fringe_Contract__c();
        fc.Contract_Name__c = 'Test1';
        fc.Govt_Contractor__c = acc.Id;
        fc.Fringe_Allocation_Rules__c = far.Id;
        fc.Price_Book__c = pb.Id;
        fc.Fringe_Rate__c = 5;
        fc.Hour_Bank__c = 120;
         fc.HRA__c = '50';
         fc.PaidLeave__c = '50';
        fc.Contract_Type__c = 'SCA - Service Contract Act';
        fc.Benefit_Plan__c = 'Health Insurance;Health Reimbursement Accounts (HRAs);Dental Insurance';
        //fc.Fringe_Rate_Based_on_Job_Classification__c = true;
        fc.SELECT_COST_OF_BENEFITS__c = 'Hourly Cost of Benefits';
        fc.Reporting_Type__c = 'Monthly Hours Report';
        
        insert fc;
         Benefit_Plan__c bp = new Benefit_Plan__c();
        bp.Fringe_Contract__c = fc.Id;
        bp.Coverage_Type__c = 'Employee Only';
        bp.Name = 'Ben1';
        bp.Contribution_to_HRA_Benefit__c = 20;
        bp.Contribution_to_IPRA__c = 20;
        insert bp;
        fcIds.add(fc.Id);
         List<Individual_Fringe_Benefits_Percentages__c> ifbpList = new List<Individual_Fringe_Benefits_Percentages__c>();
         Dynamic_Fields__c df = new Dynamic_Fields__c();
         for(Integer i = 1; i<=2;i++){
             Individual_Fringe_Benefits_Percentages__c ifbp = new Individual_Fringe_Benefits_Percentages__c();
             df.Name = String.valueOf(fc.Id);
             ifbp.Percentage__c = 50;
             if(i == 1){
                 ifbp.Individual_Fringe_Benefit_Account__c = 'HRA';
                 df.List_of_Fields__c = 'HRA__c';
             }else{
                 ifbp.Individual_Fringe_Benefit_Account__c = 'PaidLeave';
                 df.List_of_Fields__c = 'HRA__c,PaidLeave__c';
             }
             ifbp.Fringe_Contract__c = fc.Id;
             ifbpList.add(ifbp);
         }
         insert df;
         insert ifbpList;
        
        pb.Fringe_Contract__c = fc.Id;
        update pb;
        
         Employee__c emp1 = new Employee__c();
         emp1.Name = 'testEmp1';
         emp1.Fringe_Contract_Type__c = fc.Id;
         emp1.Unique_Identifier__c = 'testEmp1';
         emp1.Govt_Contractor__c = acc.Id;
         emp1.Emp_Status__c = 'Active';
        emp1.Fringe_Result_Category__c='Underspent';
        //em.Hour_Bank_cat__c = 'Underspent';
        emp1.Rolling_Hour_Bank_Account_Balance__c = 100;
        //em1.Hourly_Cost_Of_benefit2__c = 150;
         emp1.Dental_Insurance_Plan__c = 'DELTA DENTAL PREMIER PLAN';
         emp1.Health_Insurance_Plan__c = 'BCBS PPO CHOICE';
         emp1.Life_Insurance_Plan__c = '$10,000 PLAN';
         emp1.Coverage_Tier__c = 'Employee Only';
         Insert emp1;
		 
		 
        Monthly_Hours__c mh4 = new Monthly_Hours__c();
        mh4.Employee__c = emp1.id;
        mh4.EE__c = 'testEmp1';
        mh4.Curr_Reg_Hrs_NYC__c = 120;
        mh4.Month_and_Year__c = 'november 2021';
        mh4.Govt_Contractors__c = acc.Id;
        mh4.Fringe_Contract__c = fc.Id;
        mh4.Coverage_Tier__c = 'Employee Only';
        mh4.Cost_Of_Benefit__c = pb.Id;
        insert mh4;
		MethodDHandler.methodD(mh4.Id);
		List<PricebookEntry> pdlist = new List<PricebookEntry>();
        Integer count =1;
        for(PricebookEntry pe :[SELECT UnitPrice, Id, Name, Pricebook2Id, Product2Id, IsActive, ProductCode FROM PricebookEntry WHERE Product2Id IN: proIdList]){
            if(count <= 3){
                pe.UnitPrice = 100;
                pdlist.add(pe);
                count++;
            }
        }
        update pdlist;
        
        Individual_Fringe_Benfit__c objBenefits=new Individual_Fringe_Benfit__c (HRA__C=34,Employee__c=emp1.Id,Curr_Reg_Hours_NYC__c=110,Class_Level_Of_Benefits2__c='Class I Benefits');
        insert objBenefits;
        RetroactiveAdjustments.fringeRate(fcIds);
        Test.startTest();
        //MethodDHandler.methodD(mh1.Id);
        //MethodDHandler.methodD(mh2.Id);
        //MethodDHandler.methodD(mh3.Id);
        RuleBasedOnClassLevelBenfits.benefits(mh4.id);
        Test.stopTest();
		 } 
}