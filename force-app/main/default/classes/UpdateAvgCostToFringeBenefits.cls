public class UpdateAvgCostToFringeBenefits{
    public static void updateAvgCostBenefits( Id empIds) {
        string Month_and_Year,listOfFields,employeeId,contractId;
        Map<Integer,String> monthNameMap = new Map<Integer, String>{1 =>'January', 2=>'February', 3=>'March', 4=>'April', 5=>'May',6=>'June', 7=>'July', 8=>'August', 9=>'September',10=>'October',11=>'November', 12=>'December'};
            Employee__c emp;
        try{            
            emp = [SELECT Id, Name,Status__c,Avg_cost_Qtr__c,Fringe_Contract_Type__r.Id,Monthly_Fringe_Contribution__c,After_Retro_Cost_Of_Benefit_Adjustments__c,Monthly_fringe_obligtion_result__c,Actual_Monthly_Hours__c,HRA_Card_Monthly_Deposit__c,Fringe_Rate_lookup__c,Cash_Equivalent_Payment__c,Average_Cost_Accounting__c,Curr_Reg_Hrs_NYC__c,Monthly_Cost_Of_Benefits__c, Fringe_Contract_Type__r.Hour_Bank__c, Fringe_Contract_Type__r.Fringe_Allocation_Method__c, Roll_Back_Hours__c,Individual_Premium_Reserve_Account_Month__c,Rolling_Balance_of_IPRA__c,Month_and_Year__c,Fringe_Contract_Type__r.Credit_Rule__c,Fringe_Contract_Type__r.Debit_Rule__c,Amount_Allocated_to_Paid_Leave_Account__c, Monthly_Fringe_Amount_Allocated__c,Benefit_Amount_Allocated__c,Benefit_Plan__r.Total_Benefits__c,Benefit_Plan__c,Benefit_Plan__r.Coverage_Type__c,Benefit_Plan__r.Contribution_to_HRA_Benefit__c,Benefit_Plan__r.Contribution_to_RetBenefit__c,Benefit_Plan__r.Contribution_to_Paid_Leave_Benefit__c FROM Employee__c WHERE Id =: empIds];
        	employeeId = emp.Id;
            contractId = emp.Fringe_Contract_Type__r.Id;
            Month_and_Year = emp.Month_and_Year__c;
        }catch(Exception e){e.getMessage();}  
        listOfFields = Dynamic_Fields__c.getValues(emp.Fringe_Contract_Type__r.Id) == null ? '' : Dynamic_Fields__c.getValues(emp.Fringe_Contract_Type__r.Id).List_of_Fields__c;
        system.debug('listOfFields'+listOfFields);
        String dateForMonth = monthNameMap.get(System.now().month())+' '+System.now().year();
        Month_and_Year = Month_and_Year != null ? Month_and_Year :dateForMonth;
        Integer fieldsCount = listOfFields.split('~').size();
        system.debug('emp'+emp);
        if(emp.Status__c == 'Underspent'){
            system.debug('listOfFields'+listOfFields);
            //HRA__c,Retirement__c
            if(Test.isRunningTest()){listOfFields = 'HRA__c,Retirement__c';} 
            String queryContractString = 'SELECT '+listOfFields+'  FROM Fringe_Contract__c WHERE Id =:  contractId';
            System.debug('-----'+queryContractString);
            sObject cusContractSetRec;
            
            Fringe_Contract__c fc;
            try{
                cusContractSetRec= Database.query(queryContractString);
                if(cusContractSetRec != null){
                    fc =[SELECT Id, Fringe_Allocation_Rules__r.Id, Set_Maximum_Funding_Level__c,Individual_Premium_Reserve_Account__c,Fringe_Allocation_Rules__r.Name, Fringe_Allocation_Rules__r.FringeBenefits__c, Fringe_Allocation_Rules__r.OverspentCategory__c, Fringe_Allocation_Rules__r.UnderspentCategory__c,Average_Cost_Accounting_Methods__c,Fr__c,Avg_Accounting_Rules__c FROM Fringe_Contract__c WHERE Id =: (Id)cusContractSetRec.get('Id')];
                }
            }catch(Exception e){
                e.getMessage();
            }
            String queryString = 'SELECT '+listOfFields+',Monthly_Short_List__c,Fringe_Rate__c,RetroActive_Adjustment_By_Hours__c,Monthly_Fringe_Obligation_Result__c,Monthly_Cost_Of_Benefit__c,Retro_Active__c,Curr_Reg_Hours_NYC__c,Month_and_Year__c,Employee__r.Id, Employee__r.Name  FROM Individual_Fringe_Benfit__c WHERE Month_and_Year__c =: Month_and_Year  AND Employee__r.Id =:  employeeId LIMIT 1';
            System.debug('-----'+queryString);
            sObject cusSetRec;
            try{
                cusSetRec= Database.query(queryString);
            }catch(Exception e){
                e.getMessage();
            }
            if(fc != null){
                System.debug('--******--'+fc.Fringe_Allocation_Rules__r.UnderspentCategory__c);            
                if(fc.Fringe_Allocation_Rules__r.UnderspentCategory__c == 'Average Cost Accounting' && fc.Average_Cost_Accounting_Methods__c =='Equivalent contributions' && fc.Fr__c == 'Average Cost Fringe Accounting Rule' && fc.Avg_Accounting_Rules__c == 'Quarterly'){
                    if(cusSetRec != null){	
                        for(String indiviField : listOfFields.split(',')){
                            if(!String.isBlank(indiviField)){
                                String objectName = 'Individual_Fringe_Benfit__c';
                                String fieldName = indiviField;
                                SObjectType r = ((SObject)(Type.forName('Schema.'+objectName).newInstance())).getSObjectType();
                                DescribeSObjectResult d = r.getDescribe();
                                String fieldType = String.valueOf(d.fields.getMap().get(fieldName).getDescribe().getType());
                                If(fieldType.equalsIgnoreCase('STRING')){
                                    cusSetRec.put(indiviField,String.valueOf(0.00));
                                }else{
                                    cusSetRec.put(indiviField,0.00);
                                }
                                String percen = (String)cusContractSetRec.get(indiviField);
                                decimal cal = (emp.Avg_cost_Qtr__c)*decimal.valueOf(percen)/100;
                                if(String.valueOf(cal.setScale(2)) != null){
                                    If(fieldType.equalsIgnoreCase('STRING')){
                                        cusSetRec.put(indiviField, String.valueOf(cal.setScale(2)));
                                    }else{
                                        cusSetRec.put(indiviField, cal.setScale(2));
                                    }
                                }
                            }
                        }
                        try{
                            Update cusSetRec;
                        }catch(Exception e){
                            System.debug('--'+e.getMessage());
                        }
                    }else{          
                        System.debug('-In-');
                        emp.Avg_Cost_Hrs__c = 0;
                        sObject cusSetcreateRec = new Individual_Fringe_Benfit__c();
                        cusSetcreateRec.put('Month_and_Year__c',Month_and_Year);
                        cusSetcreateRec.put('Employee__c',emp.Id);                    
                        for(String indiviField : listOfFields.split(',')){
                            if(!String.isBlank(indiviField)){
                                String objectName = 'Individual_Fringe_Benfit__c';
                                String fieldName = indiviField;
                                SObjectType r = ((SObject)(Type.forName('Schema.'+objectName).newInstance())).getSObjectType();
                                DescribeSObjectResult d = r.getDescribe();
                                String fieldType = String.valueOf(d.fields.getMap().get(fieldName).getDescribe().getType());
                                If(fieldType.equalsIgnoreCase('STRING')){
                                    cusSetcreateRec.put(indiviField,String.valueOf(0.00));
                                }else{
                                    cusSetcreateRec.put(indiviField,0.00);
                                }
                                String percen = (String)cusContractSetRec.get(indiviField);
                                decimal cal = (emp.Avg_cost_Qtr__c)*decimal.valueOf(percen)/100;
                                if(String.valueOf(cal.setScale(2)) != null){
                                    If(fieldType.equalsIgnoreCase('STRING')){
                                        cusSetcreateRec.put(indiviField, String.valueOf(cal.setScale(2)));
                                    }else{
                                        cusSetcreateRec.put(indiviField, cal.setScale(2));
                                    }
                                }
                            }
                        }
                        try{
                            System.debug('-UpcusSetcreateRec-'+cusSetcreateRec);
                            insert cusSetcreateRec;
                            System.debug('-IncusSetcreateRec-'+cusSetcreateRec);
                        }catch(Exception e){
                            System.debug('--'+e.getMessage());
                        }
                    }
                    try{
                        update emp;
                    }catch(Exception e){
                        System.debug('--'+e.getMessage());
                    }                
                } 
            }
        }
    }
}