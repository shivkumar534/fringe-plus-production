public class overspentDebitRule1 {
    /*public static void updateEmployeeFromMonthlyHours(Id monthlyHourId){
        String empUniqId, listOfFields, employeeId;
        decimal monthyear, downlevel;
        String contractId, Month_and_Year;
        
        Monthly_Hours__c mon = [SELECT Id, Name, EE__c,Month_and_Year__c, Curr_Reg_Hrs_NYC__c FROM Monthly_Hours__c WHERE Id =: monthlyHourId];
        empUniqId = mon.EE__c;
        monthyear = mon.Curr_Reg_Hrs_NYC__c;
        Month_and_Year = mon.Month_and_Year__c;
        Employee__c emp = [SELECT Id,Monthly_Fringe_Contribution__c,Name,Actual_Monthly_Hours__c,Fringe_Rate_lookup__c, Curr_Reg_Hrs_NYC__c,Monthly_Cost_Of_Benefits__c, Fringe_Contract_Type__r.Hour_Bank__c, Fringe_Contract_Type__r.Fringe_Allocation_Method__c, Roll_Back_Hours__c FROM Employee__c WHERE Name =: empUniqId];
        listOfFields = Dynamic_Fields__c.getValues(emp.Fringe_Contract_Type__r.Id).List_of_Fields__c;
        contractId = emp.Fringe_Contract_Type__r.Id;
        employeeId = emp.Id;
        emp.Month_and_Year__c = Month_and_Year;
        emp.Curr_Reg_Hrs_NYC__c =  monthyear > emp.Fringe_Contract_Type__r.Hour_Bank__c ? emp.Fringe_Contract_Type__r.Hour_Bank__c : monthyear;
        downlevel = monthyear > emp.Fringe_Contract_Type__r.Hour_Bank__c ? emp.Fringe_Contract_Type__r.Hour_Bank__c : monthyear;
        emp.Actual_Monthly_Hours__c = monthyear;
        try{
            update emp;
        }catch(Exception e){e.getMessage();}            
        
        Map<Integer,String> monthNameMap = new Map<Integer, String>{1 =>'January', 2=>'February', 3=>'March', 4=>'April', 5=>'May',6=>'June', 7=>'July', 8=>'August', 9=>'September',10=>'October',11=>'November', 12=>'December'};
            
            System.debug('----'+monthNameMap.get(System.now().month()));
        System.debug('-----'+System.now().year());
        Employee__c emp1;
        try{
            emp1 = [SELECT Id, Name,Status__c,Monthly_Fringe_Contribution__c,Actual_Monthly_Hours__c,HRA_Card_Monthly_Deposit__c,Fringe_Rate_lookup__c, Curr_Reg_Hrs_NYC__c,Monthly_Cost_Of_Benefits__c, Fringe_Contract_Type__r.Hour_Bank__c, Fringe_Contract_Type__r.Fringe_Allocation_Method__c, Roll_Back_Hours__c,Individual_Premium_Reserve_Account_Month__c,Rolling_Balance_of_IPRA__c,Month_and_Year__c,Fringe_Contract_Type__r.Credit_Rule__c,Fringe_Contract_Type__r.Debit_Rule__c, Monthly_Fringe_Amount_Allocated__c FROM Employee__c WHERE Id =: employeeId];
        }catch(Exception e){e.getMessage();}   
        String dateForMonth = monthNameMap.get(System.now().month())+' '+System.now().year();
        Month_and_Year = Month_and_Year != null ? Month_and_Year :dateForMonth;
        Integer fieldsCount = listOfFields.split('~').size();
        
        if(emp1.Status__c == 'Overspent'){
            String queryContractString = 'SELECT '+listOfFields+', Fringe_Allocation_Method__c FROM Fringe_Contract__c WHERE Id =:  contractId';
            System.debug('-----'+queryContractString);
            sObject cusContractSetRec;
            
            Fringe_Contract__c fc;
            try{
                cusContractSetRec= Database.query(queryContractString);
                if(cusContractSetRec != null){
                    fc =[SELECT Id, Fringe_Allocation_Rules__r.Id,Individual_Premium_Reserve_Account__c,Fringe_Allocation_Rules__r.Name, Fringe_Allocation_Rules__r.FringeBenefits__c, Fringe_Allocation_Rules__r.OverspentCategory__c, Fringe_Allocation_Rules__r.UnderspentCategory__c FROM Fringe_Contract__c WHERE Id =: (Id)cusContractSetRec.get('Id')];
                }
            }catch(Exception e){
                e.getMessage();
            }
            String queryString = 'SELECT '+listOfFields+',Monthly_Short_List__c,Curr_Reg_Hours_NYC__c,Month_and_Year__c,Employee__r.Id, Employee__r.Name  FROM Individual_Fringe_Benfit__c WHERE Month_and_Year__c =: Month_and_Year  AND Employee__r.Id =:  employeeId';
            System.debug('-----'+queryString);
            sObject cusSetRec;
            try{
                cusSetRec= Database.query(queryString);
            }catch(Exception e){
                e.getMessage();
            }
            System.debug('--******--'+fc.Fringe_Allocation_Rules__r.OverspentCategory__c);
            if( fc.Fringe_Allocation_Rules__r.OverspentCategory__c == 'Debit from Individual Premium Reserve Account and mainain "Debit amount from Ind Prm Res Acnt"'){
                decimal parcal = emp1.Monthly_Fringe_Amount_Allocated__c == null ? 0.00: emp1.Monthly_Fringe_Amount_Allocated__c;
                emp1.Rolling_Balance_of_IPRA__c = emp1.Rolling_Balance_of_IPRA__c == null? emp1.Individual_Premium_Reserve_Account_Month__c == null? 0.00:parcal+emp1.Individual_Premium_Reserve_Account_Month__c:parcal+emp1.Individual_Premium_Reserve_Account_Month__c;
                decimal lastmonthval = emp1.Individual_Premium_Reserve_Account_Month__c == null? 0.00:emp1.Individual_Premium_Reserve_Account_Month__c;
                decimal totalval = lastmonthval == null ?parcal:parcal+lastmonthval;
                cusSetRec.put('Individual_Premium_Reserve_Account__c',lastmonthval);
                cusSetRec.put('Rolling_Individual_Premium_Reserve__c',totalval );
            }
            else{
                sObject cusSetcreateRec = new Individual_Fringe_Benfit__c();
                cusSetcreateRec.put('Month_and_Year__c',Month_and_Year);
                cusSetcreateRec.put('Employee__c',emp.Id);
                if(fc.Individual_Premium_Reserve_Account__c == false){
                    for(String indiviField : listOfFields.split(',')){
                        if(!String.isBlank(indiviField)){
                            String objectName = 'Individual_Fringe_Benfit__c';
                            String fieldName = indiviField;
                            SObjectType r = ((SObject)(Type.forName('Schema.'+objectName).newInstance())).getSObjectType();
                            DescribeSObjectResult d = r.getDescribe();
                            String fieldType = String.valueOf(d.fields.getMap().get(fieldName).getDescribe().getType());
                            If(fieldType.equalsIgnoreCase('STRING')){
                                cusSetcreateRec.put(indiviField,String.valueOf(0.00));
                            }else{
                                cusSetcreateRec.put(indiviField,0.00);
                            }
                            
                        }
                    }
                }
            }
        }
    }*/
    
}