public class InsertHourBankAccountFromEmployee {
    private static boolean run = true;
    public static boolean runOnce(){
        if(run){
            run=false;
            return true;
        }else{
            return run;
        }
    }
    public static void monthlyOrWeeklyHours(Map<String, Employee__c> monthlyOrWeeklyHoursList, Map<String, decimal> setMapHourBank,  Map<String, decimal> setMapcob){
        List<Hours_Bank_Account__c> hbaList = new List<Hours_Bank_Account__c>();
        System.debug('---'+monthlyOrWeeklyHoursList.size() );
        Integer count = 1;
        if(monthlyOrWeeklyHoursList.size() > 0 ){
            for(String empId : monthlyOrWeeklyHoursList.keySet()){
                if(count <= monthlyOrWeeklyHoursList.size()){
                    Employee__c emp = monthlyOrWeeklyHoursList.get(empId);
                    Hours_Bank_Account__c hba = new Hours_Bank_Account__c();
                    hba.Hour_Bank_Requirements__c = setMapHourBank.get(emp.Name);
                    hba.Monthly_Fringe_Contributions__c = emp.Fringe_Rate_lookup__c * emp.Curr_Reg_Hrs_NYC__c;
                    hba.Hourly_Rates__c = emp.Fringe_Rate_lookup__c;
                    hba.Fringe_Rates__c = setMapcob.get(emp.Name)/setMapHourBank.get(emp.Name);
                    System.debug('-----'+emp.Curr_Reg_Hrs_NYC__c +'     '+ setMapHourBank.get(emp.Name));
                    if((emp.Curr_Reg_Hrs_NYC__c - setMapHourBank.get(emp.Name)) > 0){
                        hba.Credit_Hour_Bank_Accounts__c =  emp.Curr_Reg_Hrs_NYC__c - setMapHourBank.get(emp.Name);
                    }else if((emp.Curr_Reg_Hrs_NYC__c - setMapHourBank.get(emp.Name)) <= 0){
                        hba.Debit_Hour_Bank_Accounts__c = emp.Curr_Reg_Hrs_NYC__c - setMapHourBank.get(emp.Name);
                    }
                    hbaList.add(hba);
                    count++;
                }
            }
        }
        
        try{
            Insert hbaList;
        }catch(Exception e){System.debug('-----'+e.getMessage());}
    }
	
    
}