//  Author : Diwakar_Reddy_Vadde
//  Date   : 8th April 2021

public class UpdateclassBenefits {
    
    /*public static void Method(List<string> empIds) {
        
        // Capturing Current registered Hours
         Map<String, Decimal> monthlyHoursregMap = new Map<String, Decimal>();
        // Capturing Month & Year from monthly Hours
        Map<String, String> monthAndYear = new Map<String, String>();
        // Employee List
        List<Employee__c> empList = new List<Employee__c>();
        // Individual Fringe Benefits
        List<Individual_Fringe_Benfit__c> IfbList = new List<Individual_Fringe_Benfit__c>();
        
        for(Monthly_Hours__c mh : [SELECT Id, Name, EE__c,Month_and_Year__c, Curr_Reg_Hrs_NYC__c FROM Monthly_Hours__c WHERE EE__c In: empIds]){
            monthlyHoursregMap.put(mh.EE__c, mh.Curr_Reg_Hrs_NYC__c);
            monthAndYear.put(mh.EE__c,mh.Month_and_Year__c);
        }
        for(Employee__c emp: [SELECT id, name,Curr_Reg_Hrs_NYC__c FROM Employee__c WHERE Name IN: empIds]) {
            if(empIds.contains(emp.Name)){
                emp.Curr_Reg_Hrs_NYC__c = monthlyHoursregMap.get(emp.Name);
                empList.add(emp);
               }
        }
        try{
            if(empList.size() > 0){
                Upsert empList;
            }
        }catch(Exception e){
            System.debug('---exception---'+e.getMessage());
        }
        
        integer count = 1;
        integer recordcount =empIds.size();
        Map<Id,Employee__c> empMap = new Map<Id, Employee__c>();
        Map<Integer,String> monthNameMap = new Map<Integer, String>{1 =>'January', 2=>'February', 3=>'March', 4=>'April', 5=>'May',6=>'June', 7=>'July', 8=>'August', 9=>'September',10=>'October',11=>'November', 12=>'December'};
            for(Individual_Fringe_Benfit__c ifb : [select Id,Employee__c,HRA__c, Month_and_Year__c,Employee__r.id,Employee__r.Curr_Reg_Hrs_NYC__c,Class_Level_Of_Benefits2__c
                                                   from Individual_Fringe_Benfit__c 
                                                  where Month_and_Year__c =: monthNameMap.get(System.now().month())+' '+System.now().year() AND Employee__r.Name in : empIds]) {
                									if(count <= recordcount){
                                                      empMap.put(ifb.Employee__r.Id, ifb.Employee__r );
                                                      if(ifb.Employee__r.Curr_Reg_Hrs_NYC__c > 0 && ifb.Employee__r.Curr_Reg_Hrs_NYC__c <= 90) {
                                                          System.debug('is it bloew 90 :'+ifb.Employee__r.Curr_Reg_Hrs_NYC__c);
                                                           ifb.Class_Level_of_Benefits2__c = 'Class I Benefits';
                                     						ifbList.add(ifb);
                                                      }else if(ifb.Employee__r.Curr_Reg_Hrs_NYC__c > 90 && ifb.Employee__r.Curr_Reg_Hrs_NYC__c <= 130 ){
                                                          System.debug('is it between 90 & 130 :'+ifb.Employee__r.Curr_Reg_Hrs_NYC__c );
                                                          ifb.Class_Level_of_Benefits2__c = 'Class II Benefits';
                                     						ifbList.add(ifb);
                                                      }else if(ifb.Employee__r.Curr_Reg_Hrs_NYC__c > 130){
                                                          System.debug('is it above 130 :'+ifb.Employee__r.Curr_Reg_Hrs_NYC__c);
                                                          ifb.Class_Level_of_Benefits2__c = 'Class III Benefits';
                                     						ifbList.add(ifb);
                                                      }
                                                     System.debug('------'+ifb);
                                                     count++;
            }
    }
if(count <= recordCount){
            for(Employee__c famIds : [SELECT Id,Name,Status__c,Curr_Reg_Hrs_NYC__c
                                      FROM Employee__c WHERE Name IN: empIds]){
                if(famIds != null){
                    
                    if(famIds.Curr_Reg_Hrs_NYC__c <90) {
                        Individual_Fringe_Benfit__c ifbinsert = new Individual_Fringe_Benfit__c();
                        ifbinsert.Employee__c = famIds.Id;
                    	ifbinsert.Month_and_Year__c = monthNameMap.get(System.now().month())+' '+System.now().year();
                        ifbinsert.Class_Level_of_Benefits2__c = 'Class I Benefits';
                        IfbList.add(ifbinsert);
                    }else if(famIds.Curr_Reg_Hrs_NYC__c > 90 && famIds.Curr_Reg_Hrs_NYC__c < 130) {
                        Individual_Fringe_Benfit__c ifbinsert = new Individual_Fringe_Benfit__c();
                        ifbinsert.Employee__c = famIds.Id;
                    	ifbinsert.Month_and_Year__c = monthNameMap.get(System.now().month())+' '+System.now().year();
                        ifbinsert.Class_Level_of_Benefits2__c = 'Class II Benefits';
                        IfbList.add(ifbinsert);
                    }else if(famIds.Curr_Reg_Hrs_NYC__c > 130) {
                        Individual_Fringe_Benfit__c ifbinsert = new Individual_Fringe_Benfit__c();
                        ifbinsert.Employee__c = famIds.Id;
                    	ifbinsert.Month_and_Year__c = monthNameMap.get(System.now().month())+' '+System.now().year();
                        ifbinsert.Class_Level_of_Benefits2__c = 'Class III Benefits';
                        IfbList.add(ifbinsert);
                    }
                    
                    
                }
                  count++;
                                         
        }   
       
    }
            if(ifbList.size() > 0){
            try{
                upsert ifbList;
            }catch(Exception e){e.getMessage();}
}
}*/
}