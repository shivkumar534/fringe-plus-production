public class FringePriceBookCreation {
    
    @future
    public static void createPriceBook(List<Id> pbList){
        List<Pricebook2> productList = new List<Pricebook2>();
        for(Fringe_PriceBook__c fpb :[SELECT Id, Name, Fringe_Contract__c, Effactive_Date__c, Govt_Contractor__c, Hourly_Cost_of_Benefit__c, 
                                      Is_Standard_Price_Book__c, Is_Job_Classification__c, Is_Wage_Parity__c, Monthly_Cost_Of_Benefit__c, 
                                      Active__c, Description__c FROM Fringe_PriceBook__c WHERE Id IN:pbList]){
                                          Pricebook2 pd = new Pricebook2();
                                          pd.Name = fpb.Name;
                                          pd.Fringe_PriceBook__c = fpb.Id;
                                          pd.Monthly_Cost_Of_Benefit__c = fpb.Monthly_Cost_Of_Benefit__c;
                                          pd.Fringe_Contract__c = fpb.Fringe_Contract__c != null?fpb.Fringe_Contract__c:null;
                                          pd.Fringe_Rate_Effactive_Date__c = fpb.Effactive_Date__c != null? fpb.Effactive_Date__c:null;
                                          pd.Govt_Contractors__c = fpb.Govt_Contractor__c != null? fpb.Govt_Contractor__c:null;
                                          pd.Hourly_Cost_of_Benefit__c = fpb.Hourly_Cost_of_Benefit__c;
                                          pd.Is_Job_Classification__c = fpb.Is_Job_Classification__c; 
                                          pd.Is_Wage_Parity__c = fpb.Is_Wage_Parity__c;
                                          pd.IsActive = fpb.Active__c;
                                          pd.Description = fpb.Description__c != null? fpb.Description__c : null;
                                          
                                          
            productList.add(pd);
        }
        if(productList.size() > 0){
            Insert productList;
        }
        
    }
    
    
    public static void updatePriceBook(List<Id> pbList){
        List<Pricebook2> productList = new List<Pricebook2>();
        Map<Id,Fringe_PriceBook__c> pricebookobjList = new Map<Id,Fringe_PriceBook__c>();
        for(Fringe_PriceBook__c fpb :[SELECT Id, Name, Fringe_Contract__c, Effactive_Date__c, Govt_Contractor__c, Hourly_Cost_of_Benefit__c, 
                                      Is_Standard_Price_Book__c, Is_Job_Classification__c, Is_Wage_Parity__c, Monthly_Cost_Of_Benefit__c, 
                                      Active__c, Description__c FROM Fringe_PriceBook__c WHERE Id IN:pbList]){
                                      pricebookobjList.put(fpb.Id,fpb);
                                      }
        for(Pricebook2 pd :[SELECT Id, Name, Fringe_PriceBook__c,IsActive, Fringe_Contract__c, Govt_Contractor__c, Is_Job_Classification__c, 
                             Fringe_Rate_Effactive_Date__c, Is_Wage_Parity__c, Hourly_Cost_of_Benefit__c, Monthly_Cost_Of_Benefit__c 
                             FROM Pricebook2 WHERE Fringe_PriceBook__c IN:pbList]){
                                          Fringe_PriceBook__c fpb = pricebookobjList.get(pd.Fringe_PriceBook__c);
                                          pd.Name = fpb.Name;
                                          pd.Fringe_PriceBook__c = fpb.Id;
                                          pd.Monthly_Cost_Of_Benefit__c = fpb.Monthly_Cost_Of_Benefit__c;
                                          pd.Fringe_Contract__c = fpb.Fringe_Contract__c != null?fpb.Fringe_Contract__c:null;
                                          pd.Fringe_Rate_Effactive_Date__c = fpb.Effactive_Date__c != null? fpb.Effactive_Date__c:null;
                                          pd.Govt_Contractors__c = fpb.Govt_Contractor__c != null? fpb.Govt_Contractor__c:null;
                                          pd.Hourly_Cost_of_Benefit__c = fpb.Hourly_Cost_of_Benefit__c;
                                          pd.Is_Job_Classification__c = fpb.Is_Job_Classification__c; 
                                          pd.Is_Wage_Parity__c = fpb.Is_Wage_Parity__c;
                                          pd.IsActive = fpb.Active__c;
                                          pd.Description = fpb.Description__c != null? fpb.Description__c : null;
                                          
                                          
            productList.add(pd);
        }
        if(productList.size() > 0){
            update productList;
        }
        
    }
    
    //createProducts
    @future
    public static void createProducts(List<Id> pbList){
        List<Product2> productList = new List<Product2>();
        Set<Id> priceBookIds = new Set<Id>();
        Integer productInsertCount = 0;
        for(Fringe_Product__c fpb :[SELECT Id, Name, Fringe_PriceBook__c, Unit_Price__c, Active__c, Description__c, Job_Classification__c, 
                                    Product_Code_Product_Type__c,Product_Description_Benefit_Plan_Name__c, Product_Name__c, 
                                    TPA_Fee_on_Premium_Rate__c, TPA_FEE_Per_Hour_Per_Employee__c, TPA_Fee_PEPM__c FROM Fringe_Product__c WHERE Id IN:pbList]){
                                       //SELECT Id, Name, ProductCode, Family, IsActive, QuantityUnitOfMeasure, Job_Classification__c, TPA_Fee_on_Premium_Rat__c, TPA_Fee_PEPM__c, Build_in_Cost_of_Benefit__c, TPA_FEE_Per_Hour_Per_Employee__c FROM Product2
                                        Product2 pd = new Product2();
                                        pd.Name = fpb.Name;
                                        pd.Fringe_PriceBook__c = fpb.Fringe_PriceBook__c != null? fpb.Fringe_PriceBook__c:null;
                                        priceBookIds.add(fpb.Fringe_PriceBook__c);
                                        pd.Fringe_Product__c = fpb.Id;
                                        pd.Unit_Price__c = fpb.Unit_Price__c != null? fpb.Unit_Price__c: null;
                                        pd.ProductCode = fpb.Product_Code_Product_Type__c != null?fpb.Product_Code_Product_Type__c:null;
                                        pd.Family = fpb.Product_Name__c != null ? fpb.Product_Name__c : null;
                                        pd.TPA_Fee_on_Premium_Rat__c = fpb.TPA_Fee_on_Premium_Rate__c != null? fpb.TPA_Fee_on_Premium_Rate__c:null;
                                        pd.TPA_Fee_PEPM__c = fpb.TPA_Fee_PEPM__c != null?  fpb.TPA_Fee_PEPM__c : null;
                                        pd.Job_Classification__c = fpb.Job_Classification__c; 
                                        pd.TPA_FEE_Per_Hour_Per_Employee__c = fpb.TPA_FEE_Per_Hour_Per_Employee__c != null? fpb.TPA_FEE_Per_Hour_Per_Employee__c: null;
                                        pd.IsActive = fpb.Active__c;
                                        pd.Description = fpb.Description__c != null? fpb.Description__c : null;                       
                                        productList.add(pd);
        }
        if(productList.size() > 0){
            try{
                productInsertCount = productList.size();
                Insert productList;
            }catch(Exception e){e.getMessage();}
            
        }
        if(priceBookIds.size() > 0){
            List<Id> standardpbIds = new List<Id>();
            List<Id> productIds = new List<Id>();
            List<PricebookEntry> standardpelist = new List<PricebookEntry>();
            Integer productCount = pbList.size();
            Map<Id,Decimal> mapproduct = new Map<Id,Decimal>();
            for(Product2 pro :[SELECT Id, Name, ProductCode, Unit_Price__c,IsActive, Family, Job_Classification__c, TPA_Fee_on_Premium_Rat__c, TPA_Fee_PEPM__c, TPA_FEE_Per_Hour_Per_Employee__c, Build_in_Cost_of_Benefit__c, Fringe_PriceBook__c FROM Product2 WHERE Fringe_PriceBook__c IN: priceBookIds Order By Createddate DESC LIMIT: productCount]){
                productIds.add(pro.Id);
                mapproduct.put(pro.Id, pro.Unit_Price__c);
            }
            for(Pricebook2 standardPriceBook : [SELECT Id FROM Pricebook2 WHERE isStandard = true LIMIT: productCount]){
                standardpbIds.add(standardPriceBook.Id);
            }            
            for(Id piceBookIds : standardpbIds){
                for(Id proIds : productIds){
                    PricebookEntry priceBookEntrys = new PricebookEntry(
                        Pricebook2Id = piceBookIds,
                        Product2Id = proIds,
                        UnitPrice = mapproduct.get(proIds),
                        UseStandardPrice = false,
                        IsActive = true
                    );
                    standardpelist.add(priceBookEntrys);
                }
            }
            try{
                insert standardpelist;
            }catch(Exception e){e.getMessage();}
            Map<String, String> productMap = new Map<String, String>();
            for(Pricebook2 pd:[ SELECT Id, Name, Fringe_PriceBook__c, Is_Wage_Parity__c, Govt_Contractors__c FROM Pricebook2 WHERE Fringe_PriceBook__c IN: priceBookIds]){
                                  productMap.put(pd.Fringe_PriceBook__c, pd.Id);  
            }
            List<PricebookEntry> peList = new List<PricebookEntry>();
            if(productMap.size() > 0){
                for(Product2 pro :[SELECT Id, Name, ProductCode, Unit_Price__c,IsActive, Family, Job_Classification__c, TPA_Fee_on_Premium_Rat__c, TPA_Fee_PEPM__c, TPA_FEE_Per_Hour_Per_Employee__c, Build_in_Cost_of_Benefit__c, Fringe_PriceBook__c FROM Product2 WHERE Fringe_PriceBook__c IN: priceBookIds Order By Createddate DESC LIMIT :productInsertCount]){
                    //Id, Pricebook2Id, Name, Product2Id, UnitPrice, IsActive, ProductCode 
                    PricebookEntry pe = new PricebookEntry();
                    //pe.Name = pro.Name;
                    pe.Product2Id = pro.Id;
                    pe.UseStandardPrice = false;
                    pe.Pricebook2Id = productMap.get(pro.Fringe_PriceBook__c);
                    //pe.ProductCode = pro.ProductCode;
                    pe.UnitPrice = pro.Unit_Price__c;
                    pe.IsActive = pro.IsActive;
                    peList.add(pe);
                }
            }
            if(peList.size() > 0){
                try{
                    insert peList;
                }catch(Exception e){e.getMessage();}
            }
        }
    }
    
    @future
    public static void updateProducts(List<Id> pbList){
        List<Product2> productList = new List<Product2>();
        List<PricebookEntry> producentrytList = new List<PricebookEntry>();
        Set<Id> productIds = new Set<Id>();
        Integer productInsertCount = 0;
        Map<Id,Fringe_Product__c> productobjList = new Map<Id,Fringe_Product__c>();
        Map<Id,Fringe_Product__c> productentryobjList = new Map<Id,Fringe_Product__c>();
        for(Fringe_Product__c fpb :[SELECT Id, Name, Fringe_PriceBook__c, Unit_Price__c, Active__c, Description__c, Job_Classification__c, 
                                    Product_Code_Product_Type__c, Product_Description_Benefit_Plan_Name__c,Product_Name__c, 
                                    TPA_Fee_on_Premium_Rate__c, TPA_FEE_Per_Hour_Per_Employee__c, TPA_Fee_PEPM__c FROM Fringe_Product__c WHERE Id IN:pbList]){
                                    productobjList.put(fpb.Id,fpb);
                                    }
        for(Product2 pd :[SELECT Id, Name,Fringe_Product__c, ProductCode FROM Product2 WHERE Fringe_Product__c IN: pbList]){
            Fringe_Product__c fpb = productobjList.get(pd.Fringe_Product__c);
            productentryobjList.put(pd.Id,fpb);
            pd.Name = fpb.Name;
            //pd.Fringe_PriceBook__c = fpb.Fringe_PriceBook__c != null? fpb.Fringe_PriceBook__c:null;
            //priceBookIds.add(fpb.Fringe_PriceBook__c);
            pd.Unit_Price__c = fpb.Unit_Price__c != null? fpb.Unit_Price__c: null;
            pd.ProductCode = fpb.Product_Code_Product_Type__c != null?fpb.Product_Code_Product_Type__c:null;
            pd.Family = fpb.Product_Name__c != null ? fpb.Product_Name__c : null;
            pd.TPA_Fee_on_Premium_Rat__c = fpb.TPA_Fee_on_Premium_Rate__c != null? fpb.TPA_Fee_on_Premium_Rate__c:null;
            pd.TPA_Fee_PEPM__c = fpb.TPA_Fee_PEPM__c != null?  fpb.TPA_Fee_PEPM__c : null;
            pd.Job_Classification__c = fpb.Job_Classification__c; 
            pd.TPA_FEE_Per_Hour_Per_Employee__c = fpb.TPA_FEE_Per_Hour_Per_Employee__c != null? fpb.TPA_FEE_Per_Hour_Per_Employee__c: null;
            pd.IsActive = fpb.Active__c;
            pd.Description = fpb.Description__c != null? fpb.Description__c : null;                       
            productList.add(pd);
            productIds.add(pd.Id);
        }
        if(productList.size() > 0){
            update productList;
        }
        for(PricebookEntry pro1 :[SELECT Id, Name, Product2Id, Pricebook2Id, IsActive, UseStandardPrice, UnitPrice, ProductCode FROM PricebookEntry WHERE Product2Id IN: productIds]){
            Fringe_Product__c fpb = productentryobjList.get(pro1.Product2Id);
            pro1.UnitPrice = fpb.Unit_Price__c;
            producentrytList.add(pro1);
        }
        if(producentrytList.size() > 0){
            update producentrytList;
        }
    }
}