@isTest
private class MethodCHandlerTest {
	@isTest
    static void myTestMethod() {
        //Test Account Creation
        /*Account acc = new Account();
        acc.name = 'testAcc';
        
        insert acc;
        
        Pricebook2 pbk = new Pricebook2();
        pbk.Name ='pricebook';
        pbk.IsActive = true;
        pbk.Hourly_Cost_of_Benefit__c = true;
        insert pbk;
        
        Product2 prd= new Product2();
        prd.Name='product';
        prd.IsActive=true;
        insert prd;
        
        PricebookEntry pbe = new PricebookEntry();
        pbe.IsActive=true;
        pbe.Pricebook2Id=pbk.id;
        pbe.Product2Id=prd.id;
        pbe.UnitPrice=100;
        insert pbe;
        //Test Fringe contract creation
        Fringe_Contract__c fc = new Fringe_Contract__c();
        fc.Fringe_Rate__c = 3;
        fc.Contract_Type__c = 'SCA - Service Contract Act';
        fc.TPA_Percentage__c = 10;
        fc.Retirement__c = '25';
        fc.PaidLeave__c = '25';
        
        
        insert fc;
        
        Dynamic_Fields__c df = new Dynamic_Fields__c();
        df.name = fc.id;
        df.List_of_Fields__c = 'HRA__c,Retirement__c,PaidLeave__c';
        
        insert df;
        
        
        //Test Employee creation
        employee__C emp = new employee__c();
        emp.Name = 'testEmp';
        emp.Month_and_Year__c = 'May 2021';
        
        emp.Fringe_Contract_Type__c = fc.id;
        emp.Curr_Reg_Hrs_NYC__c = 120;
        
        
        
        insert emp;
       
        
        //Test Monthly hours creation
        Monthly_Hours__c mh = new Monthly_Hours__c();
        mh.Employee__c = emp.id;
        mh.EE__c = 'testEmp';
        mh.Curr_Reg_Hrs_NYC__c = 120;
        insert mh;
		*/
        Account acc1 = new Account();
        acc1.name = 'testAcc';
        
        insert acc1;
        Govt_Contractor__c acc= new Govt_Contractor__c();
        acc.Name = 'FBA';
        //acc.Job_Classification__c = 'Diver 1 (Wet);Diver 2 (Standby);Drywall Installer/Lather;Drywall Stocker/Scrapper;Carpenter 1 (Bridge)';
        insert acc;
        
        Fringe_Allocation_Rules__c far = new Fringe_Allocation_Rules__c();
        far.Name = 'Method A1';
        far.FringeBenefits__c = 'HRA;Paid Leave Fringe Reserve Account';
        far.OverspentCategory__c = 'Maintain Monthly Short List/Report';
        far.UnderspentCategory__c = 'Allocate to Individual Fringe Benefit Account Types';
        
        insert far;
        
        
        Pricebook2 pb= new Pricebook2();
        pb.Name = 'FBA cost of Benefit';
        pb.Is_Job_Classification__c = false;
        pb.Is_Wage_Parity__c = false;
        pb.Govt_Contractors__c = acc.Id;
        pb.Monthly_Cost_Of_Benefit__c = true;
        pb.Hourly_Cost_of_Benefit__c = false;
        
        insert pb;
        
        List<Product2> pd = new List<Product2>();
        for(Integer i = 1; i <= 3;i++){
            Product2 p = new Product2();
            p.Name = 'Employee Only';
            p.Job_Classification__c = false;
            if(i == 1){
                p.ProductCode = 'BCBS PPO CHOICE';    
            }else if(i == 2){
                p.ProductCode = 'DELTA DENTAL PREMIER PLAN';   
            }else{
                p.ProductCode = '$10,000 PLAN';   
            }
            
            p.Family = 'Cost of Benefit';
            p.IsActive = true;
            
            pd.add(p);
        }
        
        insert pd;
        
         List<Id> proIdList = new List<Id>();
         for(Product2 pd1 :[SELECT Id, Name, Job_Classification__c, ProductCode, Family, IsActive, ExternalDataSourceId FROM Product2]){
             proIdList.add(pd1.Id);
         }
        List<PricebookEntry> pdlist = new List<PricebookEntry>();
        Integer count =1;
        for(PricebookEntry pe :[SELECT UnitPrice, Id, Name, Pricebook2Id, Product2Id, IsActive, ProductCode FROM PricebookEntry WHERE Product2Id IN: proIdList]){
            if(count <= 3){
                pe.UnitPrice = 100;
                pdlist.add(pe);
                count++;
            }
        }
        update pdlist;
        Fringe_Contract__c fc = new Fringe_Contract__c();
        fc.Contract_Name__c = 'Test1';
        fc.Govt_Contractor__c = acc.Id;
        fc.Fringe_Allocation_Rules__c = far.Id;
        fc.Price_Book__c = pb.Id;
        fc.Fringe_Rate__c = 5;
        fc.Hour_Bank__c = 120;
         fc.HRA__c = '50';
         fc.PaidLeave__c = '50';
        fc.Contract_Type__c = 'SCA - Service Contract Act';
        fc.Benefit_Plan__c = 'Health Insurance;Health Reimbursement Accounts (HRAs);Dental Insurance';
        //fc.Fringe_Rate_Based_on_Job_Classification__c = true;
        fc.SELECT_COST_OF_BENEFITS__c = 'Monthly Cost of Benefits';
        fc.Reporting_Type__c = 'Monthly Hours Report';
        
        insert fc;
         List<Individual_Fringe_Benefits_Percentages__c> ifbpList = new List<Individual_Fringe_Benefits_Percentages__c>();
         Dynamic_Fields__c df = new Dynamic_Fields__c();
         for(Integer i = 1; i<=2;i++){
             Individual_Fringe_Benefits_Percentages__c ifbp = new Individual_Fringe_Benefits_Percentages__c();
             df.Name = String.valueOf(fc.Id);
             ifbp.Percentage__c = 50;
             if(i == 1){
                 ifbp.Individual_Fringe_Benefit_Account__c = 'HRA';
                 df.List_of_Fields__c = 'HRA__c';
             }else{
                 ifbp.Individual_Fringe_Benefit_Account__c = 'PaidLeave';
                 df.List_of_Fields__c = 'HRA__c,PaidLeave__c';
             }
             ifbp.Fringe_Contract__c = fc.Id;
             ifbpList.add(ifbp);
         }
         insert df;
         insert ifbpList;
        
        pb.Fringe_Contract__c = fc.Id;
        update pb;
        
         Employee__c em = new Employee__c();
         em.Name = 'David';
         em.Fringe_Contract_Type__c = fc.Id;
         em.Unique_Identifier__c = 'David';
         em.Govt_Contractor__c = acc.Id;
         em.Emp_Status__c = 'Active';
        em.Rolling_Balance_of_IPRA__c = 1000;
        em.Monthly_Cost_Of_Benefit__c = 100;
        em.Fringe_Result_Category__c='Underspent';
        em.Hour_Bank_cat__c = 'Underspent';
        em.Rolling_Hour_Bank_Account_Balance__c = 100;
        em.Hourly_Cost_Of_benefit2__c = 150;
         em.Dental_Insurance_Plan__c = 'DELTA DENTAL PREMIER PLAN';
         em.Health_Insurance_Plan__c = 'BCBS PPO CHOICE';
         em.Life_Insurance_Plan__c = '$10,000 PLAN';
         em.Coverage_Tier__c = 'Employee Only';
         
         insert em;
        
        Monthly_Hours__c mh = new Monthly_Hours__c();
         mh.Name = 'David';
         mh.EE__c = 'David';
         //mh.zipcode = '';
         mh.EmpStatus__c = 'A';
         mh.Month_and_Year__c = 'March 2021';
         mh.Curr_Reg_Hrs_NYC__c = 100;
         mh.Govt_Contractors__c = acc.Id;
         mh.Fringe_Contract__c = fc.Id;
         mh.Employee__c = em.Id;
         mh.Coverage_Tier__c = 'Employee Only';
         //mh.Job_Classification__c = '';
         //mh.Job_Classification_Zip_code__c = '';
         mh.Cost_Of_Benefit__c = pb.Id;
         insert mh;
         List<Id> mhIdList = new List<Id>();
        mhIdList.add(mh.Id);
        system.assertEquals(mh != null,true);
        
        Hours_Bank_Account__c hba = new Hours_Bank_Account__c();
        hba.Employee__c = em.Id;
        hba.Rolling_Hour_Bank_Account_Balance__c = 1000;
        insert hba;
        
        Test.startTest();
        //MethodCbatchclass
        Database.executeBatch(new MethodCbatchclass(mhIdList), 1);
        //MethodCHandler.Method(mh.id);
        //RuleBasedOnHourBanks.method(mhIdList);
        EmployeeTerminationInvoiceController to = new EmployeeTerminationInvoiceController();
        to.fetchPDFData();
		EmployeeTerminationInvoiceController.updateAllValuesToZero(em.Id);
		
        
        pb.Fringe_Rate_Effactive_Date__c = System.today();
        update pb;
        
        pb.Fringe_Rate_Effactive_Date__c = System.today();
        pb.Is_Job_Classification__c = true;
        update pb;
		
        Individual_Fringe_Benfit__c ifbnew = new Individual_Fringe_Benfit__c();
        ifbnew.Month_and_Year__c = 'April 2021';
        ifbnew.Employee__c = em.Id;
        ifbnew.Retro_Active__c = True;
        ifbnew.Fringe_Rate__c = 5;//pbMap
        ifbnew.Curr_Reg_Hours_NYC__c = 100;
        ifbnew.Monthly_Fringe_Obligation_Result__c = 5 * 100 - 200;
        ifbnew.Monthly_Cost_Of_Benefit__c = 200;
        ifbnew.After_Retro_Hours_Adjustments__c = 300 - 100;
        
        insert ifbnew;
        List<Id> dup = new List<Id>();
        dup.add(em.Id);
        RuleBasedOnClassLevelCoverage.benefits(ifbnew.Id);
        EmployeeUpdateOnFringeRateChange.updateEmployeeFringeRate(ifbnew.Id);
        EmployeeTerminationInvoiceController.createIFBRecordsAfterTermination(dup);
		EmployeeList.getData();   
        
        mh.Curr_Reg_Hrs_NYC__c = 110;
		update mh;        
        Database.executeBatch(new MethodCbatchclass(mhIdList), 1);
        Test.stopTest();
    }
    @isTest
    static void myTestMethod1() {
        Account acc1 = new Account();
        acc1.name = 'testAcc';
        
        insert acc1;
        Govt_Contractor__c acc= new Govt_Contractor__c();
        acc.Name = 'FBA';
        //acc.Job_Classification__c = 'Diver 1 (Wet);Diver 2 (Standby);Drywall Installer/Lather;Drywall Stocker/Scrapper;Carpenter 1 (Bridge)';
        insert acc;
        
        Fringe_Allocation_Rules__c far = new Fringe_Allocation_Rules__c();
        far.Name = 'Method A1';
        far.FringeBenefits__c = 'HRA;Paid Leave Fringe Reserve Account';
        far.OverspentCategory__c = 'Maintain Monthly Short List/Report';
        far.UnderspentCategory__c = 'Allocate to Individual Fringe Benefit Account Types';
        
        insert far;
        
        
        Pricebook2 pb= new Pricebook2();
        pb.Name = 'FBA cost of Benefit';
        pb.Is_Job_Classification__c = false;
        pb.Is_Wage_Parity__c = false;
        pb.Govt_Contractors__c = acc.Id;
        pb.Monthly_Cost_Of_Benefit__c = true;
        pb.Hourly_Cost_of_Benefit__c = false;
        
        insert pb;
        
        List<Product2> pd = new List<Product2>();
        for(Integer i = 1; i <= 3;i++){
            Product2 p = new Product2();
            p.Name = 'Employee Only';
            p.Job_Classification__c = false;
            if(i == 1){
                p.ProductCode = 'BCBS PPO CHOICE';    
            }else if(i == 2){
                p.ProductCode = 'DELTA DENTAL PREMIER PLAN';   
            }else{
                p.ProductCode = '$10,000 PLAN';   
            }
            
            p.Family = 'Cost of Benefit';
            p.IsActive = true;
            
            pd.add(p);
        }
        
        insert pd;
        
         List<Id> proIdList = new List<Id>();
         for(Product2 pd1 :[SELECT Id, Name, Job_Classification__c, ProductCode, Family, IsActive, ExternalDataSourceId FROM Product2]){
             proIdList.add(pd1.Id);
         }
        List<PricebookEntry> pdlist = new List<PricebookEntry>();
        Integer count =1;
        for(PricebookEntry pe :[SELECT UnitPrice, Id, Name, Pricebook2Id, Product2Id, IsActive, ProductCode FROM PricebookEntry WHERE Product2Id IN: proIdList]){
            if(count <= 3){
                pe.UnitPrice = 100;
                pdlist.add(pe);
                count++;
            }
        }
        update pdlist;
        Fringe_Contract__c fc = new Fringe_Contract__c();
        fc.Contract_Name__c = 'Test1';
        fc.Govt_Contractor__c = acc.Id;
        fc.Fringe_Allocation_Rules__c = far.Id;
        fc.Price_Book__c = pb.Id;
        fc.Fringe_Rate__c = 5;
        fc.Hour_Bank__c = 120;
         fc.HRA__c = '50';
         fc.PaidLeave__c = '50';
        fc.Contract_Type__c = 'SCA - Service Contract Act';
        fc.Benefit_Plan__c = 'Health Insurance;Health Reimbursement Accounts (HRAs);Dental Insurance';
        //fc.Fringe_Rate_Based_on_Job_Classification__c = true;
        fc.SELECT_COST_OF_BENEFITS__c = 'Monthly Cost of Benefits';
        fc.Reporting_Type__c = 'Monthly Hours Report';
        
        insert fc;
         List<Individual_Fringe_Benefits_Percentages__c> ifbpList = new List<Individual_Fringe_Benefits_Percentages__c>();
         Dynamic_Fields__c df = new Dynamic_Fields__c();
         for(Integer i = 1; i<=2;i++){
             Individual_Fringe_Benefits_Percentages__c ifbp = new Individual_Fringe_Benefits_Percentages__c();
             df.Name = String.valueOf(fc.Id);
             ifbp.Percentage__c = 50;
             if(i == 1){
                 ifbp.Individual_Fringe_Benefit_Account__c = 'HRA';
                 df.List_of_Fields__c = 'HRA__c';
             }else{
                 ifbp.Individual_Fringe_Benefit_Account__c = 'PaidLeave';
                 df.List_of_Fields__c = 'HRA__c,PaidLeave__c';
             }
             ifbp.Fringe_Contract__c = fc.Id;
             ifbpList.add(ifbp);
         }
         insert df;
         insert ifbpList;
        
        pb.Fringe_Contract__c = fc.Id;
        update pb;
        
         Employee__c em = new Employee__c();
         em.Name = 'David';
         em.Fringe_Contract_Type__c = fc.Id;
         em.Unique_Identifier__c = 'David';
         em.Govt_Contractor__c = acc.Id;
         em.Emp_Status__c = 'Active';
        em.Rolling_Balance_of_IPRA__c = 1000;
        em.Monthly_Cost_Of_Benefit__c = 100;
        em.Fringe_Result_Category__c='Underspent';
        em.Hour_Bank_cat__c = 'Overspent';
        em.Rolling_Hour_Bank_Account_Balance__c = 100;
        em.Hourly_Cost_Of_benefit2__c = 150;
         em.Dental_Insurance_Plan__c = 'DELTA DENTAL PREMIER PLAN';
         em.Health_Insurance_Plan__c = 'BCBS PPO CHOICE';
         em.Life_Insurance_Plan__c = '$10,000 PLAN';
         em.Coverage_Tier__c = 'Employee Only';
         
         insert em;
        
        Monthly_Hours__c mh = new Monthly_Hours__c();
         mh.Name = 'David';
         mh.EE__c = 'David';
         //mh.zipcode = '';
         mh.EmpStatus__c = 'A';
         mh.Month_and_Year__c = 'March 2021';
         mh.Curr_Reg_Hrs_NYC__c = 150;
         mh.Govt_Contractors__c = acc.Id;
         mh.Fringe_Contract__c = fc.Id;
         mh.Employee__c = em.Id;
         mh.Coverage_Tier__c = 'Employee Only';
         //mh.Job_Classification__c = '';
         //mh.Job_Classification_Zip_code__c = '';
         mh.Cost_Of_Benefit__c = pb.Id;
         insert mh;
         List<Id> mhIdList = new List<Id>();
        mhIdList.add(mh.Id);
        system.assertEquals(mh != null,true);
        
        Hours_Bank_Account__c hba = new Hours_Bank_Account__c();
        hba.Employee__c = em.Id;
        hba.Rolling_Hour_Bank_Account_Balance__c = 1000;
        insert hba;
        
        Test.startTest();
        //MethodCbatchclass
        Database.executeBatch(new MethodCbatchclass(mhIdList), 1);
        //MethodCHandler.Method(mh.id);
        //RuleBasedOnHourBanks.method(mhIdList);
        EmployeeTerminationInvoiceController to = new EmployeeTerminationInvoiceController();
        to.fetchPDFData();
		EmployeeTerminationInvoiceController.updateAllValuesToZero(em.Id);     
        
        
        
        pb.Fringe_Rate_Effactive_Date__c = System.today();
        update pb;
        
        pb.Fringe_Rate_Effactive_Date__c = System.today();
        pb.Is_Job_Classification__c = true;
        update pb;
		
        Individual_Fringe_Benfit__c ifbnew = new Individual_Fringe_Benfit__c();
        ifbnew.Month_and_Year__c = 'April 2021';
        ifbnew.Employee__c = em.Id;
        ifbnew.Retro_Active__c = True;
        ifbnew.Fringe_Rate__c = 5;//pbMap
        ifbnew.Curr_Reg_Hours_NYC__c = 100;
        ifbnew.Monthly_Fringe_Obligation_Result__c = 5 * 100 - 200;
        ifbnew.Monthly_Cost_Of_Benefit__c = 200;
        ifbnew.After_Retro_Hours_Adjustments__c = 300 - 100;
        
        insert ifbnew;
        List<Id> dup = new List<Id>();
        dup.add(em.Id);
        RuleBasedOnClassLevelCoverage.benefits(ifbnew.Id);
        EmployeeUpdateOnFringeRateChange.updateEmployeeFringeRate(ifbnew.Id);
        EmployeeTerminationInvoiceController.createIFBRecordsAfterTermination(dup);
		EmployeeList.getData();   
                
        mh.Curr_Reg_Hrs_NYC__c = 110;
		update mh;        
        Database.executeBatch(new MethodCbatchclass(mhIdList), 1);
        
        Test.stopTest();
    }

}