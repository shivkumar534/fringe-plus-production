public class UpsertMonthlyHoursReportToEmployee {
    /*public static void upsertToEmployee(Map<String, Decimal> monthlyHoursMap){
        List<Employee__c> empList = new List<Employee__c>();
        Map<String, Decimal> monthlyHoursregMap = new Map<String, Decimal>();
        Map<String, Employee__c> monthlyOrWeeklyHoursMap = new Map<String, Employee__c>();
        Map<String, String> monthAndYear = new Map<String, String>();
        Map<String, decimal> fringecontractBH = new  Map<String, decimal>();
        Map<String, decimal> fringeEmpCob = new  Map<String, decimal>();
        Map<String, Id> junctionrecord = new Map<String, Id>();
        List<Hours_Bank_Account__c> hbaList = new List<Hours_Bank_Account__c>();
        for(Monthly_Hours__c mh : [SELECT Id, Name, EE__c,Month_and_Year__c, Curr_Reg_Hrs_NYC__c FROM Monthly_Hours__c WHERE EE__c In: monthlyHoursMap.keySet()]){
            monthlyHoursregMap.put(mh.EE__c, mh.Curr_Reg_Hrs_NYC__c);
            monthAndYear.put(mh.EE__c,mh.Month_and_Year__c);
        }
		for(Employee__c emp: [SELECT Id, Name,Actual_Monthly_Hours__c,Fringe_Rate_lookup__c, Curr_Reg_Hrs_NYC__c,Monthly_Cost_Of_Benefits__c, Fringe_Contract_Type__r.Capping_Hours__c, Fringe_Contract_Type__r.Fringe_Allocation_Method__c, Roll_Back_Hours__c FROM Employee__c WHERE Name IN: monthlyHoursMap.keySet() AND Fringe_Contract_Type__r.Fringe_Allocation_Method__c = 'Hour Bank Accounting']){
            if(monthlyHoursMap.containsKey(emp.Name)){
                junctionrecord.put(emp.Name, emp.Id);
                emp.Curr_Reg_Hrs_NYC__c = monthlyHoursMap.get(emp.Name) > emp.Fringe_Contract_Type__r.Capping_Hours__c ? emp.Fringe_Contract_Type__r.Capping_Hours__c : monthlyHoursMap.get(emp.Name);
                emp.Actual_Monthly_Hours__c = monthlyHoursregMap.get(emp.Name);
                emp.Roll_Back_Hours__c = monthlyHoursregMap.get(emp.Name) > emp.Fringe_Contract_Type__r.Capping_Hours__c ? (monthlyHoursMap.get(emp.Name) - emp.Fringe_Contract_Type__r.Capping_Hours__c)+emp.Roll_Back_Hours__c  : emp.Roll_Back_Hours__c + (monthlyHoursMap.get(emp.Name) - emp.Fringe_Contract_Type__r.Capping_Hours__c);
                monthlyOrWeeklyHoursMap.put(emp.Name, emp);
                fringeEmpCob.put(emp.Name, emp.Monthly_Cost_Of_Benefits__c);
                fringecontractBH.put(emp.Name, emp.Fringe_Contract_Type__r.Capping_Hours__c);
                empList.add(emp);
            }
        }   
        
        try{
            
            if(empList.size() > 0){
                Upsert empList;
            }
        }catch(Exception e){System.debug('---exception---'+e.getMessage());}
        
        Integer count = 1;
        Integer actualcount = monthlyOrWeeklyHoursMap.size();
        System.debug('---'+monthlyOrWeeklyHoursMap);
        Map<String, decimal> fringeResults = new Map<String, decimal>();
        Map<Id, Id> fringecontracts = new Map<Id, Id>();
        Map<Integer,String> monthNameMap = new Map<Integer, String>{1 =>'January', 2=>'February', 3=>'March', 4=>'April', 5=>'May',6=>'June', 7=>'July', 8=>'August', 9=>'September',10=>'October',11=>'November', 12=>'December'};
            
            System.debug('----'+monthNameMap.get(System.now().month()));
        System.debug('-----'+System.now().year());
        if(monthlyOrWeeklyHoursMap.size() > 0){
            for(Hours_Bank_Account__c hba : [SELECT Id, Name,Monthly_Fringe_Contribution__c,Employee__c,Employee__r.Fringe_Contract_Type__r.Id,Employee__r.Fringe_Contract_Type__r.Capping_Hours__c, Employee__r.Actual_Monthly_Hours__c,Employee__r.Roll_Back_Hours__c,Employee__r.Fringe_Rate_lookup__c,Employee__r.Curr_Reg_Hrs_NYC__c,
                                             Monthly_Fringe_Contributions__c,Employee__r.Monthly_Cost_Of_Benefits__c,Employee__r.Name, Month_and_Year__c FROM Hours_Bank_Account__c WHERE Month_and_Year__c =: monthNameMap.get(System.now().month())+' '+System.now().year() ]){
                if(hba != null){
                    String empnameId = hba.Employee__r.Name;
                    //Employee__c emp = monthlyOrWeeklyHoursMap.get(empnameId);Monthly_Cost_Of_Benefits__c
                    //System.debug('----'+fringecontractBH.get(hba.Employee__r.Name));
                    fringecontracts.put(hba.Employee__r.Fringe_Contract_Type__r.Id,hba.Employee__r.Id);
                    hba.Hour_Bank_Requirements__c = hba.Employee__r.Fringe_Contract_Type__r.Capping_Hours__c;
                    hba.Monthly_Fringe_Contributions__c = hba.Employee__r.Fringe_Rate_lookup__c * hba.Employee__r.Curr_Reg_Hrs_NYC__c;
                    hba.Hourly_Rates__c = hba.Employee__r.Monthly_Cost_Of_Benefits__c/hba.Employee__r.Fringe_Contract_Type__r.Capping_Hours__c;
                    hba.Fringe_Rates__c = hba.Employee__r.Fringe_Rate_lookup__c;
                    hba.Employee__c = hba.Employee__r.Id;
                    hba.Rolling_Hour_Bank_Account_Balance__c = hba.Employee__r.Roll_Back_Hours__c;
                    if((hba.Employee__r.Actual_Monthly_Hours__c - hba.Employee__r.Fringe_Contract_Type__r.Capping_Hours__c) > 0){
                        hba.Credit_Hour_Bank_Accounts__c =  hba.Employee__r.Actual_Monthly_Hours__c - hba.Employee__r.Fringe_Contract_Type__r.Capping_Hours__c;
                        hba.Debit_Hour_Bank_Accounts__c = 0.00;
                        hba.Status__c = 'Underspent';
                        hba.Fringe_Results__c = hba.Monthly_Fringe_Contributions__c -hba.Monthly_Fringe_Contribution__c;
                        if(hba.Monthly_Fringe_Contributions__c -hba.Monthly_Fringe_Contribution__c > 0){
                            fringeResults.put(hba.Employee__r.Id, hba.Monthly_Fringe_Contributions__c -hba.Monthly_Fringe_Contribution__c);
                        }
                    }else {
                        if(hba.Employee__r.Actual_Monthly_Hours__c - hba.Employee__r.Fringe_Contract_Type__r.Capping_Hours__c < 0){
                            hba.Debit_Hour_Bank_Accounts__c = hba.Employee__r.Actual_Monthly_Hours__c - hba.Employee__r.Fringe_Contract_Type__r.Capping_Hours__c;
                            hba.Credit_Hour_Bank_Accounts__c = 0.00;
                             hba.Status__c = 'Overspent';
                            if(hba.Monthly_Fringe_Contributions__c -hba.Monthly_Fringe_Contribution__c < 0){
                                fringeResults.put(hba.Employee__r.Id, hba.Monthly_Fringe_Contributions__c -hba.Monthly_Fringe_Contribution__c);
                            }
                        }else{
                            hba.Status__c = 'Obligation met';
                        }
                    }
                    hbaList.add(hba);
                    count++;
                }
            }
            if(count <= actualcount){
                for(Employee__c emp: [SELECT Id, Name,Fringe_Contract_Type__r.Id,Actual_Monthly_Hours__c,Fringe_Rate_lookup__c, Curr_Reg_Hrs_NYC__c,Monthly_Cost_Of_Benefits__c, Fringe_Contract_Type__r.Capping_Hours__c, Fringe_Contract_Type__r.Fringe_Allocation_Method__c, Roll_Back_Hours__c FROM Employee__c WHERE Name IN: monthlyHoursMap.keySet() AND Fringe_Contract_Type__r.Fringe_Allocation_Method__c = 'Hour Bank Accounting']){
                //for(String empId : monthlyOrWeeklyHoursMap.keySet()){
                    if(emp != null){
                        Hours_Bank_Account__c hba = new Hours_Bank_Account__c();
                        //Employee__c emp = monthlyOrWeeklyHoursMap.get(empId);
                        fringecontracts.put(emp.Fringe_Contract_Type__r.Id,emp.Id);
                        hba.Hour_Bank_Requirements__c = fringecontractBH.get(emp.Name);
                        hba.Monthly_Fringe_Contributions__c = emp.Fringe_Rate_lookup__c * emp.Curr_Reg_Hrs_NYC__c;
                        hba.Hourly_Rates__c = fringeEmpCob.get(emp.Name)/fringecontractBH.get(emp.Name);
                        hba.Fringe_Rates__c = emp.Fringe_Rate_lookup__c;
                        hba.Rolling_Hour_Bank_Account_Balance__c = emp.Roll_Back_Hours__c;
                        hba.Month_and_Year__c = monthNameMap.get(System.now().month())+' '+System.now().year();
                        hba.Employee__c = emp.Id;
                        if((emp.Actual_Monthly_Hours__c - fringecontractBH.get(emp.Name)) > 0){
                            hba.Credit_Hour_Bank_Accounts__c =  emp.Actual_Monthly_Hours__c - fringecontractBH.get(emp.Name);
                            hba.Debit_Hour_Bank_Accounts__c = 0.00;
                            hba.Status__c = 'Underspent';
                            if(hba.Monthly_Fringe_Contributions__c -hba.Monthly_Fringe_Contribution__c > 0){
                                fringeResults.put(hba.Employee__r.Id, hba.Monthly_Fringe_Contributions__c -hba.Monthly_Fringe_Contribution__c);
                            }
                        }else {
                            if(emp.Actual_Monthly_Hours__c - fringecontractBH.get(emp.Name) < 0){
                                hba.Debit_Hour_Bank_Accounts__c = emp.Actual_Monthly_Hours__c - fringecontractBH.get(emp.Name);
                                hba.Credit_Hour_Bank_Accounts__c = 0.00;
                                hba.Status__c = 'Overspent';
                                if(hba.Monthly_Fringe_Contributions__c -hba.Monthly_Fringe_Contribution__c < 0){
                                    fringeResults.put(hba.Employee__r.Id, hba.Monthly_Fringe_Contributions__c -hba.Monthly_Fringe_Contribution__c);
                                }
                            }else{
                                hba.Status__c = 'Obligation met';
                            }
                        }
                        
                        hbaList.add(hba);
                        count++;
                    }
                }
            }
            
            try{
                if(hbaList.size() > 0){
                    upsert hbaList;
                }
            }catch(Exception e){System.debug('---exception---'+e.getMessage());}
        }
        
        
    }
    
    public static void updateOrcreateFringeBenefitAccount(Map<String, decimal> fringeResults, Map<Id,Id> fringecontracts, String obligationStatus){
        Integer count = 1;
        Integer actualCount = fringeResults.size();
        Map<String, decimal> ifbpMap = new Map<String, decimal>();
        List<Individual_Fringe_Benfit__c> ifbList = new List<Individual_Fringe_Benfit__c>();
        for(Individual_Fringe_Benefits_Percentages__c ifbp : [SELECT Id, Individual_Fringe_Benefit_Account__c, Percentage__c, 
                                                              Fringe_Contract__c,
                                                              Fringe_Contract__r.Id FROM Individual_Fringe_Benefits_Percentages__c WHERE Fringe_Contract__r.Id IN: fringecontracts.keySet() ]){
                                                                  
            ifbpMap.put(ifbp.Individual_Fringe_Benefit_Account__c+ifbp.Fringe_Contract__r.Id, ifbp.Percentage__c);
            
        }
        for(Individual_Fringe_Benfit__c ifb : [SELECT Id, Name,Month_and_Year__c, Employee__r.Fringe_Contract_Type__r.Id,Employee__r.Id,Individual_Fringe_Benfit__c.Employee__r.Name, Individual_Fringe_Benfit__c.Employee__r.Monthly_Fringe_Amount_Allocated__c FROM Individual_Fringe_Benfit__c WHERE Individual_Fringe_Benfit__c.Employee__r.Id =: fringeResults.keySet()]){
            if(ifb != null){
                for(String combinedvalue : ifbpMap.keySet()){
                    if(combinedvalue.containsIgnoreCase('HRA'+ifb.Employee__r.Fringe_Contract_Type__r.Id)){
                        ifb.HRA__c = (ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c *30/100) < 0 ? 0:ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*30/100;
                        ifb.Individual_Premium_Reserve__c = (ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c - ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*30/100) < 0? 0:ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c - ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*30/100;
                        ifbList.add(ifb);
                    }else if(combinedvalue.containsIgnoreCase('Insurance'+ifb.Employee__r.Fringe_Contract_Type__r.Id)){
                        ifb.Insurance__c = (ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c *30/100) < 0 ? 0:ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*30/100;
                        ifb.Individual_Premium_Reserve__c = (ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c - ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*30/100) < 0? 0:ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c - ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*30/100;
                        ifbList.add(ifb);
                    }else if(combinedvalue.containsIgnoreCase('Medical'+ifb.Employee__r.Fringe_Contract_Type__r.Id)){
                        ifb.Medical__c = (ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c *30/100) < 0 ? 0:ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*30/100;
                        ifb.Individual_Premium_Reserve__c = (ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c - ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*30/100) < 0? 0:ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c - ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*30/100;
                        ifbList.add(ifb);
                    }else if(combinedvalue.containsIgnoreCase('Retirement'+ifb.Employee__r.Fringe_Contract_Type__r.Id)){
                        ifb.Retirement__c = (ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c *30/100) < 0 ? 0:ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*30/100;
                        ifb.Individual_Premium_Reserve__c = (ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c - ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*30/100) < 0? 0:ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c - ifb.Employee__r.Monthly_Fringe_Amount_Allocated__c*30/100;
                        ifbList.add(ifb);//Retirement	
                    }
                    
                }
                count++;
            }
        }
        if(count <= actualCount){
            for(String emplId : fringeResults.keySet()){
                Individual_Fringe_Benfit__c ifb = new Individual_Fringe_Benfit__c();
                
            }
        }
    }
    
    public static void upsertToOtherEmployee(Map<String, Decimal> monthlyHoursMap){
        String fringeMethod;
        List<Employee__c> empList = new List<Employee__c>();
        List<String> empnames = new List<String>();
        Map<String, Decimal> monthlyHoursregMap = new Map<String, Decimal>();
        Map<String, Employee__c> monthlyOrWeeklyHoursMap = new Map<String, Employee__c>();
        Map<String, String> monthAndYear = new Map<String, String>();
        Map<String, String> customsettingFields = new Map<String, String>();
        Map<String, decimal> fringecontractBH = new  Map<String, decimal>();
        Map<String, Object> fringecontractobj = new  Map<String, Object>();
        Map<String, decimal> fringeEmpCob = new  Map<String, decimal>();
        List<Hours_Bank_Account__c> hbaList = new List<Hours_Bank_Account__c>();
        for(Monthly_Hours__c mh : [SELECT Id, Name, EE__c,Month_and_Year__c, Curr_Reg_Hrs_NYC__c FROM Monthly_Hours__c WHERE EE__c In: monthlyHoursMap.keySet()]){
            monthlyHoursregMap.put(mh.EE__c, mh.Curr_Reg_Hrs_NYC__c);
            monthAndYear.put(mh.EE__c,mh.Month_and_Year__c);
        }
        for(Employee__c emp: [SELECT Id, Name,Actual_Monthly_Hours__c,Monthly_Fringe_Amount_Allocated__c,Fringe_Rate_lookup__c, Curr_Reg_Hrs_NYC__c,Monthly_Cost_Of_Benefits__c, Fringe_Contract_Type__r.Capping_Hours__c, Fringe_Contract_Type__r.Fringe_Allocation_Method__c, Roll_Back_Hours__c FROM Employee__c WHERE Name IN: monthlyHoursMap.keySet() AND Fringe_Contract_Type__r.Fringe_Allocation_Method__c ='Individual Fringe Benefit Accounts']){
            if(monthlyHoursMap.containsKey(emp.Name)){
                fringeMethod = emp.Fringe_Contract_Type__r.Fringe_Allocation_Method__c;
                emp.Curr_Reg_Hrs_NYC__c = monthlyHoursMap.get(emp.Name) > emp.Fringe_Contract_Type__r.Capping_Hours__c ? emp.Fringe_Contract_Type__r.Capping_Hours__c : monthlyHoursMap.get(emp.Name);
                emp.Actual_Monthly_Hours__c = monthlyHoursregMap.get(emp.Name);
                empnames.add(emp.Name);
                fringecontractobj.put(emp.Fringe_Contract_Type__r.Id,emp.Fringe_Contract_Type__r);
                //emp.Roll_Back_Hours__c = monthlyHoursregMap.get(emp.Name) > emp.Fringe_Contract_Type__r.Capping_Hours__c ? (monthlyHoursMap.get(emp.Name) - emp.Fringe_Contract_Type__r.Capping_Hours__c)+emp.Roll_Back_Hours__c  : emp.Roll_Back_Hours__c + (monthlyHoursMap.get(emp.Name) - emp.Fringe_Contract_Type__r.Capping_Hours__c);
                monthlyOrWeeklyHoursMap.put(emp.Name, emp);
                customsettingFields.put(emp.Fringe_Contract_Type__r.Id, Dynamic_Fields__c.getValues(emp.Fringe_Contract_Type__r.Id).List_of_Fields__c);
                fringeEmpCob.put(emp.Name,emp.Monthly_Fringe_Amount_Allocated__c);
                fringecontractBH.put(emp.Name, emp.Fringe_Contract_Type__r.Capping_Hours__c);
                empList.add(emp);
            }
        }   
        
        try{
            if(empList.size() > 0){
                Upsert empList;
            }
        }catch(Exception e){System.debug('---exception---'+e.getMessage());}
        
            Integer count = 1;
            Integer actualcount = monthlyHoursMap.size();
            List<Individual_Fringe_Benfit__c> ifbList = new List<Individual_Fringe_Benfit__c>();
            System.debug('---'+monthlyOrWeeklyHoursMap);
            Map<Integer,String> monthNameMap = new Map<Integer, String>{1 =>'January', 2=>'February', 3=>'March', 4=>'April', 5=>'May',6=>'June', 7=>'July', 8=>'August', 9=>'September',10=>'October',11=>'November', 12=>'December'};
                
            System.debug('----'+monthNameMap.get(System.now().month()));
            System.debug('-----'+System.now().year());
        String SobjectApiName = 'Individual_Fringe_Benfit__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        
        String apiNames;
        //List<Dynamic_Fields__c> ls = [SELECT Name, List_of_Fields__c];
        for(String apiName : fieldMap.keyset())
        {
            if(!apiName.equalsIgnoreCase('ownerid')){
                if(fieldMap.get(apiName).getDescribe().isUpdateable()) {
                    if(String.isBlank(apiNames)){
                        apiNames = apiName;
                    }else{
                        apiNames += ','+apiName;
                    }//apiNames.add(apiName);
                }
            }
        }
		String monthyear = monthNameMap.get(System.now().month())+' '+System.now().year();
        String queryString = 'SELECT '+apiNames+', Employee__r.Name, Employee__r.Fringe_Contract_Type__c FROM Individual_Fringe_Benfit__c WHERE Month_and_Year__c =: monthyear  AND Employee__r.Name IN:  empnames';
        System.debug('-----'+queryString);
        List<sObject> cusSetRec = Database.query(queryString);
        //Integer count = fieldsList.split(',').size();
        List<sObject> upsertListObj = new List<sObject>();
        for(sObject fc : cusSetRec){
            Individual_Fringe_Benfit__c ui = (Individual_Fringe_Benfit__c)fc;
            String emptocontract = ui.Employee__r.Fringe_Contract_Type__c;
            //emptocontract = jsonMap.get('Fringe_Contract_Type__c');
            if(!String.isBlank(emptocontract)){
                String fieldsList = String.valueOf(customsettingFields.get(emptocontract));
                for(String fieldapi : fieldsList.split(',')){
                    if(!String.isBlank(fieldapi)){
                        fc.put(fieldapi,monthlyHoursMap.get(fieldapi));
                    }
                }
                
                upsertListObj.add(fc);
            }
            count++;
        }
            if(count <= actualcount){
                for(Employee__c emp: [SELECT Id, Name,Actual_Monthly_Hours__c,Monthly_Fringe_Amount_Allocated__c,Fringe_Rate_lookup__c, Curr_Reg_Hrs_NYC__c,Monthly_Cost_Of_Benefits__c, Fringe_Contract_Type__r.Capping_Hours__c, Fringe_Contract_Type__r.Fringe_Allocation_Method__c, Roll_Back_Hours__c FROM Employee__c WHERE Name IN: monthlyHoursMap.keySet() AND Fringe_Contract_Type__r.Fringe_Allocation_Method__c ='Individual Fringe Benefit Accounts']){
                    if(emp != null){
                        Individual_Fringe_Benfit__c ifb = new Individual_Fringe_Benfit__c();
                        //Employee__c emp = monthlyOrWeeklyHoursMap.get(empId);
                        ifb.Employee__c = emp.Id;
                        ifb.Month_and_Year__c = monthNameMap.get(System.now().month())+' '+System.now().year();
                        ifb.HRA__c =  (emp.Monthly_Fringe_Amount_Allocated__c)*30/100;
                        ifb.Individual_Premium_Reserve__c = emp.Monthly_Fringe_Amount_Allocated__c - emp.Monthly_Fringe_Amount_Allocated__c*30/100;
                        ifbList.add(ifb);
                        count++;
                    }
                }
            }
            
            try{
                if(ifbList.size() > 0){
                    Upsert ifbList;
                }
            }catch(Exception e){System.debug('---exception---'+e.getMessage());}
    }
    
    public static void upsertToOtherEmployeeMethods(Map<String, Decimal> monthlyHoursMap){
        List<Employee__c> empList = new List<Employee__c>();
        Map<String, Decimal> monthlyHoursregMap = new Map<String, Decimal>();
        Map<String, Employee__c> monthlyOrWeeklyHoursMap = new Map<String, Employee__c>();
        Map<String, String> monthAndYear = new Map<String, String>();
        Map<String, decimal> fringecontractBH = new  Map<String, decimal>();
        Map<String, decimal> fringeEmpCob = new  Map<String, decimal>();
        List<Hours_Bank_Account__c> hbaList = new List<Hours_Bank_Account__c>();
        for(Monthly_Hours__c mh : [SELECT Id, Name, EE__c,Month_and_Year__c, Curr_Reg_Hrs_NYC__c FROM Monthly_Hours__c WHERE EE__c In: monthlyHoursMap.keySet()]){
            monthlyHoursregMap.put(mh.EE__c, mh.Curr_Reg_Hrs_NYC__c);
            monthAndYear.put(mh.EE__c,mh.Month_and_Year__c);
        }
        for(Employee__c emp: [SELECT Id, Name,Actual_Monthly_Hours__c,Fringe_Rate_lookup__c, Curr_Reg_Hrs_NYC__c,Monthly_Cost_Of_Benefits__c, Fringe_Contract_Type__r.Capping_Hours__c, Fringe_Contract_Type__r.Fringe_Allocation_Method__c, Roll_Back_Hours__c FROM Employee__c WHERE Name IN: monthlyHoursMap.keySet()]){
            if(monthlyHoursMap.containsKey(emp.Name)){
                emp.Curr_Reg_Hrs_NYC__c = monthlyHoursMap.get(emp.Name) > emp.Fringe_Contract_Type__r.Capping_Hours__c ? emp.Fringe_Contract_Type__r.Capping_Hours__c : monthlyHoursMap.get(emp.Name);
                emp.Actual_Monthly_Hours__c = monthlyHoursregMap.get(emp.Name);
                emp.Roll_Back_Hours__c = monthlyHoursregMap.get(emp.Name) > emp.Fringe_Contract_Type__r.Capping_Hours__c ? (monthlyHoursMap.get(emp.Name) - emp.Fringe_Contract_Type__r.Capping_Hours__c)+emp.Roll_Back_Hours__c  : emp.Roll_Back_Hours__c + (monthlyHoursMap.get(emp.Name) - emp.Fringe_Contract_Type__r.Capping_Hours__c);
                monthlyOrWeeklyHoursMap.put(emp.Name, emp);
                fringeEmpCob.put(emp.Name, emp.Monthly_Cost_Of_Benefits__c);
                fringecontractBH.put(emp.Name, emp.Fringe_Contract_Type__r.Capping_Hours__c);
                empList.add(emp);
            }
        }   
        
        try{
            if(empList.size() > 0){
                Upsert empList;
            }
        }catch(Exception e){System.debug('---exception---'+e.getMessage());}
    }*/
}