global class EmployeeUpdateOnFringeRateChange implements Database.batchable<Id>,Database.Stateful,Database.AllowsCallouts{ 
    List<Id> recordList = new List<Id>();
    List<Id> contractIdList = new List<Id>(); 
    List<String> monthAndYearList = new List<String>();
    Map<Id,Id> pbMap = new Map<Id,Id>();
    global EmployeeUpdateOnFringeRateChange(List<Id> recordIdList){
        
        for(Pricebook2 pblist : [SELECT Id, Name,Govt_Contractors__c,Fringe_Rate_Effactive_Date__c, Govt_Contractors__r.Name,LastModifiedDate,Fringe_Contract__c, Is_Job_Classification__c FROM Pricebook2 WHERE ID IN: recordIdList ]){
            Date dt = pblist.Fringe_Rate_Effactive_Date__c != null? pblist.Fringe_Rate_Effactive_Date__c:null;
            DateTime lastd = pblist.LastModifiedDate;
            contractIdList.add(pblist.Fringe_Contract__c);
            pbMap.put(pblist.Fringe_Contract__c, pblist.Id);
            Integer retromonth,year;
            if(dt != null){
                retromonth = dt.month();
                year = dt.year();   
            }
            Integer lastmon = lastd.month();
            if(retromonth < lastmon){
                for(Integer i = retromonth; i<= lastmon-1;i++){
                    System.debug('====='+i);
                    Map<Integer,String> monthNameMap = new Map<Integer, String>{1 =>'January', 2=>'February', 3=>'March', 4=>'April', 5=>'May',6=>'June', 7=>'July', 8=>'August', 9=>'September',10=>'October',11=>'November', 12=>'December'};
                        String monthyear = monthNameMap.get(i)+' '+year;
                    monthAndYearList.add(monthyear);
                }
            }
        }
        /*for(Individual_Fringe_Benfit__c ifb :[SELECT Id, Name, Employee__c, Employee__r.Fringe_Contract_Type__c FROM Individual_Fringe_Benfit__c WHERE Employee__r.Fringe_Contract_Type__c IN: contractIdList AND Month_and_Year__c IN: monthAndYearList AND Retro_Active__c = False AND Employee__r.Fringe_Contract_Type__c != null]){
            //recordList.add(ifb.Id);
            
        }*/
        for(Monthly_Hours__c mon : [SELECT Id, Name, EE__c,Job_Classification_Zip_code__c,Month_and_Year__c,Cost_Of_Benefit__c,Job_Classification__c,Zip_Code__c,Fringe_Contract__c, Coverage_Tier__c,Curr_Reg_Hrs_NYC__c FROM Monthly_Hours__c WHERE Month_and_Year__c IN: monthAndYearList AND Fringe_Contract__c IN: contractIdList]){
            recordList.add(mon.Id);
        }
    }
    global List<Id> start(Database.BatchableContext BC){
        return recordList;
    }
    
    global void execute(Database.BatchableContext BC, List<Id> recordList){
        if(recordList.size() > 0){       
            for(Id recordId : recordList){
                EmployeeMonthlyCalculationHandler.updateEmployeeFromMonthlyHours(recordId, ''); 
                //updateEmployeeFringeRate(recordId);
            }
        }
    }
    global void finish(Database.BatchableContext BC){
      
    }
    
    public static void updateEmployeeFringeRate(Id ifbID){
        Decimal fringeRate;
        try{
            Individual_Fringe_Benfit__c ifb = [SELECT Id, Name, Month_and_Year__c, Monthly_Cost_Of_Benefit__c, After_Retro_Hours_Adjustments__c, Curr_Reg_Hours_NYC__c, Monthly_Fringe_Obligation_Result__c, Fringe_Rate__c,Employee__c, Employee__r.Fringe_Contract_Type__c FROM Individual_Fringe_Benfit__c WHERE Id =: ifbID ];
            //contractRetroList.add(pblist.Id);
            if(ifb != null){
                if(ifb.Employee__r.Fringe_Contract_Type__c != null){
                    Employee__c emp = [SELECT Id,Monthly_Fringe_Contribution__c,Job_Classification__c,Name,Actual_Monthly_Hours__c,Fringe_Rate_lookup__c, Curr_Reg_Hrs_NYC__c,Monthly_Cost_Of_Benefits__c,Fringe_Contract_Type__r.Avg_Accounting_Rules__c,Roll_Back_Hours__c FROM Employee__c WHERE Id =: ifb.Employee__c];
                    if(emp.Job_Classification__c != null){
                        PricebookEntry pbe = [SELECT Id, Name, Pricebook2Id,Product2.Name, Product2Id, UnitPrice, IsActive, ProductCode FROM PricebookEntry WHERE Name =: emp.Job_Classification__c AND Pricebook2.Fringe_Contract__c =: ifb.Employee__r.Fringe_Contract_Type__c];
                        if(pbe != null){
                            fringeRate = pbe.UnitPrice;   
                            Individual_Fringe_Benfit__c ifbnew = new Individual_Fringe_Benfit__c();
                            ifbnew.Month_and_Year__c = ifb.Month_and_Year__c;
                            ifbnew.Employee__c = ifb.Employee__c;
                            ifbnew.Retro_Active__c = True;
                            ifbnew.Fringe_Rate__c = fringeRate;//pbMap
                            ifbnew.Curr_Reg_Hours_NYC__c = ifb.Curr_Reg_Hours_NYC__c;
                            ifbnew.Monthly_Fringe_Obligation_Result__c = ifb.Curr_Reg_Hours_NYC__c * fringeRate - ifb.Monthly_Cost_Of_Benefit__c;
                            ifbnew.Monthly_Cost_Of_Benefit__c = ifb.Monthly_Cost_Of_Benefit__c;
                            ifbnew.After_Retro_Hours_Adjustments__c = ifb.Monthly_Fringe_Obligation_Result__c != null ?ifb.Curr_Reg_Hours_NYC__c * fringeRate - ifb.Monthly_Cost_Of_Benefit__c: ifb.Monthly_Fringe_Obligation_Result__c - (ifb.Curr_Reg_Hours_NYC__c * fringeRate - ifb.Monthly_Cost_Of_Benefit__c) ;
                            
                            try{insert ifbnew;}catch(Exception e){e.getMessage();}
                        }
                    }
                }
                
                /*cusSetRec.put('Month_and_Year__c',Month_and_Year);
cusSetRec.put('Employee__c',emp1.Id);
cusSetRec.put('Retro_Active__c',True);//Retro_Active__c
cusSetRec.put('Fringe_Rate__c',emp1.Fringe_Rate_lookup__c);
cusSetRec.put('Curr_Reg_Hours_NYC__c',emp1.Curr_Reg_Hrs_NYC__c);
cusSetRec.put('Monthly_Fringe_Obligation_Result__c',emp1.Monthly_fringe_obligtion_result__c);
cusSetRec.put('Monthly_Cost_Of_Benefit__c',emp1.Monthly_Cost_Of_Benefits__c);
cusSetRec.put('After_Retro_Hours_Adjustments__c',emp.Month_and_Year__c == Month_and_Year ? emp1.Monthly_fringe_obligtion_result__c - obligationResult:0);*/
            }
        }catch(Exception e){e.getMessage();}
    }
}