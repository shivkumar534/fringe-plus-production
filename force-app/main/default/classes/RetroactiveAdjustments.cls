// Author : Diwakar Reddy Vadde
// Date  : 28th April 2021

public class RetroactiveAdjustments {
   
    @InvocableMethod
    public static void fringeRate(List<Id> fcIds) {
        System.debug('This is the calss : Retroactive adjustment');
        System.debug('This is the retroactive adjustment method');
        // Collecting employee names who are realted to fringe Contract 
        List<string> empLst = new List<string>();
        // Capturing monhthly hours ids which are related to Employee
       Id monthlyHourId ;
        
        for(Fringe_Contract__c fclst : [Select id,name, Fringe_Rate__c,(select id, name from Employees__r) from Fringe_Contract__c where id in : fcIds]) {
            for(Employee__c emp : fclst.Employees__r){
                empLst.add(emp.name);
            }  
        }
        for(Monthly_Hours__c mh : [SELECT Id, Name, EE__c,Month_and_Year__c, Curr_Reg_Hrs_NYC__c FROM Monthly_Hours__c WHERE EE__c in : empLst]) {
            monthlyHourId = mh.Id;
            System.debug('Monthly hours ids is in for loop : '+monthlyHourId);
        }
        If(monthlyHourId != null) {
            System.debug('Monthly hours ids is in If condition : '+monthlyHourId);
            //EmployeeMonthlyCalculationHandler.updateEmployeeFromMonthlyHours(monthlyHourId);
        }
    }          
        
}