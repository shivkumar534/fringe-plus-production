//author Tarun & Diwakar//

global with sharing class  Account_Picklist {
    @AuraEnabled
    global static List<String> getData(){
        System.debug('get data');
        List<String> options= new List<String>();
        for(Govt_Contractor__c acc:[select id,name from Govt_Contractor__c]){
            options.add(acc.name);
        }
        return options;
    }
    
    @AuraEnabled
    global static List<String> getContract(String accId){
        system.debug('get contract');
        List<String> options= new List<String>();
        for(Fringe_Contract__c contract:[select id,name,Govt_Contractor__r.Name from Fringe_Contract__c where Govt_Contractor__r.Name =: accId]){
            options.add(contract.name);
        }
        return options;
    }
      
    @AuraEnabled
    global static List<String> getEmpYears(){
        system.debug('get emp years');
        List<String> years= new List<String>();
        for(Employee_years__c empyears:[select Id,name from Employee_years__c order by name asc]){
            years.add(empyears.name);
        }
        return years;
    }
    
    @AuraEnabled
    global static Map<String, String> getEmployeeData(String AccountName,String EmployeeId){
        Employee__c EmpObj;
        Map<String, String> empMap=new Map<String, String>();
        System.debug('get employee dta');
        system.debug('account name :'+AccountName);
        system.debug('employee id :'+EmployeeId);
        try{
            EmpObj = [select id,name,Govt_Contractor__r.name,Govt_Contractor__c,Gender__c,Unique_Identifier__c,DOB__c,Curr_Reg_Hrs_NYC__c,Monthly_Cost_Of_Benefits__c,Monthly_Fringe_Contribution__c,First_Name__c,Middle_Name__c,Last_Name__c,Fringe_Rate_lookup__c,Fringe_Contract_Type__r.Price_Book__c,Coverage_Tier__c,Coverage_Effective_From_Date__c,Coverage_Effective_To_Date__c,Health_Insurance_Plan__c,Dental_Insurance_Plan__c,Vision_Insurance_Plan__c,Plan_1__c,Plan_2__c,Plan_3__c,Plan_4__c,Plan_5__c,Benefits_Effective_Date__c,Month_and_Year__c,Short_Term_Disability_Insurance__c,Long_Term_Disability_Insurance__c,Life_Insurance_Plan__c from Employee__c where Govt_Contractor__r.name=:AccountName and Unique_Identifier__c=:EmployeeId];
            System.debug('pricebook id'+EmpObj.Fringe_Contract_Type__r.Price_Book__c);
            System.debug('employee id :'+EmpObj.Id);
            empMap.put('Id', EmpObj.Id);
            empMap.put('Name', EmpObj.name);
            empMap.put('UniqueId', EmpObj.Unique_Identifier__c);
            empMap.put('DOB__c', String.valueof(EmpObj.DOB__c));//DOB__c
            empMap.put('Gender__c', EmpObj.Gender__c);//Gender__c
            empMap.put('FirstName', String.valueof(EmpObj.First_Name__c));
            empMap.put('MiddleName', String.valueof(EmpObj.Middle_Name__c));
            empMap.put('LastName', String.valueof(EmpObj.Last_Name__c));
            //empMap.put('Curr_Reg_Hrs_NYC__c', String.valueof(EmpObj.Curr_Reg_Hrs_NYC__c));//Curr_Reg_Hrs_NYC__c
            empMap.put('Coveragetier', EmpObj.Coverage_Tier__c);
            
            //-----------------------BenefitMonth------------------------
            Integer BenefitMonth = EmpObj.Benefits_Effective_Date__c.Month();//CASE(MONTH( DATEVALUE( EmpObj.Benefits_Effective_Date__c )),
            System.debug('Benefit Month :'+BenefitMonth);
            empmap.put('Benefits_Effective_Date__c',String.valueOf(BenefitMonth));
            
            //----------------Health Insurance-------------------------------------
            System.debug('-----------'+EmpObj.Health_Insurance_Plan__c);
            empMap.put('Health_Insurance_Plan__c', EmpObj.Health_Insurance_Plan__c);
           
            //-----------------Dental Insurance------------------------------------------
            System.debug('-----------'+EmpObj.Dental_Insurance_Plan__c);
            empMap.put('Dental_Insurance_Plan__c', EmpObj.Dental_Insurance_Plan__c);
        
            //--------------------Vision Insurance------------------------------------
            System.debug('-----------'+EmpObj.Vision_Insurance_Plan__c);
            empMap.put('Vision_Insurance_Plan__c', EmpObj.Vision_Insurance_Plan__c);
            
            //-----------------------Life Insurance Plan-------------------------------
            System.debug('----'+EmpObj.Life_Insurance_Plan__c	);
            empMap.put('Life_Insurance_Plan__c', EmpObj.Life_Insurance_Plan__c	);
            
            //-------------------Short Term Insurance Plan----------------------------
            System.debug('----'+EmpObj.Short_Term_Disability_Insurance__c);
            empMap.put('Short_Term_Disability_Insurance__c', EmpObj.Short_Term_Disability_Insurance__c);
            
            //---------------------Long Term Insurance Plan----------------------------
            System.debug('----'+EmpObj.Long_Term_Disability_Insurance__c);
            empMap.put('Long_Term_Disability_Insurance__c', EmpObj.Long_Term_Disability_Insurance__c);
            
           
            
            //----------------Monthly_Cost_Of_Benefits__c-----------------
            empMap.put('Monthly_Cost_Of_Benefits__c',String.valueof(EmpObj.Monthly_Cost_Of_Benefits__c));
            System.debug('monthly cost of benefits :'+EmpObj.Monthly_Cost_Of_Benefits__c);
            for(PricebookEntry pbe : [SELECT Id, Name, ProductCode, UnitPrice, Pricebook2Id, Product2.name,UseStandardPrice from PricebookEntry where Pricebook2Id =: EmpObj.Fringe_Contract_Type__r.Price_Book__c ]) {
                if(EmpObj.Health_Insurance_Plan__c == pbe.ProductCode && EmpObj.Coverage_Tier__c == pbe.Name){
                    System.debug('-----'+EmpObj.Coverage_Tier__c +'      '+ pbe.Name);
               		empMap.put('Health_Insurance_Plan_Value',String.valueOf(pbe.UnitPrice));
                    System.debug('Health_Insurance_Plan_Value :'+pbe.UnitPrice);
          		 }
           		if(EmpObj.Dental_Insurance_Plan__c == pbe.ProductCode && EmpObj.Coverage_Tier__c == pbe.Name){
               		empMap.put('Dental_Insurance_Plan_Value',String.valueOf(pbe.UnitPrice));
                    System.debug('Dental_Insurance_Plan_Value :'+pbe.UnitPrice);
           			}
                if(EmpObj.Vision_Insurance_Plan__c  == pbe.ProductCode && EmpObj.Coverage_Tier__c == pbe.Name){
              		empMap.put('Vision_Insurance_Plan_Value',String.valueOf(pbe.UnitPrice));
                    System.debug('Vision_Insurance_Plan_Value :'+pbe.UnitPrice);
          		}
                 if(EmpObj.Life_Insurance_Plan__c  == pbe.ProductCode && EmpObj.Coverage_Tier__c == pbe.Name){
              		empMap.put('Life_Insurance_Plan_Value',String.valueOf(pbe.UnitPrice));
                    System.debug('Life_Insurance_Plan_Value :'+pbe.UnitPrice);
          		}
                if(EmpObj.Short_Term_Disability_Insurance__c == pbe.ProductCode && EmpObj.Coverage_Tier__c == pbe.Name){
                    empMap.put('Short_Term_Disability_Insurance__c',String.valueOf(pbe.UnitPrice));
                }
                if(EmpObj.Long_Term_Disability_Insurance__c == pbe.ProductCode && EmpObj.Coverage_Tier__c == pbe.Name){
                    empMap.put('Long_Term_Disability_Insurance__c',String.valueOf(pbe.UnitPrice));
                }
            }
            
            empMap.put('Plan_1__c', EmpObj.Plan_1__c);
            empMap.put('Plan_2__c', EmpObj.Plan_2__c);
            empMap.put('Plan_3__c', EmpObj.Plan_3__c);
            empMap.put('Plan_4__c', EmpObj.Plan_4__c);
            empMap.put('Plan_5__c', EmpObj.Plan_5__c);
            //String MonthlyFC = String.valueOf(EmpObj.Monthly_Fringe_Contribution__c);
            //empMap.put('MonthlyFC', MonthlyFC);
            //String FringeRate = String.valueOf(EmpObj.Fringe_Rate_lookup__c);
           // empMap.put('FringeRate',FringeRate);
            String CoverageEffectivefromDate = String.valueOf(EmpObj.Coverage_Effective_From_Date__c);
            empMap.put('CoverageEffectivefromDate',CoverageEffectivefromDate);
            String CoverageEffectivetoDate = String.valueOf(EmpObj.Coverage_Effective_To_Date__c);
            empMap.put('CoverageEffectivetoDate',CoverageEffectivetoDate);
            //empMap.put('MonthlyCOB', EmpObj.Monthly_Cost_Of_Benefits__c);
        }
        catch(Exception e){e.getMessage();}        
        return empMap;
    }

    
    
    @AuraEnabled
    global static Map<String, String> getEmployeeDatabyMonthYear(String EmployeeId,String Monthyear){
        Employee__c EmpObj;
        Map<String, String> empMap=new Map<String, String>();
        System.debug('getEmployeeDatabyMonthYear---'+Monthyear);
        system.debug('employee id :'+EmployeeId);
        try{
            EmpObj = [select id,name,Govt_Contractor__r.name,Govt_Contractor__c,Gender__c,Unique_Identifier__c,DOB__c,Curr_Reg_Hrs_NYC__c,Monthly_Cost_Of_Benefits__c,Monthly_Fringe_Contribution__c,First_Name__c,Middle_Name__c,Last_Name__c,Fringe_Rate_lookup__c,Fringe_Contract_Type__r.Price_Book__c,Coverage_Tier__c,Coverage_Effective_From_Date__c,Coverage_Effective_To_Date__c,Health_Insurance_Plan__c,Dental_Insurance_Plan__c,Vision_Insurance_Plan__c,Life_Insurance_Plan__c,Plan_1__c,Plan_2__c,Plan_3__c,Plan_4__c,Plan_5__c,Benefits_Effective_Date__c,Month_and_Year__c,Long_Term_Disability_Insurance__c,Short_Term_Disability_Insurance__c from Employee__c where Id =:EmployeeId];
                 // and  Benefits_Effective_Date__c >=:Monthyear and Termination_Date__c <=:Monthyear
            System.debug('pricebook id'+EmpObj.Fringe_Contract_Type__r.Price_Book__c);
            System.debug('employee id :'+EmpObj.Id);
            
            empMap.put('Id', EmpObj.Id);
            empMap.put('Name', EmpObj.name);
            empMap.put('UniqueId', EmpObj.Unique_Identifier__c);
            empMap.put('DOB__c', String.valueof(EmpObj.DOB__c));//DOB__c
            empMap.put('Gender__c', EmpObj.Gender__c);//Gender__c
            empMap.put('FirstName', String.valueof(EmpObj.First_Name__c));
            empMap.put('MiddleName', String.valueof(EmpObj.Middle_Name__c));
            empMap.put('LastName', String.valueof(EmpObj.Last_Name__c));
            //empMap.put('Curr_Reg_Hrs_NYC__c', String.valueof(EmpObj.Curr_Reg_Hrs_NYC__c));//Curr_Reg_Hrs_NYC__c
            empMap.put('Coveragetier', EmpObj.Coverage_Tier__c);
            //-----------------------BenefitMonth------------------------
            Integer BenefitMonth = EmpObj.Benefits_Effective_Date__c.Month();//CASE(MONTH( DATEVALUE( EmpObj.Benefits_Effective_Date__c )),
            System.debug('Benefit Month :'+BenefitMonth);
            empmap.put('Benefits_Effective_Date__c',String.valueOf(BenefitMonth));
            
            //----------------Health Insurance-------------------------------------
            System.debug('-----------'+EmpObj.Health_Insurance_Plan__c);
            empMap.put('Health_Insurance_Plan__c', EmpObj.Health_Insurance_Plan__c);
            //empMap.put('Health_Insurance_Plan_Value','0');
           
            //-----------------Dental Insurance------------------------------------------
            System.debug('-----------'+EmpObj.Dental_Insurance_Plan__c);
            empMap.put('Dental_Insurance_Plan__c', EmpObj.Dental_Insurance_Plan__c);
            //empMap.put('Dental_Insurance_Plan_Value','0');
          
            //--------------------Vision Insurance------------------------------------
            System.debug('-----------'+EmpObj.Vision_Insurance_Plan__c);
            empMap.put('Vision_Insurance_Plan__c', EmpObj.Vision_Insurance_Plan__c);
            
            //------------------Life Insurance Plan-------------------------------------
            System.debug('-----------'+EmpObj.Life_Insurance_Plan__c);
            empMap.put('Life_Insurance_Plan__c', EmpObj.Life_Insurance_Plan__c);
          
            //-------------------Short Term Disability Insurance----------------------
            System.debug('----------'+EmpObj.Short_Term_Disability_Insurance__c);
            empMap.put('Short_Term_Disability_Insurance__c', EmpObj.Short_Term_Disability_Insurance__c);
           
            //-------------------Long Term Disability Insurance--------------------------
            System.debug('---------'+EmpObj.Long_Term_Disability_Insurance__c);
            empMap.put('Long_Term_Disability_Insurance__c', EmpObj.Long_Term_Disability_Insurance__c);
            //empMap.put('Vision_Insurance_Plan_Value','0');
            
            //----------------Monthly_Cost_Of_Benefits__c-----------------
            empMap.put('Monthly_Cost_Of_Benefits__c',String.valueof(EmpObj.Monthly_Cost_Of_Benefits__c));
            System.debug('monthly cost of benefits :'+EmpObj.Monthly_Cost_Of_Benefits__c);
            for(PricebookEntry pbe : [SELECT Id, Name, ProductCode, UnitPrice, Pricebook2Id, Product2.name,UseStandardPrice from PricebookEntry where Pricebook2Id =: EmpObj.Fringe_Contract_Type__r.Price_Book__c ]) {
                if(EmpObj.Health_Insurance_Plan__c == pbe.ProductCode && EmpObj.Coverage_Tier__c == pbe.Name){
               		empMap.put('Health_Insurance_Plan_Value',String.valueOf(pbe.UnitPrice));
                    System.debug('Health_Insurance_Plan_Value :'+pbe.UnitPrice);
          		 }
           		if(EmpObj.Dental_Insurance_Plan__c == pbe.ProductCode && EmpObj.Coverage_Tier__c == pbe.Name){
               		empMap.put('Dental_Insurance_Plan_Value',String.valueOf(pbe.UnitPrice));
                    System.debug('Dental_Insurance_Plan_Value :'+pbe.UnitPrice);
           			}
                if(EmpObj.Vision_Insurance_Plan__c  == pbe.ProductCode && EmpObj.Coverage_Tier__c == pbe.Name){
              		empMap.put('Vision_Insurance_Plan_Value',String.valueOf(pbe.UnitPrice));
                    System.debug('Vision_Insurance_Plan_Value :'+pbe.UnitPrice);
          		}
                 if(EmpObj.Life_Insurance_Plan__c  == pbe.ProductCode && EmpObj.Coverage_Tier__c == pbe.Name){
              		empMap.put('Life_Insurance_Plan_Value',String.valueOf(pbe.UnitPrice));
                    System.debug('Life_Insurance_Plan_Value :'+pbe.UnitPrice);
          		}
                if(EmpObj.Short_Term_Disability_Insurance__c  == pbe.ProductCode && EmpObj.Coverage_Tier__c == pbe.Name){
              		empMap.put('Short_Term_Disability_Insurance_Value',String.valueOf(pbe.UnitPrice));
                    System.debug('Short_Term_Disability_Insurance_Value :'+pbe.UnitPrice);
          		}
                if(EmpObj.Long_Term_Disability_Insurance__c == pbe.ProductCode && EmpObj.Coverage_Tier__c == pbe.Name){
              		empMap.put('Long_Term_Disability_Insurance_Value',String.valueOf(pbe.UnitPrice));
                    System.debug('Long_Term_Disability_Insurance_Value :'+pbe.UnitPrice);
          		}
            }
            for(Custom_Links__c cl : [SELECT Id, Name, Type__c, Links__c FROM Custom_Links__c WHERE Name =: EmpObj.Govt_Contractor__c]){
               String types = cl.Type__c;
                if(types.containsIgnoreCase('HRA')){
                    empMap.put('HRA_Links',cl.Links__c);
                } else if(types.containsIgnoreCase('Retirement')){
                    empMap.put('Retirement_Links',cl.Links__c);
                } else if(types.containsIgnoreCase('Paid Leave')){
                    empMap.put('PaidLeave_Links',cl.Links__c);
                } else{
                    empMap.put('IPRA_Links',cl.Links__c);
                }
            }
            empMap.put('Plan_1__c', EmpObj.Plan_1__c);
            empMap.put('Plan_2__c', EmpObj.Plan_2__c);
            empMap.put('Plan_3__c', EmpObj.Plan_3__c);
            empMap.put('Plan_4__c', EmpObj.Plan_4__c);
            empMap.put('Plan_5__c', EmpObj.Plan_5__c);
            //String MonthlyFC = String.valueOf(EmpObj.Monthly_Fringe_Contribution__c);
            //empMap.put('MonthlyFC', MonthlyFC);
            //String FringeRate = String.valueOf(EmpObj.Fringe_Rate_lookup__c);
            //empMap.put('FringeRate',FringeRate);
            String CoverageEffectivefromDate = String.valueOf(EmpObj.Coverage_Effective_From_Date__c);
            empMap.put('CoverageEffectivefromDate',CoverageEffectivefromDate);
            String CoverageEffectivetoDate = String.valueOf(EmpObj.Coverage_Effective_To_Date__c);
            empMap.put('CoverageEffectivetoDate',CoverageEffectivetoDate);
            //empMap.put('MonthlyCOB', EmpObj.Monthly_Cost_Of_Benefits__c);
            System.debug('-----'+EmployeeId+'-------'+Monthyear);
            try{
                Individual_Fringe_Benfit__c ifb = [SELECT Id, Name,Fringe_Rate__c, HRA__c,Benefit_Period_Month_and_Year__c,Individual_Premium_Reserve__c,Monthly_Withdraw_From_IPRA__c,Rolling_Individual_Premium_Reserve__c, Employee__c,Monthly_Fringe_Contribution__c, Curr_Reg_Hours_NYC__c, PaidLeave__c, Month_and_Year__c,Rolling_Hour_Bank_Account_Balance__c, Monthly_Fringe_Obligation_Result__c, After_Retro_Hours_Adjustments__c, Rolling_Hour_Bank_Account__c, Rolling_Balance_of_IPRA__c, PaidLeaveFringeReserveAccount__c, Retirement__c FROM Individual_Fringe_Benfit__c WHERE Employee__c =: EmployeeId and Benefit_Period_Month_and_Year__c	 =: Monthyear LIMIT 1];
                if(ifb != null){
                    empMap.put('IFBFringeRate',String.valueOf(ifb.Fringe_Rate__c != null ? ifb.Fringe_Rate__c : null));
                    empMap.put('Curr_Reg_Hrs_NYC__c',String.valueOf(ifb.Curr_Reg_Hours_NYC__c != null ? ifb.Curr_Reg_Hours_NYC__c : null));
                    empMap.put('FringeContribution',String.valueOf(ifb.Monthly_Fringe_Contribution__c != null ? ifb.Monthly_Fringe_Contribution__c : null));
                    empMap.put('ObligationResult',String.valueOf(ifb.Monthly_Fringe_Obligation_Result__c != null ? ifb.Monthly_Fringe_Obligation_Result__c : null));
                    empMap.put('PaidPeriod',String.valueOf(ifb.Month_and_Year__c != null ? ifb.Month_and_Year__c : null));
                    empMap.put('HRA',String.valueOf(ifb.HRA__c != null ? ifb.HRA__c : null));
                    empMap.put('PaidLeave',String.valueOf(ifb.PaidLeaveFringeReserveAccount__c != null ?ifb.PaidLeaveFringeReserveAccount__c : null));
                    empMap.put('Retirement',String.valueOf(ifb.Retirement__c != null ?  ifb.Retirement__c : null));
                    empMap.put('HourBankAccount',String.valueOf(ifb.Rolling_Hour_Bank_Account__c != null ?  ifb.Rolling_Hour_Bank_Account__c : null));//
                    empMap.put('PremiumAccount',String.valueOf(ifb.Rolling_Balance_of_IPRA__c != null ? ifb.Rolling_Balance_of_IPRA__c : null));
                    empMap.put('WithdrawIPRA',String.valueOf(ifb.Monthly_Withdraw_From_IPRA__c != null ? ifb.Monthly_Withdraw_From_IPRA__c : null));
                    empMap.put('MonthlyIPRA',String.valueOf(ifb.Individual_Premium_Reserve__c != null ? ifb.Individual_Premium_Reserve__c : null));
                }
            }catch(Exception e){e.getMessage();}
            
        }
        catch(Exception e){e.getMessage();}        
        return empMap;
    }

    
    
    /*@AuraEnabled
    global static Map<String, String> getIFB(String AccountName,String EmployeeId){
        Individual_Fringe_Benfit__c ifbObj;
        Map<String, String> ifbMap=new Map<String, String>();
try {
will get pricebook;

}
        try{
            ifbObj = [select id,name,Govt_Contractor__r.name,Govt_Contractor__c,Unique_Identifier__c from Individual_Fringe_Benfit__c where Govt_Contractor__r.name=:AccountName and Unique_Identifier__c=:EmployeeId];
            ifbMap.put('Id', ifbObj.Id);
            ifbMap.put('UniqueId', ifbObj.Unique_Identifier__c);
            ifbMap.put('MonthlyFC', ifbObj.Unique_Identifier__c);
        }
        catch(Exception e){e.getMessage();}        
        return ifbMap;
    }*/
    
    @AuraEnabled
    global static Map<String, String> getEmployeeDetail(String EmployeeId , String Monthyear){
        System.debug('employee id :'+EmployeeId);
        System.debug('date from portal :'+Monthyear);
        Map<String, String> Empdetails = new Map<String, String>();
        Id EmpId = Id.valueOf(EmployeeId);
        try{
           Individual_Fringe_Benfit__c ifb = [select id,name,Monthly_Fringe_Contribution__c,Monthly_Cost_Of_Benefit__c,Monthly_Withdraw_From_IPRA__c,Current_Balance_Of_Individual_Premium_Ac__c from Individual_Fringe_Benfit__c where Employee__r.Id =: EmployeeId and Benefit_Period_Month_and_Year__c =: Monthyear limit 1];           
           System.debug('query crossed in getEmployeeDetails :'+ifb);
           Empdetails.put('MonthlyFC1',String.valueOf(ifb.Monthly_Fringe_Contribution__c != null ? ifb.Monthly_Fringe_Contribution__c: 0.00));
           Empdetails.put('MonthlyCOB', String.valueOf(ifb.Monthly_Cost_Of_Benefit__c));
           //Empdetails.put('WithdrawIPRA',String.valueOf(ifb.Monthly_Withdraw_From_IPRA__c));
           //Empdetails.put('CurrentIPA',String.valueOf(ifb.Current_Balance_Of_Individual_Premium_Ac__c));
            
           }
        	catch(Exception e){e.getMessage();}        
        		system.debug('employee details based on benefit month :'+Empdetails);
             	return Empdetails;
            }
  }