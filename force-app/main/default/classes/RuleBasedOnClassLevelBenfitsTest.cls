@isTest
private class RuleBasedOnClassLevelBenfitsTest {
    
    
     @isTest
        static void myTestMethod1() {
            FringeBenefits__c fb = new FringeBenefits__c();
            fb.Name = 'HRA';
            insert fb;
        }
    @isTest
    static void myTestMethod() {
         //----------------Test Account Creation------------------------------
        Account acc = new Account();
        acc.name = 'testAcc1';
        insert acc;
        //------------------Fringe Allocation Rules---------------------------
        Fringe_Allocation_Rules__c far = new Fringe_Allocation_Rules__c();
        far.Name = 'classlevelBenefits';
        far.UnderspentCategory__c = 'Class Level Coverage';
        far.Class_I_From_Hours__c = 1;
        far.Class_I_To_Hours__c = 50;
        far.Class_II_From_Hours__c= 51;
        far.Class_II_To_Hours__c=100;
        far.Class_III_From_Hours__c=101;
        far.Class_III_To_Hours__c =150;
        far.Class_IV_From_Hours__c=151;
        far.Class_IV_To_Hours__c =200;
        far.Class_V_From_Hours__c=201;
        far.Class_V_To_Hours__c =250;
        insert far;
        //------------------------Test Fringe contract creation---------------
        Fringe_Contract__c fc = new Fringe_Contract__c();
        fc.Fringe_Allocation_Rules__c = far.id;
        fc.Contract_Name__c = 'testContract';
        fc.Is_Active__c = True; 
        insert fc;
        //-----------Test Employee creation---------------------------
        employee__C emp = new employee__c();
        emp.Name = 'testEmp'; 
        emp.Fringe_Contract_Type__c = fc.id;       
        insert emp;
        //----------------Test Monthly hours creation------------------
        Monthly_Hours__c mh = new Monthly_Hours__c();
        mh.Employee__c = emp.id;
        mh.EE__c = 'testEmp';
        mh.Curr_Reg_Hrs_NYC__c = 120;
        mh.Month_and_Year__c = 'January 2021';
        insert mh;
		
        RuleBasedOnClassLevelBenfits.benefits(mh.id);
         mh.Curr_Reg_Hrs_NYC__c = 20;
         //mh.Month_and_Year__c = 'January 2021';
         update mh;
        RuleBasedOnClassLevelBenfits.benefits(mh.id);
         mh.Curr_Reg_Hrs_NYC__c = 70;
         update mh;
        RuleBasedOnClassLevelBenfits.benefits(mh.id);
         mh.Curr_Reg_Hrs_NYC__c = 120;
         update mh;
        RuleBasedOnClassLevelBenfits.benefits(mh.id);
         mh.Curr_Reg_Hrs_NYC__c = 170;
         //mh.Month_and_Year__c = 'January 2021';
        update mh;
        RuleBasedOnClassLevelBenfits.benefits(mh.id);
         mh.Curr_Reg_Hrs_NYC__c = 220;
        update mh;
        
        employee__C emp1 = new employee__c();
        emp1.Name = 'testEmp1'; 
        emp1.Fringe_Contract_Type__c = fc.id;       
        insert emp1;
        
        Monthly_Hours__c mh1 = new Monthly_Hours__c();
        mh1.Employee__c = emp1.id;
        mh1.EE__c = 'testEmp1';
        mh1.Curr_Reg_Hrs_NYC__c = 20;
        mh1.Month_and_Year__c = 'January 2021';
        insert mh1;
        RuleBasedOnClassLevelBenfits.benefits(mh1.id);
        
        employee__C emp2 = new employee__c();
        emp2.Name = 'testEmp2'; 
        emp2.Fringe_Contract_Type__c = fc.id;       
        insert emp2;
        Monthly_Hours__c mh2 = new Monthly_Hours__c();
        mh2.Employee__c = emp2.id;
        mh2.EE__c = 'testEmp2';
        mh2.Curr_Reg_Hrs_NYC__c = 70;
        mh2.Month_and_Year__c = 'February 2021';
        insert mh2;
        RuleBasedOnClassLevelBenfits.benefits(mh2.id);
        
        employee__C emp3 = new employee__c();
        emp3.Name = 'testEmp3'; 
        emp3.Fringe_Contract_Type__c = fc.id; 
        insert emp3;
        Monthly_Hours__c mh3 = new Monthly_Hours__c();
        mh3.Employee__c = emp3.id;
        mh3.EE__c = 'testEmp3';
        mh3.Curr_Reg_Hrs_NYC__c = 170;
        mh3.Month_and_Year__c = 'March 2021';
        insert mh3;
        RuleBasedOnClassLevelBenfits.benefits(mh3.id);
        
        employee__C emp4 = new employee__c();
        emp4.Name = 'testEmp4'; 
        emp4.Fringe_Contract_Type__c = fc.id; 
        insert emp4;
        Monthly_Hours__c mh4 = new Monthly_Hours__c();
        mh4.Employee__c = emp4.id;
        mh4.EE__c = 'testEmp4';
        mh4.Curr_Reg_Hrs_NYC__c = 240;
        mh4.Month_and_Year__c = 'April 2021';
        insert mh4;
        Test.startTest();
        RuleBasedOnClassLevelBenfits.benefits(mh4.id);
        Test.stopTest();
      }
}